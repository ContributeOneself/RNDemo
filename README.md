# RNDemo

## 项目介绍
> React Native  demo
- 由于近期公司项目需要，此处仅此为`本人`初学的。真实的 项目代码，不再此处。
- 如果我在写项目时，遇到好的文章和视频，我会继续更新。直到项目编写成型，在来完善此处。*`这里的都太基础了，后期我在分阶段，放出代码吧！`*

---

##  Pod 安装简易教程
一、项目框架
Homebrew软件包管理器

[官网(英文)](https://brew.sh)  [官网(中文)](https://brew.sh/index_zh-cn)

ruby

```sh
#用Homebrew 安装 ruby
brew install ruby
```

## 查看ruby版本(是否安装了ruby)
```sh
ruby -v
```
ruby 镜像

https://gems.ruby-china.org

2018-4-2 目前最可靠的教程博客

iOS安装CocoaPods详细过程 https://www.jianshu.com/p/9e4e36ba8574

---

<br><br><br>


# 热更新/热修复(调研的可能不全)

## [热更新方案](https://srxboys.github.io/2018/06/03/热更新方案/)

---

# React Native

## [React Native ` 开发工具 `](https://srxboys.github.io/2018/06/03/React-Native-Develop-Tool/)

## [React Native 安装](https://srxboys.github.io/2018/06/02/Mac-React-Native)


---

## 未整理的 心得
- [ 学习篇 ](./ios/1_study.md)
- [ Node.js服务 ](./ios/2_used.md)
- [ 第三方插件 ](./ios/3_Component.md)
- [ 了解更多 ](./ios/5_share.md)
- [ `Atom`工具使用问题 ](./ios/6_atom.md)
- [ 自己写插件 ](./ios/ReactNativeDIY.md)
- [ 热更新 ](./ios/hotFix.md)


---

<br><br><br>

# 参考文献(搜集碎片)
> 官网
- facebook官方文档 http://facebook.github.io  &nbsp; &nbsp; github布局地址  github布局地址:https://github.com/facebook/react-native-website
- facebook官方中文被个人翻译后的文档 https://reactnative.cn &nbsp;&nbsp;  github布局地址:https://github.com/reactnativecn/react-native.cn
- Redux `是 JavaScript 状态容器` http://www.redux.org.cn
- 推荐博客 `ES6` 语法学习(阮一峰) http://es6.ruanyifeng.com

<br>

> 学习文章
- 【React Native】从源码一步一步解析它的实现原理: https://www.jianshu.com/p/5cc61ec04b39   
- 江清清的技术专栏(react native视频教程) http://www.lcode.org/tag/react-native视频教程/
- React Native 简介：用 JavaScript 搭建 iOS 应用 (1): https://segmentfault.com/a/1190000003076518

> -
- React Native布局详细指南 https://blog.csdn.net/fengyuzhengfan/article/details/52090154
- React Native 响应式布局实践 https://blog.csdn.net/byeweiyang/article/details/80128865
- ReactNative 代码调试方法 https://blog.csdn.net/yayayaya20122012/article/details/51067798
- React Native WebView 实现滑动监听(通过修改源码来实现) https://blog.csdn.net/u013718120/article/details/79403970
- React-Native之手势进阶篇 https://blog.csdn.net/lu1024188315/article/details/73741929
- “指尖上的魔法” -- 谈谈React-Native中的手势 https://www.tuicool.com/articles/fqIRJjn
- React Native开发之动画(Animations) https://blog.csdn.net/hello_hwc/article/details/51775696

> -
- DeviceEventEmitter可以跨组件，跨页面进行数据传递，还有一些状态的修改。http://www.jianshu.com/p/c6991a241b4f
- 组件之间的通信: https://blog.csdn.net/p106786860/article/details/52408875
- React 中组件间通信的几种方式 https://www.jianshu.com/p/fb915d9c99c4
- 关于ReactNative 中 this 踩过的坑(bind) https://www.jianshu.com/p/bb194863ec0c 
- ios原生和react-native各种交互的示例代码 https://www.jb51.net/article/121782.htm

> -
- 深入浅析react native es6语法 https://www.cnblogs.com/sanshao221/p/6530123.html
- ES6学习15（异步操作和Async函数） http://lib.csdn.net/article/reactnative/54629
- React Native之ScrollView通过map()方法动态加载数组 https://blog.csdn.net/woshizisezise/article/details/51072351
- React Native(十一)删除事件以及刷新列表(数组操作)https://www.cnblogs.com/zhengyeye/p/7992236.html
- js中的数组对象排序 https://www.cnblogs.com/xljzlw/p/3694861.html
- js判断对象还是数组 https://www.cnblogs.com/dehuachenyunfei/p/6568382.html
- Javascript Array forEach()中无法return和break，代替方法some()与every()  https://blog.csdn.net/lihefei_coder/article/details/76736296

> -
- react-native兴趣交流群技术文章整理https://blog.csdn.net/slowlifes/article/details/75330862
- React Native布局实践：开发京东客户端首页 https://github.com/yuanguozheng/JdApp

<br>

> `视频学习`
- (慕课网)ReactNative基础与入门(视频讲师:CrazyCodeBoy) https://www.imooc.com/learn/808

<br>

> `客户端 / IDE 工具` 设置
- React Native 打离线包之iOS篇 https://www.jianshu.com/p/0354d629c458
- ReactNative - 打离线包 (一) 原生RN命令打包 https://www.jianshu.com/p/bb7c5f1d304e
- 如何配置Eslint检测React代码 https://www.jianshu.com/p/edda91891fb2
- flow https://blog.csdn.net/future_challenger/article/details/52770264
- Flow - JS静态类型检查工具 https://segmentfault.com/a/1190000008088489

<br>

> `客户端 / IDE 工具` 搜集
- 十大最受欢迎的 React Native 应用开发编辑器: http://baijiahao.baidu.com/s?id=1579165228216450915&wfr=spider&for=pc
- iOS程序员的React Native开发工具集(整理了React Native iOS开发过程中有用的工具、服务、测试、库以及网站等): http://www.cocoachina.com/ios/20170401/18996.html
- React Native开发之IDE（Atom+Nuclide）安装，运行，调试:    https://blog.csdn.net/hello_hwc/article/details/51612139
- React Native填坑之旅--Flow篇（番外）: https://blog.csdn.net/future_challenger/article/details/52770264
- React Native开发工具: http://www.hangge.com/blog/cache/detail_1490.html
- 手把手教你在Mac中搭建iOS的 React Native环境: https://www.cnblogs.com/damnbird/p/6074607.html
- react-native 项目初始化+编辑器 https://www.cnblogs.com/johnxc/p/6800703.html
- IGNITE生成项目分解(IGNITE是一个React Native的脚手架生成器) https://www.jianshu.com/p/1b0cb5b32049

<br>

> `搜集`热更新
- 移动端热更新方案: https://www.jianshu.com/p/739c5c5160f1
- 为什么Unity没有实现iOS平台代码热更新？ https://www.zhihu.com/question/28079874
- 如何评价腾讯在Unity下的xLua（开源）热更方案: https://www.zhihu.com/question/54344452/answer/138990189
- 如何评价 Google 的 Fuchsia、Android、iOS 跨平台应用框架 Flutter？ https://www.zhihu.com/question/50156415
- 比较跨平台的APP开发工具：PhoneGap，Xamarin，Flutter，React Native http://www.kingwins.com.cn/content-2818.html

> `实现`热更新
- 安装mysql:  https://blog.csdn.net/catstarxcode/article/details/78940385
