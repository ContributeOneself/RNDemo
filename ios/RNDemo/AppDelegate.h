//
//  AppDelegate.h
//  RNDemo
//
//  Created by srxboys on 2018/3/2.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

