//
//  Model_5.h
//  RNDemo
//
//  Created by srxboys on 2018/5/16.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTLog.h>

@protocol Model5Delegate <NSObject>
@optional
- (void)model5RequestAPI;
@end

@interface Model_5 : NSObject<RCTBridgeModule>
@property (nonatomic, weak) id<Model5Delegate>delegate;
@end


UIKIT_EXTERN NSNotificationName const Model_5_RequestAPI_Notification;


/*
   最笨的交互
 */
