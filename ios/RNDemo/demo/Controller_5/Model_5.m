//
//  Model_5.m
//  RNDemo
//
//  Created by srxboys on 2018/5/16.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import "Model_5.h"

NSNotificationName const Model_5_RequestAPI_Notification = @"Model_5_RequestAPI_Notification";

@implementation Model_5
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(requestAPI)
{
    if([self.delegate respondsToSelector:@selector(model5RequestAPI)]) {
        [self.delegate model5RequestAPI];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:Model_5_RequestAPI_Notification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
