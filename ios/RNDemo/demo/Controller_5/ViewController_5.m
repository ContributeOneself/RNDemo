//
//  ViewController_5.m
//  RNDemo
//
//  Created by srxboys on 2018/5/16.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import "ViewController_5.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

#import "Model_5.h"
#import "ModelView5.h"

@interface ViewController_5 ()<Model5Delegate>
@property (nonatomic, strong) UIView * contentView;
@property (nonatomic, strong) Model_5 * model5;
@property (nonatomic, strong) ModelView5 * modelView5;
@end

@implementation ViewController_5

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGFloat width = self.view.bounds.size.width;
    _contentView.frame = CGRectMake(0, 200, width, 300);
    _rnRootView.frame = _contentView.bounds;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"RN 5";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRNDid) name:RCTContentDidAppearNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(model5RequestAPI) name:Model_5_RequestAPI_Notification object:nil];
    [self configUI];
}

- (void)configUI {
    _contentView = [UIView new];
    _contentView.frame = CGRectMake(0, 200, 375, 300);
//    _contentView.backgroundColor = [UIColor redColor];
    [self.view addSubview:_contentView];
    
    NSURL * jsCodeLocation;
    //1、执行本地找 main.jsbundle (先找本地 127.0.0.1:80xxxx/index.ios)
//    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"文件夹" fallbackResource:nil];
    
    //2、本地找 指定文件
//    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForFallbackResource:@"main" fallbackExtension:nil];
    
    //3、 找本地 127.0.0.1:80xxxx/index.ios(和 1很想)
//    jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios&dev=true"];
    
    
    //4、 网络 行不通
//    jsCodeLocation = [NSURL URLWithString:@"https://gitee.com/srxboys/RNJsDemo/blob/master/jsDemo/index.ios_Controller_5"];
    
    
    // 5 行不通
//    NSString * localFilePath = [[NSBundle mainBundle] pathForResource:@"index.ios_Controller_5" ofType:@"js"];
//    if(localFilePath.length == 0) {
//        return;
//    }
//    jsCodeLocation = [NSURL fileURLWithPath:localFilePath];
    
    
    //6
//    jsCodeLocation = [[RCTBundleURLProvider sharedSettings]  jsBundleURLForBundleRoot:@"文件夹" fallbackResource:@"index.ios_Controller_5" fallbackExtension:@"js"];
    
//    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] resourceURLForResourceRoot:@"文件夹" resourceName:@"index.ios_Controller_5" resourceExtension:@"js" offlineBundle:nil];
    
    
//    _modelView5 = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
//                                             moduleName:@"RNDemo"
//                                      initialProperties:nil
//                                          launchOptions:nil];
//    _rnRootView = _modelView5;
    
    RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                        moduleName:@"RNDemo"
                                                 initialProperties:nil
                                                     launchOptions:nil];
    _rnRootView = rootView;
    
    [_contentView addSubview:_rnRootView];
}

- (void)loadRNDid {
    NSLog(@"\n \n rn load did \n \n \n");

    NSDictionary * appProperties = _rnRootView.appProperties;
    NSLog(@"\n\n\n rn appProperties=%@ \n\n\n", appProperties);
    
    /*
     加载进度(可自己写)
     @property (nonatomic, assign) NSTimeInterval loadingViewFadeDelay;
     @property (nonatomic, assign) NSTimeInterval loadingViewFadeDuration;
     
     //重新加载页面
     [_rnRootView.bridge reload];
     */
    
}


- (void)model5RequestAPI {
    NSLog(@"\n\n\n model5RequestAPI  model5RequestAPI \n\n\n");
    [_rnRootView.bridge reload];
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
