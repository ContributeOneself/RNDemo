//
//  MyDate.m
//  RNDemo
//
//  Created by srxboys on 2018/5/14.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import "RXMyDate.h"
#import <React/RCTConvert.h>

@implementation RXMyDate

RCT_EXPORT_MODULE();

RCT_REMAP_METHOD(printDate, date1:(nonnull NSNumber *)d1 date2:(nonnull NSNumber *)d2)
{
    NSDate* dt1 = [RCTConvert NSDate:d1];
    NSDate* dt2 = [RCTConvert NSDate:d2];
    NSComparisonResult result = [dt1 compare:dt2];
    switch(result){
        case NSOrderedAscending:
            NSLog(@"比较结果%@",@"-->开始时间小于(<)结束时间");
            break;
        case NSOrderedDescending:
            NSLog(@"比较结果%@",@"-->开始时间大于(>)结束时间");
            break;
        default:
            NSLog(@"比较结果%@",@"-->开始时间等于(==)结束时间");
            break;
    }
    
}
@end
