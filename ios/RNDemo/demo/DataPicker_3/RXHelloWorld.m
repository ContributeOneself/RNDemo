//
//  RXHelloWorld.m
//  RNDemo
//
//  Created by srxboys on 2018/5/14.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import "RXHelloWorld.h"

@implementation RXHelloWorld

RCT_EXPORT_MODULE();

#warning mark 下面2个方法，都是支持的，

/*
RCT_REMAP_METHOD(sayHello, msg:(NSString *)msg) {
    NSLog(@"打印Hello World%@",msg);
}
 */


RCT_EXPORT_METHOD(sayHello:(NSString *) msg)
{
    NSLog(@"打印Hello World%@",msg);
}

@end
