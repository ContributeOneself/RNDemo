//
//  YooliHome.m
//  RNDemo
//
//  Created by srxboys on 2018/5/22.
//  Copyright © 2018年 srxboys. All rights reserved.
//

#import "YooliHome.h"
#import <React/RCTConvert.h>

@implementation YooliHome
RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(goWYBInvest:(NSString *)page)
{
    NSLog(@"goWYBInvest click page=%@", page);
}

RCT_EXPORT_METHOD(goDCBInvest:(NSString *)page)
{
    NSLog(@"goDCBInvest click page=%@", page);
}

RCT_EXPORT_METHOD(goLJG:(NSString *)page)
{
    NSLog(@"goLJG click page=%@", page);
}


- (void)gotoNavigationWithJSBundle:(NSString *)jsbundle {

}

@end
