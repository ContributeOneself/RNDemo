
6、
```sh
# 在React Native项目根目录下运行
npm start   /   react-native start
```

7、`index.ios.js` 生产可执行的`main.jsbundle`文件(可被RN_server 找到[也就是上面的6])
一般是6启动后，修改了 js 的配置文件，需要执行下面的命令，编译js,变成可运行的jsbundle
```sh
curl http://localhost:8081/index.ios.bundle -o main.jsbundle
```


8、重新 启动RN服务问题 <br>
凡是遇到 终端命令错误等的问题，记得先 彻底关闭 ` 终端 `，在去执行，可能就好了。 <br>
<br>
或者 <br>

```sh
# 查看`React-Native服务运行进程`命令
lsof -i:8081
# 查询结果(node的PID)
# COMMAND  PID    USER   FD   TYPE             DEVICE   SIZE/OFF   NODE           NAME
# node    9753 srxboys   32u  IPv6 0xd47...     0t0      TCP       *:sproxyadmin  (LISTEN)

# 关掉进程(PID)
kill -9 9753

```

<br>


http://es6.ruanyifeng.com


---------




// 下面都是基础的东西，简易多看看官方源码，你会发现更多
// 如果你是新手，简易看看基础


# 编译项目 之前，一定要自测
1、先 运行`React Native `(上面的第6步)  <br>

2、看看`index.js `是否有问题 (上面的 第7步) <br>

3、你的 `jsCodeLocation ` 是以何种方式 写的, 是否执行你的js <br>
      (1)是 NSBundle 走Xcode的`Copy Bundle resources`里面找文件 xx.jsbundle <br>
      (2) [NSURL filexxx]   ，就是改 app 目录下里面找文件  xx.jsbundle <br>
      (3) [NSURL urlWithxx]  很遗憾，这个不支持，可能RN觉得不安全 <br>
            这个我们可以先下载xx.jsbundle,  然后执行 (2) 步【注意 安全保护处理】 <br>

------

## 三、 运行项目错误
1、如果有用的文件，一定确保文件在 `app` 的目录里  <br>
2、确保js 文件正确(上面)  <br>
3、确保js 调用的RN库，你的项目RN都保函这些库(包括自带、第三方)  <br>
4、RN 以及 支持的第三方，Xcode编译通过  <br>




- WARN eslint-plugin-react-native@3.2.1 requires a peer of eslint@^3.17.0 || ^4.0.0 but none is insta ...  <br>
package.json 文件追加 最后2行代码 

```sh
...
"dependencies": {
"react": "16.3.1",
"react-native": "0.55.4",
"eslint": "^3.17.0",
"eslint-plugin-react-native": "^3.2.1"
},
...
```











