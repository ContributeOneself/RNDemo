
关于  `定制RN原生组件` 个人心得 可能有出入

# React-native 的 js

---
#  jsx
## RN 你百度不到的底层运行逻辑

### 虚拟机
### 渲染
- 1、组件
- 2、布局
- 3、style
- ...



# iOS
##  RN->Componet 的 style
- (1)Yoga
- (2) RCTLayout
- (3)
- ...
- so 我们定制控件，可不用写 style 的适配


----

# 实践

## 必须导入的
```sh
# 这个不清楚，待研究 ，
const React = require('React');   # 这个必须导入
const ReactNative = require('ReactNative'); # 这个不导入，也不报错

# 区分 iOS / Android（适配平台、控件布局、分别匹配组件的用途）
const Platform = require('Platform');

# 参数定义 需要
const PropTypes = require('prop-types'); 
# 组件的参数 需要
const ViewPropTypes = require('ViewPropTypes');

# 创建类 需要
const createReactClass = require('create-react-class');

# 匹配原生组件 需要
const requireNativeComponent = require('requireNativeComponent');

```

## 其他的（根据需要自行导入吧）
```sh
# 
const ColorPropType = require('ColorPropType');
const StyleSheet = require('StyleSheet');

#
const UIManager = require('UIManager');
const View = require('View');
const Text = require('Text');
#... RN自定组件

#
const deprecatedPropType = require('deprecatedPropType');
const invariant = require('fbjs/lib/invariant');
const keyMirror = require('fbjs/lib/keyMirror');
const processDecelerationRate = require('processDecelerationRate');
const resolveAssetSource = require('resolveAssetSource');

#
const RCTWebViewManager = require('NativeModules').WebViewManager;

#
import type {ViewProps} from 'ViewPropTypes';
import type {ViewChildContext} from 'ViewContext';
```



----

# 实践

# RN js

```oc
/**
 * @this YLRefresh : 定制app原生组件(是自己开发原生适配RN)
 *
 * author : srxboys
 * @flow  : 用于 静态语法检查
 */

'use strict';

const ColorPropType = require('ColorPropType');
const NativeMethodsMixin = require('NativeMethodsMixin');
const Platform = require('Platform');
const React = require('React');
const PropTypes = require('prop-types');
const ViewPropTypes = require('ViewPropTypes');

const createReactClass = require('create-react-class');
const requireNativeComponent = require('requireNativeComponent');

if (Platform.OS === 'android') {
    const AndroidSwipeRefreshLayout =
    require('UIManager').AndroidSwipeRefreshLayout;
    var RefreshLayoutConsts = AndroidSwipeRefreshLayout
    ? AndroidSwipeRefreshLayout.Constants
    : {SIZE: {}};
} else {
    var RefreshLayoutConsts = {SIZE: {}};
}

/**
 *
 * ### Usage example

  ...
 *
 */

const YLRefreshControl = createReactClass({
    displayName: 'YLRefreshControl',
    statics: {
        SIZE: RefreshLayoutConsts.SIZE,
    },
    propTypes: {
        ...ViewPropTypes,
        // property
            /**
            * text刷新头部文本
            * Default app接口数据。不用我们设置
            */
            refreshText: PropTypes.string,

            refreshing: PropTypes.bool,
            isColorful: PropTypes.bool,
            hasNavBar: PropTypes.bool,

        //function
            onBeginRefresh: PropTypes.func,
            onEndRefresh:  PropTypes.func
    },

    mixins: [NativeMethodsMixin],
    _rctRefresh: {},

    // 导航栏
    // _nativeRef: (null: any),
    //  _lastNativeRefreshing: false,
    // componentDidMount() {
    //   this._lastNativeRefreshing = this.props.refreshing;
    // },
    //
    // componentDidUpdate(prevProps: {refreshing: boolean}) {
    //   // RefreshControl is a controlled component so if the native refreshing
    //   // value doesn't match the current js refreshing prop update it to
    //   // the js value.
    //   if (this.props.refreshing !== prevProps.refreshing) {
    //     this._lastNativeRefreshing = this.props.refreshing;
    //   } else if (this.props.refreshing !== this._lastNativeRefreshing) {
    //     this._nativeRef.setNativeProps({refreshing: this.props.refreshing});
    //     this._lastNativeRefreshing = this.props.refreshing;
    //   }
    // },

    render: function() {
        const props = {...this.props};
        //...
        return (
            <NativeRefreshControl
                {...props}
                ref={(ref) => { this._rctRefresh = ref; }}
                //function
                onBeginRefresh={this._onBeginRefresh}
                onEndRefresh={this._onEndRefresh}
            />
        )
    },

    _onBeginRefresh(){
        this._lastNativeRefreshing = true;
        this.props.onBeginRefresh && this.props.onBeginRefresh();
        this.forceUpdate();
    },

    _onEndRefresh(){
        this._lastNativeRefreshing = false;
        this.props.onEndRefresh && this.props.onEndRefresh();
        this.forceUpdate();
    },
});




//原生组件名
if (Platform.OS === 'ios') {
    var NativeRefreshControl = requireNativeComponent(
        'YLRCTRefreshControl',
        YLRefreshControl
    );
} else if (Platform.OS === 'android') {
    var NativeRefreshControl = requireNativeComponent(
        'AndroidSwipeRefreshLayout',
        YLRefreshControl
    );
}


module.exports = YLRefreshControl;

```

# OC Code 这个有时间在放出来吧


