# atom

## atom报错"Cannot load the system dictionary for zh-CN"
     https://newsn.net/say/atom-error-cant-load.html
解决方案如下，在atom的preferences设置中，到了Packages菜单，然后在查找输入框里面，输入spell字样。然后就可以看到一个包，叫做：spell-check这个插件，在这个插件的右下角，找到disable按钮，点一下，禁用这个插件即可。
     

## Flow was not found when attempting to start Atom/Nuclide
 各种不管用
<!--     https://stackoverflow.com/questions/43407149/flow-was-not-found-when-attempting-to-start-atom-nuclide         -->
<!--     https://flow.org/en/docs/install/       -->

解决这个，就解决上面的啦 <br>
- Wrong version of Flow. The config specifies version ^0.67.0 but this is version 0.73.0
解决:
1、node_modules->react-native->.flowconfig   version 改为 0.67.1 （https://yarnpkg.com/zh-Hans/package/flow-bin 最新的为 0.74.0  。 so ...）
2、sudo npm install flow-bin@0.74.0 -g         `在项目根目录`

## atom 的 nuclide 怎么配置flow呢 (详细教程 https://nuclide.io)
`which flow` 得到的路径  -> nuclide 的  `path to flow exeutable`

     
     
## can't find start packager command of RN
https://github.com/facebook/nuclide/issues/1392

## Unable to resolve module `AccessibilityInfo
可能是 你使用的 `终端` 
```sh
curl http://localhost:8081/index.ios.bundle -o main.jsbundle
```
建议使用(具体目录根据自己需要)
```sh
eact-native bundle --platform ios --dev yes --entry-file index.ios.js --bundle-output ./ios/bundle/main.jsbundle --assets-dest ./ios/bundle/
```

# 快捷键
```
Atom：
    快捷键 https://www.jianshu.com/p/e33f864981bb

常用的:
    1、搜索:   command + shift + p
    2、显示/隐藏目录  command + \


在Atom中设置你的Snippet(代码格式) https://www.cnblogs.com/tkchu/p/6136683.html
'.source.python':                       # 在何种文件中使用这个Snippet
    'python coding':                        # 对Snippet的简短说明
        'prefix': 'coding'                    # 输入什么前缀可以触发这个Snippet
        'body': '# -*- coding:utf8 -*-\n'     # Snippet的内容
        
        
        
        
        ------------------------------------------------------------------------
        
color 所有颜色  open ./node_modules/color-name/index.js
        
        
//代码注释 高亮提示        
        ------------------------------------------------------------------------
        
        //  WARNING: 警告 ⚠️
        @author    : 作者
        @return    : 返回
        
        @public    : 公共
        @private   : 私有
        
        @class     : 类
        @func      : 方法
        @property  : 属性 📚
        @param     : 参数
        
        @var       : 变量
        @const     : 常量
        @static    : 静态
        @default   : 默认
        @new
        
        @extends   : 继承(组件只能继承 RN组件，不可以自定义  切记。。 js 的都支持)
        @interface
        @api
        
        @download
        
        @this
        
        @file
        
        
        // TODO:
        @todo        
```

# nuclide

## 想让flow 检测你写的，就要在你写的文件夹顶部，copy一个`.flowconfig`,到根目录

# watchman
