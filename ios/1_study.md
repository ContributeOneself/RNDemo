

# React-native 的第一天 安装基础篇 <br>
(基于MaxOS   iOS) <br>
## 一、安装工具 <br>

1、[ 安装 homebre ](https://brew.sh)  <br>
```sh
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
2、然后 安装 ` node.js `  <br>
```sh
    brew install node
```
3、  配置` node `国内镜像
```sh
# 修改 下载仓库为淘宝镜像
npm config set registry https://registry.npm.taobao.org --global

#如果要发布自己的镜像需要修改回来
#npm config set registry https://registry.npmjs.org/
　 
npm config set disturl https://nodejs.org/dist/ --global

#是否配置成功,请使用下面命令
npm config get registry
npm config get disturl

#查看npm全部配置
npm config get
```
4、Yarn、React Native的命令行工具（react-native-cli） <br>
Yarn是Facebook提供的替代npm的工具，可以加速node模块的下载。React Native的命令行工具用于执行创建、初始化、更新项目、运行打包服务（packager）等任务。 <br>

```sh
npm install -g yarn react-native-cli

# 配置` Yarn `国内镜像
yarn config set registry https://registry.npm.taobao.org --global
yarn config set disturl https://npm.taobao.org/dist --global
```

## 二、安装 RN <br>
1、新建工程文件夹 并 cd 后，建立 `/ios` 文件夹(用于Xcode工程文件) <br>
https://reactnative.cn/docs/0.51/integration-with-existing-apps.html#content <br>

2、在项目根目录下创建一个名为package.json的空文本文件 <br>
或者 <br>
npm init （这个工作跟cocoapods的podfile文件初始化有点像）-> 根据提示输入工程的package.json的内容。 <br>

3、添加 yarn (react-native依赖管理工具)
```sh
yarn add react-native

# facebook.github.io/react-native/docs/image.html 看官网最新是哪个版本
yarn add react@16.3.1
```

// 如果你已经安装过 `node`  `Yarn` 就可以安装RN库，和 cocoPoads管理(pod 导入RN方式) <br>
4、工程根目录 使用命令(这个会很慢很慢，取决你的网络) <br>
```sh
npm install
```

5、进入项目目录(我的是 ./ios/) ,如果你已经安装Cocoapods 了 
```sh
pod install
```

6、react-native (跑 本地服务，方面实时用nodo.js去解析ES6的代码->`main.bundle.js`)
```sh
# 在React Native项目根目录下运行
npm start   /   react-native start
```

7、index.ios.js 生产可执行的文件(可被RN_server 找到) （作为测试用，真正开发需要 `react-native bundle.........`）
```sh
curl http://localhost:8081/index.ios.bundle -o main.jsbundle
```
