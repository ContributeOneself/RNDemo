# 热更新实践
### server端
npm v0.2.20       
node >6.0

安装code-push-server

	$ npm install code-push-server -g
	$ code-push-server-db init --dbhost localhost --dbuser root --dbpassword #初始化mysql数据库
	$ code-push-server #启动服务 浏览器中打开 http://127.0.0.1:3000
	
修改config.js

![](/Users/qd/Downloads/code-push-server-config-js.png)
启动时，告诉了我们config.js的位置。去修改它。

修改db配置
 
 	db: {
	    username: process.env.RDS_USERNAME || "root",
	    password: process.env.RDS_PASSWORD || "appName@",
	    database: process.env.DATA_BASE || "codepush",
	    host: process.env.RDS_HOST || "127.0.0.1",
	    port: process.env.RDS_PORT || 3306,
	    dialect: "mysql",
	    logging: false
	  },

修改local配置：

	local: {
	    storageDir: "/usr/tablee/workspaces/storage",
	    downloadUrl: "http://172.16.2.180:3000/download",
	    public: '/download'
	  },
	  
修改jwt设置，保证安全性

	jwt: {
	    // Recommended: 63 random alpha-numeric characters
	    // Generate using: https://www.grc.com/passwords.htm
	    tokenSecret: 'Qc9dx6WmkF4NgfgHs0RvYyxE1LR7Yfb0z1JUAAwAlG0PLutXE7SXfhbyQ4e8cz4'
	  },
	  
修改common设置

	common: {
	    tryLoginTimes: 0,
	    diffNums: 3,
	    dataDir: "/usr/tablee/workspaces/data",
	    // storageType which is your binary package files store. options value is ("local" | "qiniu" | "s3")
	    storageType: "local",
	    // options value is (true | false), when it's true, it will cache updateCheck results in redis.
	    updateCheckCache: false
	  },
重新启动code-push-server

### 推送端（存放代码的server）
安装react-native-code-push及code-push-cli，在client端，存放代码的地方。

	npm install -g code-push-cli
	//进入项目根目录下运行
	npm install --save react-native-code-push@latest
	
* 登陆code-push-server服务器

		code-push login http://172.16.2.180:3000
	
	此时会打开浏览器并出现以下界面

	![](/Users/qd/Downloads/code-push-login.png)

	在浏览器中输入账号密码（默认admin，123456）登陆，然后获取token，将token复制后，返回终端，拷贝token再回车后登陆成功
* 添加项目

		code-push app add MyApp-android android react-native
		
	其中MyApp-android是应用的名称，此时会告诉我们 DeploymentKey 
	
		Successfully added the "MyApp-android" app, along with the following default deployments:
		┌────────────┬───────────────────────────────────────┐
		│ Name       │ Deployment Key                        │
		├────────────┼───────────────────────────────────────┤
		│ Production │ aUC4yZtphVneeCWD4kQxxxxxx │
		├────────────┼───────────────────────────────────────┤
		│ Staging    │ wz32NK1SxP0ef7mxf64Axxxx │
		└────────────┴───────────────────────────────────────┘

	记录下来，客户端接入时需要用到
	
### 客户端（Android & iOS）接入

* Android端	
	1.settings.gradle中加入工程依赖

		include ':app', ':base',':PushSDK', ':react-native-code-push'
		project(':react-native-code-push').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-code-push/android/app')
		
	*工程必须改名成app*
	2.在app下build.gradle新增gradle依赖，react-native-code-push工程，以及versionName
	
		apply plugin: 'com.android.application'
		apply plugin: 'android-apt'
		apply from: "../android_common.gradle"
		// code push gradle
		apply from: "../../node_modules/react-native-code-push/android/codepush.gradle"
		
	---
	
		defaultConfig {
	        applicationId "com.cashloan"
	        minSdkVersion 23
	        targetSdkVersion 24
	
	        versionCode 66816
	        versionName "1.5.0" //code push 从此处获取版本号信息
	
	        multiDexEnabled true
	        ndk {
	            abiFilters "armeabi-v7a", "x86"
	        }
	        packagingOptions {
	            exclude "lib/arm64-v8a/librealm-jni.so"
	        }
	    }
	 
	---
		dependencies {
		    compile fileTree(include: ['*.jar'], dir: 'libs')
		    compile project(':base')
		    compile project(':PushSDK')
		    compile 'com.facebook.react:react-native:+'
		    compile project(':react-native-code-push')  // code push 工程
		}
		
	3.YLReactActivity修改，bundle路径由codepush控制，新增codepush的package（bundle路径必须在新增完codepush package后才能加，否则会报错）。
	入口由moduleName控制修改为入参控制（详见index.android.js中修改）
	
		ReactInstanceManagerBuilder builder = ReactInstanceManager.builder()
                .setApplication(getApplication())
                .setCurrentActivity(YLReactActivity.this)
                //bundle路径都交给codepush去控制
				//.setJSMainModulePath("index.android")
                .addPackage(new MainReactPackage())
                .addPackage(new ReactPackage() {
                    @Override
                    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
                        List<NativeModule> list = new ArrayList<>();
                        list.add(new CommModule(reactContext));
                        list.add(new JsAndroidModule(reactContext));
                        return list;
                    }

                    @Override
                    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
                        return Collections.emptyList();
                    }
                })
                .addPackage(new CodePush("wz32NK1SxP0ef7mxf64Ay7FleJWr4ksvOXqog",CoreUtils.getApplicationContext(),BuildConfig.DEBUG, "http://172.16.2.180:3000"))
                //bundle路径都交给codepush去控制
                .setJSBundleFile(CodePush.getJSBundleFile())
                .setUseDeveloperSupport(BuildConfig.DEBUG)
                .setInitialLifecycleState(LifecycleState.BEFORE_RESUME);
				//bundle路径都交给codepush去控制
				//builder.setBundleAssetName("index.android.bundle");

	        mReactInstanceManager = builder.build();
	
	        Bundle bundle = new Bundle();
	        bundle.putString(Entrance, moduleName);
	        mReactRootView.startReactApplication(mReactInstanceManager, "AppRoot", bundle);
	        
	 4.修改index.android.js
	 
		 import CodePush from 'react-native-code-push'
	  	 class SkipRoot extends Component{

		    componentDidMount() {
		        CodePush.notifyAppReady();
				 this.syncSilently();
		    }   
		    /** Update is downloaded silently, and applied on restart (recommended) */
		    syncSilently() {
		        CodePush.sync(
		        {},
		        this._codePushStatusDidChange.bind(this),
		        this._codePushDownloadDidProgress.bind(this)
		        );
		    }  
		    
		    _codePushDownloadDidProgress(progress) {
	        	console.log(progress);
		    }
		
		    _codePushStatusDidChange(syncStatus) {
		        switch(syncStatus) {
		            case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
		            console.log("Checking for update.");
		            break;
		            case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
		            console.log("Downloading package.");  
		            break;
		            case CodePush.SyncStatus.AWAITING_USER_ACTION:
		            console.log("Awaiting user action."); 
		            break;
		            case CodePush.SyncStatus.INSTALLING_UPDATE:
		            console.log("Installing update."); 
		            break;
		            case CodePush.SyncStatus.UP_TO_DATE:
		            console.log("App up to date.");
		            break;
		            case CodePush.SyncStatus.UPDATE_IGNORED:
		            console.log("Update cancelled by user.");
		            break;
		            case CodePush.SyncStatus.UPDATE_INSTALLED:
		            console.log("Update installed and will be applied on restart.");
		            break;
		            case CodePush.SyncStatus.UNKNOWN_ERROR:
		            console.log("An unknown error occurred.");
		            break;
		        }
		    }
		
		    render(){
		        switch (this.props.entrance){
		            case 'About':
		                return <About />
		            case 'Help':
		                return <Help />
		            case 'List':
		                return <List />
		        }
		    }  
		  }
		
		AppRegistry.registerComponent('AppRoot', () => SkipRoot);

#### 部署热更新
测试环境执行：

	code-push release-react MyApp-android android

生产环境执行：

	code-push release-react MyApp-android android -d Production

* ios端	
	1.podfile添加引入模块
	
	  	pod 'CodePush', :path => '../node_modules/react-native-code-push', :subspecs => ['Core','SSZipArchive','JWT']
	 
	2.在RNRootPageHelper中添加bundleUrl由codepush控制
	
		jsCodeLocation = [CodePush bundleURL];
	3.修改index.ios.js(同android)
