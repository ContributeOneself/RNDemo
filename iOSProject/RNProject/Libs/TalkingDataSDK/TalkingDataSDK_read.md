
TalkingDataSDK (Download Time 2018-4-23)

SaaS_AppAnalytics_iOS_SDK_V4.0.21


依赖库:
AdSupport                     获取advertisingIdentifier
CoreMotion                    支持摇一摇功能
CoreTelephony               获取运营商标识
SystemConfiguration     检测网络状况
libc++                             支持最新的c++11标准
libz                                  进行数据压缩

// 只是记录 当前项目所使用的版本，以便后续更新。(方便团队合作持续开发)
