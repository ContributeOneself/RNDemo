//
//  DYCheckButton.m
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYCheckButton.h"
#import "SCMAnimationManage.h"

@implementation DYCheckButton

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat left = self.contentLeft;
    if(self.iconImageView.image) {
        _iconImageView.frame = CGRectMake(left, (self.Ex_height - _iconSize.height)/2, _iconSize.width, _iconSize.height);
        left += _iconSize.width + 10;
    }
    
    self.keyLabel.frame = CGRectMake(left, 0, self.Ex_width - left, self.Ex_height);
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType {
    DYCheckButton* btn = [super buttonWithType:buttonType];
    if(btn) {
        [btn configUI];
    }
    return btn;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configUI];
    }
    return self;
}

- (void)configUI {
    _iconImageView = [UIImageView new];
    _iconImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_iconImageView];
    
    _keyLabel = [UILabel new];
    [self addSubview:_keyLabel];
    
    self.titleLabel.textColor = [UIColor clearColor];
}

- (void)setIconNomal:(UIImage *)iconNomal {
    _iconNomal = iconNomal;
    self.iconImageView.image = iconNomal;
}

- (void)setKey:(NSString *)key {
    self.keyLabel.text = key;
}

- (NSString *)key {
    return self.keyLabel.text;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if(selected) {
        self.iconImageView.image = self.iconSelected;
    }
    else {
        self.iconImageView.image = self.iconNomal;
    }
}

- (void)sloshingHint {
    [SCMAnimationManage animationEarthquakeWithView:self];
}
@end
