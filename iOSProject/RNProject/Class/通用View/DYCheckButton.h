//
//  DYCheckButton.h
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DYCheckButton : UIButton
@property (nonatomic, strong) UIImageView * iconImageView;
@property (nonatomic, strong) UILabel * keyLabel;

@property (nonatomic, copy) UIImage * iconNomal;
@property (nonatomic, copy) UIImage * iconSelected;
@property (nonatomic, assign) CGSize iconSize;
@property (nonatomic, copy) NSString * key;
@property (nonatomic, assign) CGFloat contentLeft;

//晃动提示
- (void)sloshingHint;
@end
