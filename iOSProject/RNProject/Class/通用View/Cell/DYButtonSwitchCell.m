//
//  DYButtonSwitchTableViewCell.m
//  DragonYin
//
//  Created by 王亚军 on 2018/4/30.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYButtonSwitchCell.h"

@interface DYButtonSwitchCell ()

@property (nonatomic, strong) UIButton * licaiBtn;
@property (nonatomic, strong) UIButton * xiaofeiBtn;
@property (nonatomic, strong) UIButton * jiesuanBtn;

@end

@implementation DYButtonSwitchCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    
}

- (void)configCellUI{
    [super configCellUI];
    NSArray * array = @[@"理财",@"消费",@"结算"];
    for (int i = 0; i < 3; i ++) {
        UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.titleLabel.font = FONT_BASE(12);
        [btn setTitle:array[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"img_checkbox_false"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"img_checkbox_yes"] forState:UIControlStateSelected];
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        [self.contentView addSubview:btn];
        //这个不好，失去了通用性
        btn.frame = CGRectMake(35 + 80 *i, 10, 80, 30);
        if (i == 0) {
            self.licaiBtn = btn;
        }else if (i ==1){
            self.xiaofeiBtn = btn;
        }else if (i == 2){
            self.jiesuanBtn = btn;
        }
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}
- (void)btnClick:(UIButton *)sender{
    NSArray * arr = @[_licaiBtn,_xiaofeiBtn,_jiesuanBtn];
    for ( int i = 0 ;i < 3; i++) {
        
        UIButton * btn = arr[i];
        if (btn == sender) {
            [btn setSelected:YES];
            if (self.switchBtnClick) {
                self.switchBtnClick(i+1);
            }
        }else{
            [btn setSelected:NO];
        }
    }
    
}
- (void)setIndex:(SwitchType)index{
    _index = index;
    NSArray * arr = @[_licaiBtn,_xiaofeiBtn,_jiesuanBtn];

    index -= 1;
    switch (index) {
        case SwitichTypeFinancial:{
            for (UIButton * btn in arr) {
                if (btn == _licaiBtn) {
                    [btn setSelected:YES];
                }else{
                    [btn setSelected:NO];
                }
            }
        }break;
        case SwitichTypeConsumer:{
            for (UIButton * btn in arr) {
                if (btn == _xiaofeiBtn) {
                    [btn setSelected:YES];
                }else{
                    [btn setSelected:NO];
                }
            }
        }break;
        case SwitichTypeBalance:{
            for (UIButton * btn in arr) {
                if (btn == _jiesuanBtn) {
                    [btn setSelected:YES];
                }else{
                    [btn setSelected:NO];
                }
            }
        }break;
        default:
            break;
    }
}
@end
