//
//  DYIconRichLabelCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYIconRichLabelCell.h"
#import "SCMAnimationManage.h"

@implementation DYIconRichLabelCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat left = self.contentInset.left;
    CGFloat top = self.contentInset.top;
    CGFloat cellHeight = self.contentView.Ex_height - top - self.contentInset.bottom;
    if(_checkButton.imageView.image) {
        left += 10;
        _checkButton.frame = CGRectMake(left, (cellHeight-_checkSize.height)/2+top, _checkSize.width, _checkSize.height);
        left += _checkSize.width+20;
    }
    if ([self.richLabel.text length] > 0) {
        self.richLabel.frame = CGRectMake(left, top, self.contentView.Ex_width-left - self.contentInset.right,  cellHeight);
    } else {
        self.richLabel.frame = CGRectZero;
    }
    
}

- (void)configCellUI {
    [super configCellUI];
    
    _checkSize = CGSizeMake(0, 0);
    
    _checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_checkButton addTarget:self action:@selector(checkButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_checkButton];
    
    _richLabel = [RichLabel new];
    _richLabel.font = FONT_BASE(14);
    _richLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_richLabel];
}

- (void)setCheckNomalImage:(UIImage *)checkNomalImage {
    [self.checkButton setImage:checkNomalImage forState:UIControlStateNormal];
}

- (UIImage *)checkNomalImage {
    return [self.checkButton imageForState:UIControlStateNormal];
}

- (void)setCheckSelectedImage:(UIImage *)checkSelectedImage {
    [self.checkButton setImage:checkSelectedImage forState:UIControlStateSelected];
}

- (UIImage *)checkSelectedImage {
    return [self.checkButton imageForState:UIControlStateSelected];
}

- (void)checkButtonClick {
    if(self.checkSelectedImage) {
        self.checkButton.selected = !self.checkButton.isSelected;
    }
    if(self.checkTapAction) {
        self.checkTapAction();
    }
}

- (void)sloshingHint {
    [SCMAnimationManage animationEarthquakeWithView:self];
}

@end
