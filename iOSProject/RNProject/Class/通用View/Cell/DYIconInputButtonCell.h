//
//  DYIconInputButtonCell.h
//  DragonYin
//
//  Created by srxboys on 2018/4/26.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYIconKeyInputCell.h"

@interface DYIconInputButtonCell : DYIconKeyInputCell
@property (nonatomic, strong) UIButton * accessButton;

@property (nonatomic, copy) UIImage * accessImage;
@property (nonatomic, assign) CGSize accessSize; //默认多少
@property (nonatomic, copy) TapAction accessAction;
@end
