//
//  BaseTableViewCell.h
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CELL_PADDING  22.0
#define CELL_LINE_H   0.5

typedef NS_ENUM(NSInteger, CellSeparatorType) {
    CellSeparatorTypeNone = 0,   //没有
    CellSeparatorTypeLine,       //通长
    CellSeparatorTypeLineIndent, //拒左
    CellSeparatorTypeCenter      //左 右 CELL_PADDING
};

typedef struct CellBottomInsets {
    CGFloat  left, right;  // specify amount to inset (positive) for each of the edges. values can be negative to 'outset'
} CellBottomInsets;

UIKIT_STATIC_INLINE CellBottomInsets CellBottomInsetsMake(CGFloat left, CGFloat right) {
    CellBottomInsets insets = {left, right};
    return insets;
}

typedef void(^TapAction)(void);

@interface BaseTableViewCell : UITableViewCell
@property (nonatomic, assign) CellSeparatorType topSeparatorType;//顶部分割线 默认None


@property (nonatomic, assign) CellSeparatorType bottomSeparatorType; //底部分割线 默认None
@property (nonatomic, assign) CellBottomInsets bottomSeparatorInsets; //头/尾 距离 （上面的不一样）


@property (nonatomic, strong) UIColor *splitterColor; //cell之间分割线的颜色
@property (nonatomic, assign) BOOL enabled; //是否响应(不是内部的控件是否相应，是整个cell，包括tableView selectd 是否相应)

@property (nonatomic, copy) UIColor *totalBackgroundColor;//全部的背景色 进行统一修改

@property (nonatomic, assign) UIEdgeInsets contentInset; //default left=22 right=22 (子类实现布局)

- (void)configCellUI NS_REQUIRES_SUPER; //(子类实现布局)
@end
