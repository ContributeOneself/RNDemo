//
//  DYButtonSwitchTableViewCell.h
//  DragonYin
//
//  Created by 王亚军 on 2018/4/30.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseTableViewCell.h"

typedef NS_ENUM(NSInteger, SwitchType) {
    SwitichTypeFinancial = 0, //理财
    SwitichTypeConsumer  = 1, //消费
    SwitichTypeBalance   = 2  //结算
};

@interface DYButtonSwitchCell : BaseTableViewCell

@property (nonatomic, copy) void (^switchBtnClick)(SwitchType index);

@property (nonatomic, assign) SwitchType index;

//后期再说，不能写死了 通用cell，说的就是通用性
//@property (nonatomic, copy) UIImage * imageNomal;
//@property (nonatomic, copy) UIImage * imageSelected;

@end
