//
//  DYIconInputButtonCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/26.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYIconInputButtonCell.h"

@implementation DYIconInputButtonCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat top = self.contentInset.top;
    CGFloat left = self.contentInset.left;
    CGFloat cellHeight = self.contentView.Ex_height - top - self.contentInset.bottom;
    
    if(self.iconButton.imageView.image) {
        self.iconButton.frame = CGRectMake(left, (cellHeight-self.iconSize.height)/2+top, self.iconSize.width, self.iconSize.height);
        left += self.iconSize.width + 20;
    }
    
    if(self.keyLabel.text || self.keyLabel.attributedText) {
        if(self.valueTextField.text || self.valueTextField.attributedText) {
            CGRect baseFrame = CGRectMake(0, 0, self.contentView.Ex_width - left - self.contentInset.right, cellHeight);
            CGFloat keyLabelWidth = ceilf([self.keyLabel textRectForBounds:baseFrame limitedToNumberOfLines:1].size.width);
            self.keyLabel.frame = CGRectMake(left, top, keyLabelWidth, cellHeight);
            left += keyLabelWidth + 5;
            
            CGFloat valueWidth = self.contentView.Ex_width - left - self.contentInset.right;
            if(self.accessSize.width) {
                valueWidth = valueWidth - self.accessSize.width - 5;
                CGFloat accessTop = (cellHeight - self.accessSize.height)/2+top;
                self.accessButton.frame = CGRectMake(self.Ex_width - self.accessSize.width - 2 - self.contentInset.right, accessTop, self.accessSize.width, cellHeight);
            }
            self.valueTextField.frame = CGRectMake(left, top, valueWidth, cellHeight);
        }
        else {
            self.keyLabel.frame = CGRectMake(left, top, self.contentView.Ex_width - left - self.contentInset.right, cellHeight);
            self.valueTextField.frame = CGRectZero;
        }
    }
    else {
        self.keyLabel.frame = CGRectZero;
        CGFloat valueWidth = self.contentView.Ex_width - left - self.contentInset.right;
        if(self.accessSize.width) {
            valueWidth = valueWidth - self.accessSize.width - 5;
            CGFloat top = (self.Ex_height - self.accessSize.height)/2;
            self.accessButton.frame = CGRectMake(self.Ex_width - self.accessSize.width - 2 - self.contentInset.right, top, self.accessSize.width, self.accessSize.height);
        }
        self.valueTextField.frame = CGRectMake(left, 0, valueWidth, self.contentView.Ex_height);
    }
}

- (void)configCellUI {
    [super configCellUI];
    
    _accessButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_accessButton addTarget:self action:@selector(accessButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_accessButton];
}

- (void)setAccessImage:(UIImage *)accessImage {
    _accessImage = accessImage;
    [self.accessButton setImage:accessImage forState:UIControlStateNormal];
}

- (void)accessButtonClick {
    [self endEditing:YES];
    if(self.accessAction) {
        self.accessAction();
    }
}

@end
