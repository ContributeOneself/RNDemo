//
//  DYBankIDCell.m
//  DragonYin
//
//  Created by srxboys on 2018/5/5.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYBankIDCell.h"
#import "RXBankCardFormatTextField.h"

@implementation DYBankIDCell

- (void)configCellUI {
    [super configCellUI];
    
    [self.valueTextField removeFromSuperview];
    self.valueTextField = [RXBankCardFormatTextField new];
    self.valueTextField.font = FONT_BASE(14);
    self.valueTextField.textColor = COLOR_333;
    [self.contentView addSubview:self.valueTextField];
}

@end
