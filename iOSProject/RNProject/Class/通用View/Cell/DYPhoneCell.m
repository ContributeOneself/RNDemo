//
//  DYPhoneCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/26.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYPhoneCell.h"
#import "RXPhoneNumberFormatTextField.h"

@implementation DYPhoneCell

- (void)configCellUI {
    [super configCellUI];
    
    [self.valueTextField removeFromSuperview];
    self.valueTextField = [RXPhoneNumberFormatTextField new];
    self.valueTextField.font = FONT_BASE(14);
    self.valueTextField.textColor = COLOR_333;
    [self.contentView addSubview:self.valueTextField];
}

@end
