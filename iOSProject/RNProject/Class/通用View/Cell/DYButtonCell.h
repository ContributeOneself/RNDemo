//
//  DYButtonCell.h
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface DYButtonCell : BaseTableViewCell
@property (nonatomic, strong) UIButton * button;

@property (nonatomic, copy) NSString * key;
@property (nonatomic, copy) TapAction buttonTapAction;
@end
