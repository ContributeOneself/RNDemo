//
//  DYIconKeyInputCell.h
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RXBlockTextField.h"


@interface DYIconKeyInputCell : BaseTableViewCell
@property (nonatomic, strong) UIButton         *iconButton;
@property (nonatomic, strong) UILabel          *keyLabel;
@property (nonatomic, strong) RXBlockTextField *valueTextField;

@property (nonatomic, copy) UIImage  *icon;
@property (nonatomic, copy) UIImage  *iconSelected;
@property (nonatomic, assign) CGSize  iconSize; //default size(30,30)
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *valuePlaceholder;

@property (nonatomic, copy) TapAction iconAction;
@end
