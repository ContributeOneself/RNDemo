//
//  DYCaptchaCell.h
//  DragonYin
//
//  Created by srxboys on 2018/4/26.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYIconInputButtonCell.h"

@interface DYCaptchaCell : DYIconInputButtonCell
@property (nonatomic, assign) NSInteger count;    //倒计时  默认120
@property (nonatomic, copy) NSString * catchaText; //正在获取count

- (void)sendMessageForPhone:(NSString *)phone;
@end
