//
//  DYIconKeyInputCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYIconKeyInputCell.h"

@implementation DYIconKeyInputCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat top = self.contentInset.top;
    CGFloat left = self.contentInset.left;
    CGFloat cellHeight = self.contentView.Ex_height - top - self.contentInset.bottom;
    
    if(self.iconButton.imageView.image) {
        self.iconButton.frame = CGRectMake(left, (cellHeight-_iconSize.height)/2+top, _iconSize.width, _iconSize.height);
        left += _iconSize.width + 20;
    }
    
    if(_keyLabel.text || _keyLabel.attributedText) {
        if(_valueTextField.text || _valueTextField.attributedText) {
            CGRect baseFrame = CGRectMake(0, 0, self.contentView.Ex_width - left - self.contentInset.right, cellHeight);
            CGFloat keyLabelWidth = ceilf([_keyLabel textRectForBounds:baseFrame limitedToNumberOfLines:1].size.width);
            _keyLabel.frame = CGRectMake(left, top, keyLabelWidth, cellHeight);
            left += keyLabelWidth + 5;
            CGFloat valueWidth = self.contentView.Ex_width - left - self.contentInset.right;
            _valueTextField.frame = CGRectMake(left, top, valueWidth, cellHeight);
        }
        else {
            _keyLabel.frame = CGRectMake(left, top, self.contentView.Ex_width - left - self.contentInset.right, cellHeight);
            _valueTextField.frame = CGRectZero;
        }
    }
    else {
        _keyLabel.frame = CGRectZero;
        _valueTextField.frame = CGRectMake(left, top, self.contentView.Ex_width - left - self.contentInset.right, cellHeight);
    }
}

- (void)configCellUI {
    [super configCellUI];
    
    _iconSize = CGSizeMake(30, 30);
    
    _iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _iconButton.userInteractionEnabled = NO;
    [_iconButton addTarget:self action:@selector(iconButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_iconButton];
    
    _keyLabel = [UILabel new];
    _keyLabel.textColor = COLOR_333;
    _keyLabel.font = FONT_BASE(14);
    [self.contentView addSubview:_keyLabel];
    
    _valueTextField = [RXBlockTextField new];
    _valueTextField.font = FONT_BASE(14);
    _valueTextField.textColor = COLOR_333;
    [self.contentView addSubview:_valueTextField];
}

- (void)setIcon:(UIImage *)icon {
    [self.iconButton setImage:icon forState:UIControlStateNormal];
}

- (UIImage *)icon {
    return  [self.iconButton imageForState:UIControlStateNormal];
}

- (void)setIconSelected:(UIImage *)iconSelected {
    if(iconSelected) {
        self.iconButton.userInteractionEnabled = YES;
    }
    [self.iconButton setImage:iconSelected forState:UIControlStateSelected];
}

- (UIImage *)iconSelected {
    return  [self.iconButton imageForState:UIControlStateSelected];
}

- (void)setKey:(NSString *)key {
    _keyLabel.text = key;
}

- (NSString *)key {
    return _keyLabel.text;
}

- (void)setValue:(NSString *)value {
    self.valueTextField.text = value;
}

- (NSString *)value {
    return self.valueTextField.text;
}

- (void)setValuePlaceholder:(NSString *)valuePlaceholder {
    _valueTextField.placeholder = valuePlaceholder;
}

- (NSString *)valuePlaceholder {
    return _valueTextField.placeholder;
}


- (void)iconButtonClick {
    if(self.iconSelected) {
        self.iconButton.selected = !self.iconButton.isSelected;
    }
    
    if(self.iconAction) {
        self.iconAction();
    }
}
@end
