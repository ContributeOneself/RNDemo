//
//  BaseTableViewCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseTableViewCell.h"

enum SplitterAlignment
{
    SplitterAlignmentCenter,
    SplitterAlignmentLeft,
    SplitterAlignmentRight,
};
@interface CellBackgroundView : UIView
@property (nonatomic) BOOL splitterOnTop;
@property (nonatomic) BOOL splitterOnBottom;
@property (nonatomic) BOOL splitterOnDiy;
@property (nonatomic) CGFloat splitterIndentationTop;
@property (nonatomic) CGFloat splitterIndentationBottom;
@property (nonatomic) CGFloat splitterIndentationBottomHead;
@property (nonatomic) CGFloat splitterIndentationBottomEnd;
@property (nonatomic) enum SplitterAlignment splitterAlignment;
@property (nonatomic, strong) UIColor *splitterColor;

@property (nonatomic) BOOL splitterHidden;
@property (nonatomic) CGFloat splitterLengthRatioTop;
@property (nonatomic) CGFloat splitterLengthRatio;

@end

@implementation CellBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.splitterOnTop = NO;
        self.splitterOnBottom = NO;
        self.splitterIndentationTop = 0.0f;
        self.splitterIndentationBottom = 0.0f;
        self.splitterAlignment = SplitterAlignmentCenter;
        self.splitterColor = [UIColor clearColor];
        
        self.splitterHidden = NO;
        self.splitterLengthRatio = 0.0f;
        self.splitterLengthRatioTop = 0.0f;
    }
    return self;
}

- (void)dealloc
{
    
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (_splitterOnBottom == NO && _splitterOnTop == NO && _splitterOnDiy == NO)
        return ;
    
    CGFloat x = rect.origin.x;
    CGFloat w = rect.size.width;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, CELL_LINE_H);
    CGContextSetStrokeColorWithColor(context, [_splitterColor CGColor]);
    
    if (_splitterOnTop)
    {
        if (_splitterAlignment == SplitterAlignmentRight)
        {
            x = _splitterIndentationTop;
            w -= _splitterIndentationTop;
        }
        else if (_splitterAlignment == SplitterAlignmentCenter)
        {
            x = _splitterIndentationTop;
            w -= 2*_splitterIndentationTop;
        }
        
        CGContextMoveToPoint(context, x, rect.origin.y+(CELL_LINE_H/2));
        CGContextAddLineToPoint(context, x+w, rect.origin.y+(CELL_LINE_H/2));
    }
    
    if (_splitterOnBottom)
    {
        if (_splitterAlignment == SplitterAlignmentRight)
        {
            x = _splitterIndentationBottom;
            w -= _splitterIndentationBottom;
        }
        else if (_splitterAlignment == SplitterAlignmentCenter)
        {
            x = _splitterIndentationBottom;
            w -= 2*_splitterIndentationBottom;
        }
        
        CGContextMoveToPoint(context, x, rect.origin.y+rect.size.height-(CELL_LINE_H/2));
        CGContextAddLineToPoint(context, x+w, rect.origin.y+rect.size.height-(CELL_LINE_H/2));
    }
    
    if(_splitterOnDiy) {
        
        x = _splitterIndentationBottomHead;
        w = w - x - _splitterIndentationBottomEnd;
        
        CGContextMoveToPoint(context, x, rect.origin.y+rect.size.height-(CELL_LINE_H/2));
        CGContextAddLineToPoint(context, x+w, rect.origin.y+rect.size.height-(CELL_LINE_H/2));
    }
    
    CGContextClosePath(context);
    CGContextStrokePath(context);
}

- (void)setSplitterOnTop:(BOOL)splitterOnTop {
    _splitterOnTop = splitterOnTop;
    [self setNeedsDisplay];
}

- (void)setSplitterOnBottom:(BOOL)splitterOnBottom {
    _splitterOnBottom = splitterOnBottom;
    [self setNeedsDisplay];
}

- (void)setSplitterIndentationTop:(CGFloat)splitterIndentationTop {
    _splitterIndentationTop = splitterIndentationTop;
    [self setNeedsDisplay];
}

- (void)setSplitterIndentationBottom:(CGFloat)splitterIndentationBottom {
    
    _splitterIndentationBottom = splitterIndentationBottom;
    [self setNeedsDisplay];
}

- (void)setSplitterIndentationBottomHead:(CGFloat)splitterIndentationBottomHead {
    _splitterIndentationBottomHead = splitterIndentationBottomHead;
    [self setNeedsDisplay];
}

- (void)setSplitterIndentationBottomEnd:(CGFloat)splitterIndentationBottomEnd {
    _splitterIndentationBottomEnd = splitterIndentationBottomEnd;
    [self setNeedsDisplay];
}

@end





@interface BaseTableViewCell()
@end

@implementation BaseTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBackgroundView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        [self setBackgroundView];
    }
    return self;
}


- (void)setBackgroundView {
    CellBackgroundView *bgView = [[CellBackgroundView alloc] initWithFrame:CGRectZero];
    bgView.splitterAlignment = SplitterAlignmentRight;
    self.backgroundView = bgView;
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    self.backgroundColor = [UIColor whiteColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.splitterColor = [UIColor colorWithRed:224.0f/255.0f green:228.0f/255.0f blue:229.0f/255.0f alpha:1];
    
    self.clipsToBounds = YES;
    self.enabled = YES;

    _contentInset = UIEdgeInsetsMake(CELL_LINE_H, CELL_PADDING, CELL_LINE_H, CELL_PADDING);
    
    [self configCellUI];
}

- (void)setSplitterColor:(UIColor *)splitterColor {
    _splitterColor = splitterColor;
    CellBackgroundView *cellBgView = (CellBackgroundView *)self.backgroundView;
    cellBgView.splitterColor = splitterColor;
}

#pragma mark - Public Interface

- (void)setTopSeparatorType:(CellSeparatorType)topSeparatorType {
    _topSeparatorType = topSeparatorType;
    CellBackgroundView *cellBgView = (CellBackgroundView *)self.backgroundView;
    cellBgView.splitterOnTop = YES;
    switch (topSeparatorType) {
        case CellSeparatorTypeNone:
            cellBgView.splitterOnTop = NO;
            break;
        case CellSeparatorTypeLine:
            cellBgView.splitterOnTop = YES;
            cellBgView.splitterIndentationTop = 0.0f;
            break;
        case CellSeparatorTypeLineIndent:
            cellBgView.splitterOnTop = YES;
            cellBgView.splitterIndentationTop = CELL_PADDING;
        default:
            break;
    }
}

- (void)setBottomSeparatorType:(CellSeparatorType)bottomSeparatorType {
    _bottomSeparatorType = bottomSeparatorType;
    CellBackgroundView *cellBgView = (CellBackgroundView *)self.backgroundView;
    cellBgView.splitterOnBottom = YES;
    switch (bottomSeparatorType) {
        case CellSeparatorTypeNone:
            cellBgView.splitterOnBottom = NO;
            break;
        case CellSeparatorTypeLine:
            cellBgView.splitterOnBottom = YES;
            cellBgView.splitterIndentationBottom = 0.0f;
            break;
        case CellSeparatorTypeLineIndent:
            cellBgView.splitterOnBottom = YES;
            cellBgView.splitterIndentationBottom = CELL_PADDING;
        case CellSeparatorTypeCenter:
            cellBgView.splitterOnBottom = YES;
            cellBgView.splitterAlignment = SplitterAlignmentCenter;
            cellBgView.splitterIndentationBottom = CELL_PADDING;
        default:
            break;
    }
}

- (void)setBottomSeparatorInsets:(CellBottomInsets)bottomSeparatorInsets {
    _bottomSeparatorInsets = bottomSeparatorInsets;
    self.bottomSeparatorType = CellSeparatorTypeLineIndent;
    CellBackgroundView *cellBgView = (CellBackgroundView *)self.backgroundView;
    cellBgView.splitterOnTop = NO;
    cellBgView.splitterOnBottom = NO;
    cellBgView.splitterOnDiy = YES;
    cellBgView.splitterIndentationBottomHead = bottomSeparatorInsets.left;
    cellBgView.splitterIndentationBottomEnd = bottomSeparatorInsets.right;
}


- (void)setTotalBackgroundColor:(UIColor *)totalBackgroundColor
{
    _totalBackgroundColor = totalBackgroundColor;
    self.backgroundColor = totalBackgroundColor;
    
    NSMutableArray *arr = @[].mutableCopy;
    __weak typeof(arr) weak_arr = arr;
    
    void(^clearBlock)(UIView*) = ^(UIView *v){
        for (UIView *subview in v.subviews) {
            subview.backgroundColor = [UIColor clearColor];
            
            void(^blk)(UIView *)  = weak_arr.firstObject;
            blk(subview);
        }
    };
    
    [arr addObject:clearBlock];
    
    clearBlock(self);
}

- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;
    self.userInteractionEnabled = enabled;
}



- (void)configCellUI {/* 交给子类 */}

@end
