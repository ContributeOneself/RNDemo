//
//  DYIconRichLabelCell.h
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RichLabel.h"

@interface DYIconRichLabelCell : BaseTableViewCell
@property (nonatomic, strong) UIButton  *checkButton;
@property (nonatomic, strong) RichLabel *richLabel;

@property (nonatomic, copy) UIImage * checkNomalImage;
@property (nonatomic, copy) UIImage * checkSelectedImage;
@property (nonatomic, assign) CGSize checkSize; //default w=10 h=10

@property (nonatomic, copy) TapAction checkTapAction;

// 晃动效果
- (void)sloshingHint;

@end
