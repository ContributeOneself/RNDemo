//
//  DYButtonCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYButtonCell.h"

@implementation DYButtonCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat top = self.contentInset.top;
    CGFloat left = self.contentInset.left;
    CGFloat cellHeight = self.contentView.Ex_height - top - self.contentInset.bottom;
    
    CGFloat width = self.contentView.Ex_width - self.contentInset.left - self.contentInset.right;
    _button.frame = CGRectMake(left, self.contentInset.top, width, cellHeight);
}

- (void)configCellUI {
    [super configCellUI];
    
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    _button.backgroundColor = UIColorHexStr(@"a22c26");
    _button.titleLabel.font = FONT_BASE(17);
    [_button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_button addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_button];
}

- (void)buttonClick {
    if(self.buttonTapAction) {
        self.buttonTapAction();
    }
}

- (void)setKey:(NSString *)key {
    [self.button setTitle:key forState:UIControlStateNormal];
}

- (NSString *)key {
    return self.button.titleLabel.text;
}

@end
