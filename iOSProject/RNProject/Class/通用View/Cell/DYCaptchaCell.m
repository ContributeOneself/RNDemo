//
//  DYCaptchaCell.m
//  DragonYin
//
//  Created by srxboys on 2018/4/26.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYCaptchaCell.h"
#import "RXValidator.h"

#import "Constant.h"
#import "UIResponder+APIClient.h"

@interface DYCaptchaCell()
@property (nonatomic, copy) NSTimer *timer;
@property (nonatomic, assign) NSInteger downCount;
@end

@implementation DYCaptchaCell

- (void)configCellUI {
    [super configCellUI];
    self.count = 120;
    self.accessSize = CGSizeMake(75, 24);
    self.accessButton.layer.cornerRadius = self.accessSize.height/2;
    self.accessButton.layer.borderWidth = 0.2;
    self.accessButton.layer.borderColor = COLOR_999.CGColor;
    self.accessButton.clipsToBounds = YES;
    
    self.accessButton.titleLabel.font = FONT_BASE(12);
    [self.accessButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.accessButton setTitleColor:COLOR_333 forState:UIControlStateNormal];
    [self.accessButton setTitleColor:COLOR_999 forState:UIControlStateDisabled];
    
    [self.accessButton setBackgroundImage:[RXHexColor colorBecomeImage:[UIColor clearColor]] forState:UIControlStateNormal];
    [self.accessButton setBackgroundImage:[RXHexColor colorBecomeImage:COLOR_666] forState:UIControlStateDisabled];
    
    self.valueTextField.handler.textFieldShouldBeginEditingBlock = ^BOOL(UITextField *textField) {
        if(![RXValidator isValidNum:textField.text]) {
            textField.text = @"";
        }
        return YES;
    };
    
    self.valueTextField.handler.textFieldShouldChangeCharactersInRangeReplacementStringBlock = ^BOOL(UITextField *textField, NSRange range, NSString *string) {
        if([RXValidator isValidNum:string] || string.length == 0) {
            return YES;
        }
        return NO;
    };
    
}

- (void)accessButtonClick {
    [self endEditing:YES];
    
    if(self.accessAction) {
        self.accessAction();
    }
    
}

- (void)nomalButton {
    self.accessButton.enabled = YES;
}

- (void)selectButton {
    self.accessButton.enabled = NO;
}

- (void)sendCaptchaCodeError {
    [self stopTimer];
}

- (void)startTimer {
    [self stopTimer];
    [self selectButton];
    self.downCount = self.count;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeCount) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer {
    [_timer invalidate];
    _timer = nil;
}

- (void)changeCount {
    if(self.downCount<0) {
        [self stopTimer];
        [self nomalButton];
    }
    else {
        NSString * title = STRING_FORMAT(@"%@ %zd", self.catchaText, self.downCount);
        [self.accessButton setTitle:title forState:UIControlStateDisabled];
        self.downCount --;
    }
}

- (NSString *)catchaText {
    if(!StrBool(_catchaText)) {
        _catchaText = @"等待";
    }
    return _catchaText;
}


- (void)sendMessageForPhone:(NSString *)phone {
    phone = StrFormatWhiteSpace(phone);
    if(phone.length == 0) return;
    
    self.accessButton.enabled = NO;
    [self startTimer];
    
    NSDictionary * paramsDict = @{ @"phone" : phone };
    
    
    weak(weakSelf);
    [self RequestType:RequestGet method:API_MESSAGE_SEND className:nil paramsDict:paramsDict successBlock:^(Response *responseObject) {
        
        [weakSelf showToash:responseObject.message];
        
        //发送成功的验证码倒计时不关闭
        if(!responseObject.status) {
            [weakSelf stopTimer];
            return;
        }
        
        
    } failureBlock:^(NSError *error) {
        [weakSelf stopTimer];
    }];
}

- (void)showToash:(NSString *)message {
    if(!StrBool(message)) return;
    UIViewController * c = [[PageRouter sharedPageRouter] currentController];
    if(![c isKindOfClass:[UIViewController class]]) return;
    UIView * navView = c.view;
    if(c.navigationController) {
        navView = c.navigationController.view;
    }
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
    [hud showHudLabel:NonEmptyString(message)];
}


@end
