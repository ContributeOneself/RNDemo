//
//  DYHomeViewController.m
//  DragonYin
//
//  Created by srxboys on 2018/4/22.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYHomeViewController.h"
#import "BaseNavViewController.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

@interface DYHomeViewController ()

@end

@implementation DYHomeViewController

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = YES;
    [self setImgViewBGType:BG_TypeNULL];
    
    [self configUI];
}

- (void)configUI {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self download];
    });
    
}

- (void)download {
    NSString * urlString = @"https://gitee.com/ContributeOneself/RNJsDemo/raw/master/jsBundleModel/yooli.jsbundle";
    weak(weakSelf);
    [self DownloadWithURL:urlString successBlock:^(NSString *targetPath) {
        if(targetPath.length > 0) {
            [weakSelf openRN:targetPath];
        }
    } failureBlock:^(NSString *downloadPath, NSError *error) {
        [weakSelf showToash:error.description];
    }];
}

- (void)openRN:(NSString *)path {
    NSLog(@"path=\n%@", path);
    NSURL * jsCodeLocation = [NSURL fileURLWithPath:path];
    RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                        moduleName:@"RNDemo"
                                                 initialProperties:nil
                                                     launchOptions:nil];
    rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
    BaseViewController * c = [BaseViewController new];
    c.view = rootView;
    [self.navigationController pushViewController:c animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
