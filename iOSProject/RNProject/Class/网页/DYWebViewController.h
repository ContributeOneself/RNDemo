//
//  DYWebViewController.h
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController.h"
#import <WebKit/WebKit.h>

/// 3D Touch peek 的处理 block
typedef void(^PreviewActionReadBlock)(void);


@interface DYWebViewController : BaseViewController<WKNavigationDelegate>
@property (nonatomic, strong) WKWebView * webView;
@property (nonatomic, copy) NSString * urlString;
@property (nonatomic, strong, readonly) NSURLRequest * request;

- (void)loadRequest;

/// 3D Touch peek 的处理 block
@property (nonatomic, copy) PreviewActionReadBlock previewActionReackBlock;
@property (nonatomic, assign) BOOL isRead; //标识已读
@end
