//
//  DYWebViewController.m
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYWebViewController.h"
#import "NSHTTPCookie+DYExtenstion.h"

#import "WebViewJavascriptBridge.h"
#import "DYUserCache.h"
#import <WXApi.h>

#define WEB_KVO_PROGRESS @"estimatedProgress"
#define WEB_KVO_TITLE    @"title"

typedef NS_ENUM(NSInteger, URLType) {
    URLTypeHttp,
    URLTypeHttps
};

@interface DYWebViewController ()
{
    UIProgressView *_progressView;
    URLType _URLType;
}
@property (nonatomic, copy) WebViewJavascriptBridge* bridge;

@end

@implementation DYWebViewController

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    _webView.frame = self.contentView.bounds;
    _webView.Ex_height = _webView.Ex_height - STATUS_BAR_HEIGHT+22;
    _progressView.frame = CGRectMake(0, 0, _webView.Ex_width, 2);
}

- (void)networkReloadDataButtonClick {
    if(UrlBool(_webView.URL.absoluteString)) {
        [_webView reload];
    }
    else {
        NSURL * url = [NSURL URLWithString:self.urlString];

        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15];
        [_webView loadRequest:request];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setImgViewBGType:BG_TypeNULL];
    
    [self configUI];
}
- (void)barRightBtnClick:(NSUInteger)index {
    if ([WXApi isWXAppInstalled]) {
        
        WXMediaMessage *message = [WXMediaMessage message];
        message.title = @"title";
        message.description = @"description";
        [message setThumbImage:[UIImage imageNamed:@"img_logoShare"]];
        WXWebpageObject * webpageObject = [WXWebpageObject object];
        webpageObject.webpageUrl = self.urlString;
        message.mediaObject = webpageObject;
        
        SendMessageToWXReq *rep = [[SendMessageToWXReq alloc]init];
        rep.bText = NO;
        rep.message = message;
        rep.scene = WXSceneSession;
        [WXApi sendReq:rep];
        
    }else{
        [self showToash:@"您的设备未安装微信"];
    }
}
- (void)configUI {
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc]init];
    // 设置偏好设置
    config.preferences = [[WKPreferences alloc]init];
    // 默认为0
    config.preferences.minimumFontSize = 10;
    // 默认认为YES
    config.preferences.javaScriptEnabled = YES;
    // 在iOS上默认为NO，表示不能自动通过窗口打开
    config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
    
    //#其实我们没有必要去创建它，因为它根本没有属性和方法：
    // web内容处理池，由于没有属性可以设置，也没有方法可以调用，不用手动创建
    config.processPool = [[WKProcessPool alloc]init];
    
    // 通过JS与webview内容交互
    config.userContentController = [[WKUserContentController alloc]init];
    
    _webView = [WKWebView new];
    _webView.opaque = NO;
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.scrollView.bounces = NO;
    _webView.navigationDelegate = self;
    [self.contentView addSubview:_webView];

    
    _progressView = [UIProgressView new];
    [_progressView setTintColor:UIColorRGB(127, 16, 132)];
    [_progressView setTrackTintColor:UIColorHexStr(@"#f5f5f5")];
    _progressView.progress = 0;
    [self.contentView addSubview:_progressView];
    
    [WebViewJavascriptBridge enableLogging];
    _bridge = [WebViewJavascriptBridge bridgeForWebView:_webView];
    [_bridge setWebViewDelegate:self];
    
    /// kvo web 进度
    [_webView addObserver:self
               forKeyPath:WEB_KVO_PROGRESS
                  options:NSKeyValueObservingOptionNew
                  context:nil];
    [_webView addObserver:self forKeyPath:WEB_KVO_TITLE options:NSKeyValueObservingOptionNew context:NULL];
    
    
    [_bridge registerHandler:@"downloadPath" handler:^(id data, WVJBResponseCallback responseCallback) {
        RXLog(@"testObjcCallback called: %@", data);
        
        responseCallback(@"Response from testObjcCallback");
    }];
    
    NSURL * url = [NSURL URLWithString:self.urlString];
    if(url) {
        RXLog(@"详情页的 webView_url=%@", self.urlString);
        _URLType = URLTypeHttp;
        if([url.scheme isEqualToString:@"https"]) {
            _URLType = URLTypeHttps;
        }
        _request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:15];
    }
    [self loadRequest];
}

- (void)loadRequest {
    [NSHTTPCookie setCookieName:@"userId" value:USER_INFO.userId];
    [NSHTTPCookie setCookieName:@"token" value:USER_INFO.token];
    [_webView loadRequest:self.request];
}

#pragma mark - ******************** webView KVO ***********************
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    
    if ([keyPath isEqualToString:WEB_KVO_PROGRESS]) {
        [_progressView setProgress:_webView.estimatedProgress animated:YES];
        _progressView.hidden = NO;
    }
    else if ([keyPath isEqualToString:WEB_KVO_TITLE]){
        self.title = change[@"new"];
    }
    
    // 加载完成
    if (!_webView.loading) {
        _progressView.alpha = 0;
        _progressView.hidden = YES;
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    WKNavigationActionPolicy policy = WKNavigationActionPolicyAllow;
    NSURL * url = navigationAction.request.URL;
    [self hiddenNullNetworkView];
    if([url.absoluteString isEqualToString:@"native_start_bank"]) {
        decisionHandler(WKNavigationActionPolicyCancel);
        //跳转
        return;
    }

    if(navigationAction.navigationType == WKNavigationTypeLinkActivated) {
        
        //WebKit框架对跨域进行了安全性检查限制，不允许跨域，比如从一个 HTTP 页对 HTTPS 发起请求是无效的（有一个界面要跳到支付宝页面去支付，死活没反应）。而系统的 Safari ，iOS 10出现的 SFSafariViewController 都是支持跨域的
        if([url.scheme isEqualToString:@"https"] && _URLType == URLTypeHttp) {
            APP_OPEN_URL(url);
            policy = WKNavigationActionPolicyCancel;
        }
        else if([url.scheme isEqualToString:@"http"] && _URLType == URLTypeHttps){
            APP_OPEN_URL(url);
            policy = WKNavigationActionPolicyCancel;
        }
    }

    decisionHandler(policy);
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    RXLog(@"didStartProvisionalNavigation");
    _progressView.hidden = NO;
    STATUS_BAR_START();
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    _progressView.hidden = YES;
    STATUS_BAR_STOP();
    [self showNullNetworkView];
}

- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    RXLog(@"didReceiveServerRedirectForProvisionalNavigation=");
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    _progressView.hidden = YES;
    STATUS_BAR_STOP();
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    RXLog(@"加载完成");
    STATUS_BAR_STOP();
    _progressView.alpha = 0;
    _progressView.hidden = YES;
    //    [self printWebSouceCode];
    
    NSArray * cookieArray= [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies;
    for (NSHTTPCookie * cookie in cookieArray) {
        NSLog(@"\ncookie=%@", cookie);
    }
}

- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    RXLog(@"webView 内存暴涨了");
}


#pragma mark - ~~~~~~~~~~~ /// 显示html 源码 ~~~~~~~~~~~~~~~
- (void)printWebSouceCode {
    //获取header源码1
    NSString *JsToGetHTMLSource = @"document.getElementsByTagName('html')[0].innerHTML";
    [_webView evaluateJavaScript:JsToGetHTMLSource completionHandler:^(id _Nullable obj, NSError * _Nullable error) {
        if(!error) {
            RXLog(@"\n%@\n\n",obj);
        }
        else {
            RXLog(@"printWebSouceCode error=%@", error.description);
        }
    }];
}





- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    //    RXLog(@"\n alert========= \n");
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
    
}
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    //    RXLog(@"\n ConfirmPanelWithMessage========= \n");
    //    DLOG(@"msg = %@ frmae = %@",message,frame);
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }])];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    RXLog(@"\n TextInputPanel========= \n");
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"完成" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    RXLog(@"didReceiveAuthenticationChallenge");
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        
        RXLog(@"didReceiveAuthenticationChallenge serverTrust******");
        NSURLCredential *card = [[NSURLCredential alloc]initWithTrust:challenge.protectionSpace.serverTrust];
        
        completionHandler(NSURLSessionAuthChallengeUseCredential,card);
    }
}






//向上拖动弹出视图的操作
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    weak(weakSelf);
    UIPreviewAction *itemRead = [UIPreviewAction actionWithTitle:@"标识已读" style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        // 标识已读操作
        if(weakSelf.previewActionReackBlock) {
            weakSelf.previewActionReackBlock();
        }
    }];
    
    UIPreviewAction *itemCancel = [UIPreviewAction actionWithTitle:@"取消" style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        // 取消操作
    }];
    
    if(_isRead) {
        //对于消息已经已读的
        return @[itemCancel];
    }
    return @[itemRead,itemCancel];
}


- (void)dealloc {
    [_webView removeObserver:self forKeyPath:WEB_KVO_PROGRESS];
    [_webView removeObserver:self forKeyPath:WEB_KVO_TITLE];
}


@end
