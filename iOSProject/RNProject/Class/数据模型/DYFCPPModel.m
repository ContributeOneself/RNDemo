//
//  DYFCPPModel.m
//  DragonYin
//
//  Created by srxboys on 2018/5/3.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYFCPPModel.h"

#pragma mark - Face++身份证数据模型
@implementation FCIDCardModel
- (instancetype)initWithDict:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        BOOL isBack = [dic[@"side"] isEqualToString:@"back"];
        if(isBack) {
            _issuedBy = dic[@"issued_by"];
            _validDate = dic[@"valid_date"];
        }
        else {
            _type = [dic[@"type"] intValue];
            _name = dic[@"name"];
            _gender = dic[@"gender"];
            _race = dic[@"race"];
            _birthday = dic[@"birthday"];
            _address = dic[@"address"];
            _idCardNumber = dic[@"id_card_number"];
        }
        _side = isBack?FCCP_Side_Back:FCCP_Side_Front;
    }
    return self;
}

+ (instancetype)fcppIDCardModelWithCardsDict:(NSDictionary *)cardsDict {
    if(DictBool(cardsDict))
        return [[FCIDCardModel alloc] initWithDict:cardsDict];
    return nil;
}
@end

#pragma mark - Face++银行卡数据模型
@implementation FCBankCardModel
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

+ (instancetype)fcppBankCardModelWithDict:(NSDictionary *)dict {
    if(DictBool(dict))
        return [[FCBankCardModel alloc] initWithDict:dict];
    return nil;
}
@end
