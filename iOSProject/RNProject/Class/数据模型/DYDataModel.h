//
//  DYDataModel.h
//  DragonYin
//
//  Created by srxboys on 2018/4/22.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface BaseModel : NSObject <NSCoding,NSCopying>
@end

/*
//      复杂的数据模型，定义使用
//@protocol BankAllModel;  //1
//@interface DYDataModel : BaseModel
//@property (nonatomic, strong) NSArray <BankAllModel>* content; //2
//@end
 */

/// 【用户】数据模型
@interface DYUserModel : BaseModel
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *username;//登录名
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nationality; //国籍
@property (nonatomic, copy) NSString *nativePlace; //籍贯
@property (nonatomic, copy) NSString *gender; //性别
@property (nonatomic, copy) NSString *ethnic; //民族
@property (nonatomic, copy) NSString *birthday; //出生日期1987-08-01
@property (nonatomic, copy) NSString *tel;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *head;  //头像
@property (nonatomic, copy) NSString *state;
@property (nonatomic, copy) NSString *role;  //用户类型（1：系统管理员、2：银行:3：供应商、4：开户用户）
@property (nonatomic, copy) NSString *idnum; //身份证

#warning mark -下面的有吗? 需要验证
//@property (nonatomic, copy) NSString *password;//密码
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *updateDate;
@property (nonatomic, copy) NSString *email;
//@property (nonatomic, copy) NSString *phone; //不知道
//@property (nonatomic, copy) NSString * use;
//@property (nonatomic, copy) NSString *card;    //身份证
@end

/// 【银行卡】数据模型
@interface DYBankModel : BaseModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *updateDate; //记录更新时间2016-08-01 12:24:36
@property (nonatomic, copy) NSString *bankId; //银行ID
@property (nonatomic, copy) NSString *bankName; //银行名称
@property (nonatomic, copy) NSString *logo; //银行logo
@property (nonatomic, copy) NSString *bindingbankname;//绑定银行名称
@property (nonatomic, copy) NSString *cardNumber;//绑定的银行卡号
@property (nonatomic, copy) NSString *phone; //手机号
@property (nonatomic, copy) NSString *userId; //用户ID
@property (nonatomic, copy) NSString *isPlatformAgreement; //是否签署了平台协议书
@property (nonatomic, copy) NSString *name; //姓名
@property (nonatomic, copy) NSString *nationality; //国籍
@property (nonatomic, copy) NSString *nativePlace; //籍贯
@property (nonatomic, copy) NSString *card; //身份证
@property (nonatomic, copy) NSString *gender;//性别
@property (nonatomic, copy) NSString *ethnic;//民族
@property (nonatomic, copy) NSString *birthday; //出生日期2016-08-01
@property (nonatomic, copy) NSString *address; //住址
@property (nonatomic, copy) NSString *deliveryAddress;//配送地址
@property (nonatomic, copy) NSString *status; //配送状态（1:代配送、2:已配送）
@property (nonatomic, copy) NSString *logisticsCompany;//物流公司
@property (nonatomic, copy) NSString *shipmentNumber;//物流单号
@property (nonatomic, copy) NSString *cardFrontPic;//身份证证图片正面
@property (nonatomic, copy) NSString *cardFollowingPic;//身份证图片背面
@property (nonatomic, copy) NSString *bankCardPic; //银行卡图片
@property (nonatomic, copy) NSString *headPic; //大头贴

#warning mark -下面的有吗? 需要验证
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *approveDate;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *replacementCertificateDate;

//接口文档没有的字段
@property (nonatomic, copy) NSString *background;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *legalPerson;
@property (nonatomic, copy) NSString *otherName;
@property (nonatomic, copy) NSString *registeredCapital;
@end



/// 【籍贯】数据模型
@interface DYGetOrigin : BaseModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *updateDate;
@property (nonatomic, copy) NSString *parentId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *shortName;
@end








