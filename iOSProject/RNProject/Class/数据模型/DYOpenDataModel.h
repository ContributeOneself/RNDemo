//
//  DYOpenDataModel.h
//  DragonYin
//
//  Created by 王亚军 on 2018/5/14.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYUserCache.h"

@interface DYOpenDataModel : DYUserCache

@property (nonatomic, copy) NSString *username;//登录名

@property (nonatomic, strong) UIImage *frontImage;//身份证正面
@property (nonatomic, strong) UIImage *backImage;//身份证背面

@property (nonatomic, copy) NSString *frontUrl;//身份证正面
@property (nonatomic, copy) NSString *backUrl;//身份证背面

@property (nonatomic, copy) NSString *cardNumber;//绑定旧的银行卡号




@end
