//
//  DYUser.m
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYUserCache.h"
#import "Constant.h"
#import "DYDataModel.h"
#import "SCMApiClient.h"

#import "DYCache.h"
#import "UIImageView+fadeInFadeOut.h"
#import "MJExtension.h"
NSNotificationName const DYUserUdateNotification = @"DragonYin_UserInfo_chage";

NSString * const USER_CACHE_FILE_NAME  = @"DragonYin_cache";
NSString * const USER_CACHE_KEY  = @"DragonYin_key";

@interface DYUserCache()

//这个容易出问题，还是手动处理模型吧
//@property (nonatomic, strong) NSMutableArray       *userModelNameList;
//@property (nonatomic, strong) NSMutableDictionary  *keyPathObserverMap;
//@property (nonatomic, strong) RXMessageBlackHole   *messageBlackHole;

@property (nonatomic, strong) UIResponder * bankAppleAPI;
@property (nonatomic, strong) DYCache * userCache;
@property (nonatomic, strong) UIImageView * headImgView;
@end

@implementation DYUserCache
@synthesize userModel = _userModel;

+ (instancetype)shareInstance {
    static DYUserCache * user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[DYUserCache alloc] initConfig];
    });
    return user;
}

- (instancetype)initConfig
{
    self = [super init];
    if (self) {
        _bankAppleAPI = [UIResponder new];
        _userCache = [DYCache cacheWithFilename:USER_CACHE_FILE_NAME];
        _headImgView = [UIImageView new];
        [self unSynchronize];
        [self updateBankListAPI];
    }
    return self;
}


- (void)update:(DYUserModel *)userModel {
    _userModel = userModel;
    [self postNotifactionAndSyn];
}

- (void)postNotifactionAndSyn {
    [self synchronize];
    NOTI_POST(DYUserUdateNotification);
}

- (void)synchronize {
    if(StrBool(self.userId)) {
        [self.userCache setObject:[self.userModel mj_JSONString] forKey:USER_CACHE_KEY encrypted:YES];
        [self.headImgView sd_setImageFIFOWithURL:[NSURL URLWithString:self.userModel.head] completed:nil];
    }
}

- (void)unSynchronize {
    NSString * userModelJSON = [self.userCache objectForKey:USER_CACHE_KEY encrypted:YES];
    _userModel = [DYUserModel mj_objectWithKeyValues:userModelJSON];
}

- (void)logout {
    _userModel = nil;
    [self.userCache removeObjectForKey:USER_CACHE_KEY];
    NOTI_POST(DYUserUdateNotification);
}


- (void)updateBankListAPI {
    if(!StrBool(self.userId)) return;

    weak(weakSelf);
    NSDictionary * paramsDict = @{@"userID": self.userId};
    [self.bankAppleAPI RequestType:RequestGet method:API_BANK_APPLY_LIST className:[DYBankModel class] paramsDict:paramsDict successBlock:^(Response *responseObject) {
        if(!responseObject.status) return;
        id obj = responseObject.content;
        if(ArrBool(obj)) {
            @try {
                weakSelf.bandAppleArray = obj;
            } @catch (NSException *exception) {
                RXLog(@"updateBankListAPI error");
            } @finally {
                weakSelf.bandAppleArray = nil;
            }
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

#pragma mark - GET

- (DYUserModel *)userModel {
    if(_userModel == nil || ![_userModel isKindOfClass:[DYUserModel class]]) {
        _userModel = [[DYUserModel alloc] init];
    }
    return _userModel;
}

- (NSString *)userId {
    return self.userModel.userId;
}

- (NSString *)token {
    return self.userModel.token;
}

- (NSString *)name {
    return self.userModel.name;
}

- (NSString *)mobile {
    return self.userModel.mobile;
}

- (NSString *)tel {
    return self.userModel.tel;
}

- (NSString *)address {
    return self.userModel.address;
}

- (NSString *)head {
    [self.headImgView sd_setImageFIFOWithURL:[NSURL URLWithString:self.userModel.head] completed:nil];
    return self.userModel.head;
}
- (BOOL)isHead {
    if(self.headImgView.image) return YES;
    return NO;
}

- (NSString *)state {
    return self.userModel.state;
}

- (NSString *)role {
    return self.userModel.role;
}

- (NSString *)nativePlace {
    return self.userModel.nativePlace;
}

- (NSString *)nationality {
    return self.userModel.nationality;
}

- (NSString *)gender {
    return self.userModel.gender;
}

- (NSString *)ethnic {
    return self.userModel.ethnic;
}

- (NSString *)birthday {
    return self.userModel.birthday;
}

- (NSString *)idnum {
    return self.userModel.idnum;
}

- (NSString *)email {
    return self.userModel.email;
}

@end
