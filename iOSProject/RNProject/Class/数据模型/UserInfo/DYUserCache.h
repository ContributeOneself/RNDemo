//
//  DYUser.h
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>

#define USER_INFO [DYUserCache shareInstance]

@class DYUserModel;
@class DYBankModel;

@interface DYUserCache : NSObject
/**  标识，不可以被外部调用 */
- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

+ (instancetype)shareInstance;
@property (nonatomic, copy, readonly) NSString *userId;
@property (nonatomic, copy, readonly) NSString *token;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *mobile;
@property (nonatomic, copy, readonly) NSString *tel;
@property (nonatomic, copy, readonly) NSString *address;
@property (nonatomic, copy, readonly) NSString *head; //头像
@property (nonatomic, assign, readonly) BOOL isHead; //头像
@property (nonatomic, copy, readonly) NSString *state;
@property (nonatomic, copy, readonly) NSString *role; //用户类型（1：系统管理员、2：银行:3：供应商）
@property (nonatomic, copy, readonly) NSString *nativePlace;
@property (nonatomic, copy, readonly) NSString *nationality;
@property (nonatomic, copy, readonly) NSString *gender;
@property (nonatomic, copy, readonly) NSString *ethnic;
@property (nonatomic, copy, readonly) NSString *birthday;
@property (nonatomic, copy, readonly) NSString *idnum;
@property (nonatomic, copy, readonly) NSString *email;

@property (nonatomic, strong) NSMutableArray <DYBankModel *>*bandAppleArray;

@property (nonatomic, strong, readonly) DYUserModel * userModel;

- (void)logout;

/* model -> update  DYUser -> postNoti -> save local -> */
- (void)update:(DYUserModel *)userModel;

/* postNoti -> save local -> */
- (void)postNotifactionAndSyn;

/* update  */
- (void)updateBankListAPI;

/* save cache */
- (void)synchronize;

/* get cache */
- (void)unSynchronize;

@end

UIKIT_EXTERN NSNotificationName const DYUserUdateNotification;
