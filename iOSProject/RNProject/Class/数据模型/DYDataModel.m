//
//  DYDataModel.m
//  DragonYin
//
//  Created by srxboys on 2018/4/22.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYDataModel.h"

#pragma mark - 基类数据模型
@implementation BaseModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{ @"ID" : @"id"};
}
MJExtensionCodingImplementation
MJExtensionLogAllProperties

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    BaseModel* newTestObject = [[[self class] allocWithZone:zone] init];
    return newTestObject;
}
@end

#pragma mark - 用户数据模型
@implementation DYUserModel
@end




#pragma mark - 银行卡数据模型
@implementation DYBankModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{ @"ID"               : @"id",
              @"createDate"       : @"create_date",
              @"updateDate"       : @"update_date",
              @"legalPerson"      : @"legal_person",
              @"otherName"        : @"other_name",
              @"registeredCapital": @"registered_capital",
              };
}

@end


#pragma mark - 籍贯数据模型
@implementation DYGetOrigin

@end
