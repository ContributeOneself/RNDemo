//
//  DYFCPPModel.h
//  DragonYin
//
//  Created by srxboys on 2018/5/3.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//
//
/*
                Face++ model


        由于接口 返回的数据复杂、没有规律，就手动解析
 
        数据模型的属性 都是我所要的，不要的基本都没有获取
 
 */


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



typedef NS_ENUM(NSInteger, FCCP_Side) {
    FCCP_Side_Front = 0,
    FCCP_Side_Back,
};




#pragma mark -------------------------------【 Face ++  】------------------------------------------------
#pragma mark - Face++ 身份证
/// (1)Face++身份证数据模型 *****  *****
@interface FCIDCardModel: NSObject

@property (nonatomic, assign) int type; //证件类型。返回1，代表是身份证。
@property (nonatomic, copy) NSString *name; //姓名
@property (nonatomic, copy) NSString *imageId;
@property (nonatomic, copy) NSString *address;  //住址
@property (nonatomic, copy) NSString *birthday; //出生日期2016-08-01
@property (nonatomic, copy) NSString *gender;   //性别
@property (nonatomic, copy) NSString *idCardNumber; //身份证号
@property (nonatomic, copy) NSString *race; //民族
@property (nonatomic, assign) FCCP_Side side; //表示身份证的国徽面或人像面。返回值为：front: 人像面、back: 国徽面
@property (nonatomic, copy) NSString *issuedBy; //签发机关
@property (nonatomic, copy) NSString *validDate; //有效日期，返回值有两种格式：一个16位长度的字符串：YYYY.MM.DD-YYYY.MM.DD【或是】：YYYY.MM.DD-长期(2010.11.13-2020.11.13)

/// 解析json 返回给 身份证数据模型(却哪里的值？  info[cards][?]  )
+ (instancetype)fcppIDCardModelWithCardsDict:(NSDictionary *)cardsDict;
@end



/// (2)Face++银行卡数据模型 *****  *****
#pragma mark - Face++ 银行卡
@interface FCBankCardModel : NSObject
@property (nonatomic, copy) NSString *imageId;
@property (nonatomic, copy) NSString *bankCardNumber;
@end
