//
//  NavigationBar.m
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "NavigationBar.h"

#define NAV_MIN_WIDTH 70

typedef NS_ENUM(NSInteger, BarPosition) {
    BarPositionLeft = 0,
    BarPositionRight = 1
};

@interface NavigationBar()
@property (nonatomic, strong) UIImageView * backImgView;
@property (nonatomic, strong) UIView * contentView;
@property (nonatomic, strong) UIImageView * titleImgView;

@property (nonatomic, strong) UIButton * backButton;
@property (nonatomic, strong) UIView * barLeftButtonView;
@property (nonatomic, strong) NSMutableArray <NSValue*>* barLeftImagesSizeArray;

@property (nonatomic, strong) UIView * barRightButtonView;
@property (nonatomic, strong) NSMutableArray <NSValue*>* barRightImagesSizeArray;

@end

@implementation NavigationBar

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT);
    self.backImgView.frame = self.bounds;
    
    CGFloat navHeight = 44;
    _contentView.frame = CGRectMake(0, STATUS_BAR_HEIGHT, SCREEN_WIDTH, navHeight);
    
    CGFloat space = 5;
    CGFloat backLeft = 22+14+space;
    CGFloat left = backLeft;
    CGFloat maxLeft = (SCREEN_WIDTH-NAV_MIN_WIDTH)/2;
    CGFloat maxRight = maxLeft;
    
    //返回
#warning mark - 默认应为设计的尺寸
    _backButton.frame = CGRectMake(0, (navHeight-20)/2, backLeft, 20);
    
    CGFloat leftBtnX = 0;
    for(NSInteger i = 0; i < self.barLeftButtonView.subviews.count; i++) {
        if(left > maxLeft) break;
        
        UIButton * btn = self.barLeftButtonView.subviews[i];
        if(![btn isKindOfClass:[UIButton class]]) continue;
//        btn.backgroundColor = UIColorRandom;
        NSValue *value = self.barLeftImagesSizeArray[i];
        CGSize size = value.CGSizeValue;
        if(CGSizeEqualToSize(size, CGSizeZero)) {
            size = self.barItemSize;
        };
        
        if(left + size.width >=  maxLeft) {
            CGFloat min = maxLeft - left;
            if(min > 0) {
                size.width = min;
            }
            else {
                break;
            }
        }
        if(btn.currentImage) {
            CGFloat top = (navHeight - size.height)/2;
            btn.frame = CGRectMake(leftBtnX, top, size.width, navHeight);
            left += size.width;
            leftBtnX += size.width;
        }
        else if(StrBool(btn.currentTitle)){
            /*
            CGRect baseFrame = CGRectMake(0, 0, CGFLOAT_MAX, navHeight);
            CGFloat width = ceilf([btn.titleLabel textRectForBounds:baseFrame limitedToNumberOfLines:1].size.width);
            if(width > self.barItemSize.width) {
                width = self.barItemSize.width;
            }
             */
            btn.frame = CGRectMake(leftBtnX, 0, size.width, navHeight);
            left += size.width;
            leftBtnX += size.width;
        }
    }
    self.barLeftButtonView.frame = CGRectMake(backLeft, 0, left, navHeight);
    
    CGFloat right = 0;
    CGFloat rightBtnX = 0;
    for(NSInteger i = 0; i < self.barRightButtonView.subviews.count ; i++) {
        if(right>maxRight) break;
        
        UIButton * btn = self.barRightButtonView.subviews[i];
        if(![btn isKindOfClass:[UIButton class]]) continue;
        NSValue *value = self.barRightImagesSizeArray[i];
        CGSize size = value.CGSizeValue;
        if(CGSizeEqualToSize(size, CGSizeZero)) {
            size = self.barItemSize;
        };
        
        if(right + size.width >= maxRight) {
            CGFloat min = maxRight - right;
            if(min > 0) {
                size.width = min;
            }
            else {
                break;
            }
        }
    
        right += size.width;

//        btn.backgroundColor = UIColorRandom;
        
        if(btn.currentImage) {
            CGFloat top = (navHeight - size.height)/2;
            btn.frame = CGRectMake(rightBtnX, top, size.width, navHeight);
            rightBtnX += right;
        }
        else if(StrBool(btn.currentTitle)){
            /*
             CGRect baseFrame = CGRectMake(0, 0, CGFLOAT_MAX, navHeight);
             CGFloat width = ceilf([btn.titleLabel textRectForBounds:baseFrame limitedToNumberOfLines:1].size.width);
             if(width > self.barItemSize.width) {
             width = self.barItemSize.width;
             }
             */
            btn.frame = CGRectMake(rightBtnX, 0, size.width, navHeight);
            rightBtnX += size.width;
        }
        else {
            right -= size.width;
        }
    }
    self.barRightButtonView.frame = CGRectMake(SCREEN_WIDTH-right, 0, right, navHeight);
    
    left = MAX(left, right);
    CGFloat maxTitleWidth = SCREEN_WIDTH - (left * 2);
    
    if(self.titleImgView) {
        CGFloat titleWidth = 0;
        if(StrBool(self.titleLabel.text) || StrBool(self.titleLabel.attributedText.string)) {
            titleWidth = ceilf([self.titleLabel textRectForBounds:CGRectMake(0, 0, CGFLOAT_MAX, navHeight) limitedToNumberOfLines:1].size.width);
            CGFloat titleLeft = 0;
            CGFloat totalWidth = self.titleImgView.size.width + 5;
            if(totalWidth + titleLeft > maxTitleWidth) {
                titleWidth = maxTitleWidth - totalWidth;
            }
            else {
                titleLeft = (maxTitleWidth - totalWidth - titleWidth)/2;
            }
            
            left += titleLeft;
            self.titleLabel.frame = CGRectMake(left, 0, titleWidth, navHeight);
            left = left + titleWidth + space;
        }
        CGFloat top = (navHeight - self.titleImgView.size.height)/2;
        if(titleWidth == 0) {
            left = left + (maxTitleWidth - self.titleImgView.size.width)/2;
        }
        self.titleImgView.frame = CGRectMake(left, top, self.titleImgView.size.width, self.titleImgView.size.height);
    }
    else {
      self.titleLabel.frame = CGRectMake(left, 0, maxTitleWidth, navHeight);
    }
}

#pragma mark - alloc self
+ (instancetype)shareInstance {
    return [[NavigationBar alloc] initConfig];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // xib storyboard
    [self configUI];
}

- (instancetype)initConfig
{
    self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
    if (self) {
       [self configUI];
    }
    return self;
}

- (void)configUI {
    self.clipsToBounds = YES;

    // default
    _barTintColor = nil;
    _barItemSize = CGSizeMake(60, 23);
    _barItemTitleColor = [UIColor whiteColor];
    _barLeftImagesSizeArray = [NSMutableArray new];
    _barRightImagesSizeArray = [NSMutableArray new];
    
    _backImgView = [UIImageView new];
    [self addSubview:_backImgView];
    self.barTintImage = [UIImage imageNamed:@"img_bg"];
    
    // addSub
    _contentView = [UIView new];
    [self addSubview:_contentView];
    
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    _backButton.backgroundColor = [UIColor purpleColor];
    [_backButton setImage:[UIImage imageNamed:@"img_nav_left"] forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(barBack) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:_backButton];
    
    _titleLabel = [UILabel new];
//    _titleLabel.backgroundColor = [UIColor yellowColor];
    _titleLabel.font = [UIFont boldSystemFontOfSize:18];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_contentView addSubview:_titleLabel];
    
    _barLeftButtonView = [UIView new];
//    _barLeftButtonView.backgroundColor = [UIColor cyanColor];
    [_contentView addSubview:_barLeftButtonView];
    
    _barRightButtonView = [UIView new];
//    _barRightButtonView.backgroundColor = [UIColor orangeColor];
    [_contentView addSubview:_barRightButtonView];
}

#pragma mark - config button
- (UIButton *)configButtonOfBarPosition:(BarPosition)barPosition {
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    SEL btnClickSEL;
    if(barPosition == BarPositionLeft) {
        btnClickSEL = @selector(barLeftClick:);
        [self.barLeftButtonView addSubview:btn];
    }
    else {
        btnClickSEL = @selector(barRightClick:);
        [self.barRightButtonView addSubview:btn];
    }
    [btn addTarget:self action:btnClickSEL forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

- (UIButton *)configButtonTitle:(NSString *)title color:(UIColor *)color barPosition:(BarPosition)barPosition{
    UIButton * btn = [self configButtonOfBarPosition:barPosition];
    btn.titleLabel.font = [UIFont systemFontOfSize:18];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    return btn;
}

- (UIButton *)configButtonImage:(UIImage *)image barPosition:(BarPosition)barPosition {
    UIButton * btn = [self configButtonOfBarPosition:barPosition];
    [btn setImage:image forState:UIControlStateNormal];
    return btn;
}

#pragma mark - navigationBar click
- (void)barBack {
    if([self.delegate respondsToSelector:@selector(navigationBarBackClick)]) {
        [self.delegate navigationBarBackClick];
    }
}

- (void)barLeftClick:(UIButton *)btn {
    if(![self.barLeftButtonView containsSubView:btn]) return;
    NSUInteger tag = [self.barLeftButtonView.subviews indexOfObject:btn];
    
    if([self.delegate respondsToSelector:@selector(navigationBarLeftClick:)]) {
        [self.delegate navigationBarLeftClick:tag];
    }
    
}

- (void)barRightClick:(UIButton *)btn {
    if(![self.barRightButtonView containsSubView:btn]) return;
    NSUInteger tag = [self.barRightButtonView.subviews indexOfObject:btn];
    
    if([self.delegate respondsToSelector:@selector(navigationBarRightClick:)]) {
        [self.delegate navigationBarRightClick:tag];
    }
}

#pragma mark - default set
- (void)setFrame:(CGRect)frame {
    [super setFrame:CGRectMake(0, 0, SCREEN_WIDTH, NAVI_BAR_HEIGHT)];
}

- (void)setBarTintColor:(UIColor *)barTintColor {
    _barTintColor = barTintColor;
    self.backImgView.image = nil;
    self.backImgView.backgroundColor = barTintColor;
}

- (void)setBarTintImage:(UIImage *)barTintImage {
    _barTintImage = barTintImage;
    self.backImgView.image = barTintImage;
    self.backImgView.backgroundColor = nil;
}

- (void)setRootViewController:(BOOL)rootViewController {
    _rootViewController = rootViewController;
    if(_rootViewController) {
        self.backButton = nil;
    }
}

#pragma mark - leftBarButtonItem
- (void)setTitle:(NSString *)title {
    _titleLabel.text = title;
    [self updateNavLayout];
}

- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage {
    [self setTitle:title titleImage:titleImage imageSize:CGSizeZero];
}
- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage imageSize:(CGSize)imageSize {
    if(_titleImgView) {
        [_titleImgView removeFromSuperview];
    }
    _titleImgView = [UIImageView new];
    _titleImgView.image = titleImage;
    if(CGSizeEqualToSize(imageSize, CGSizeZero)) {
#warning mark - 默认应为设计的尺寸
        imageSize = CGSizeMake(28, 28);
    }
    _titleImgView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    [self.contentView addSubview:_titleImgView];
    _titleLabel.text = title;
    [self updateNavLayout];
}

- (void)addBarLeftTitles:(NSArray<NSString *> *)titles {
    [self removeBarLeftAll];
    for(NSString * title in titles) {
        [self addBarLeftTitle:title];
    }
}

- (void)addBarLeftTitle:(NSString *)title {
    [self addBarLeftTitle:title color:nil];
}

- (void)addBarLeftTitle:(NSString *)title color:(UIColor *)color {
    NSString * newTitle  = StrFormatWhiteSpace(title);
    if(!StrBool(newTitle)) return;
    if(!color) color = self.barItemTitleColor;
    [self configButtonTitle:title color:color barPosition:BarPositionLeft];
    [self.barLeftImagesSizeArray addObject:[NSValue valueWithCGSize:CGSizeZero]];
    [self updateNavLayout];
}

- (void)addBarLeftImages:(NSArray<UIImage *>*)images {
    [self removeBarLeftAll];
    for(UIImage * image in images) {
        [self addBarLeftImage:image];
    }
}

- (void)addBarLeftImage:(UIImage *)image {
    [self addBarLeftImage:image imageSize:self.barItemSize];
}

- (void)addBarLeftImage:(UIImage *)image imageSize:(CGSize)imageSize {
    if(!image) return;
    [self configButtonImage:image barPosition:BarPositionLeft];
    [self.barLeftImagesSizeArray addObject:[NSValue valueWithCGSize:imageSize]];
    [self updateNavLayout];
}



#pragma mark - rightBarButtonItem
- (void)addBarRightTitles:(NSArray<NSString *>*)titles {
    [self removeBarRightAll];
    for(NSString * title in titles) {
        [self addBarRightTitle:title];
    }
}

- (void)addBarRightTitle:(NSString *)title {
    [self addBarRightTitle:title color:nil];
}

- (void)addBarRightTitle:(NSString *)title color:(UIColor *)color {
    NSString * newTitle  = StrFormatWhiteSpace(title);
    if(!StrBool(newTitle)) return;
    if(!color) color = self.barItemTitleColor;
    [self configButtonTitle:newTitle color:color barPosition:BarPositionRight];
    [self.barRightImagesSizeArray addObject:[NSValue valueWithCGSize:CGSizeZero]];
    [self updateNavLayout];
}

- (void)addBarRightImages:(NSArray<UIImage *>*)images {
    [self removeBarRightAll];
    for(UIImage * image in images) {
        [self addBarRightImage:image];
    }
}

- (void)addBarRightImage:(UIImage *)image {
    [self addBarRightImage:image imageSize:self.barItemSize];
}

- (void)addBarRightImage:(UIImage *)image imageSize:(CGSize)imageSize {
    if(!image) return;
    [self configButtonImage:image barPosition:BarPositionRight];
    [self.barRightImagesSizeArray addObject:[NSValue valueWithCGSize:imageSize]];
    [self updateNavLayout];
}

#pragma mark -
- (void)updateNavLayout {
    [self layoutSubviews];
}

- (void)removeBarLeftAll {
    [self.barLeftButtonView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.barLeftImagesSizeArray removeAllObjects];
}

- (void)removeBarRightAll {
    [self.barRightButtonView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.barRightImagesSizeArray removeAllObjects];
}

@end
