//
//  BaseViewController.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCMApiClient.h"
#import "Constant.h"
#import "NavigationBar.h"

typedef NS_ENUM(NSInteger, BG_Type) {
    BG_TypeTop = 0,      //默认 上
    BG_TypeAll = 1,      //全屏
    BG_TypeAllCover = 2, //全屏 & 遮罩
    BG_TypeNULL
};

@interface BaseViewController : UIViewController
/// 是否支持旋转屏幕
@property (nonatomic, assign) BOOL isRotate;
/// 3D touch 【peek pop】 要特殊处理
@property (nonatomic, assign) BOOL peekPop3DtouchNavPopGesEnable;
/// 右滑手势是否可以popNav
@property (nonatomic, assign) BOOL navPopGestureRecognizerEnable;


//-----------------------------导航栏---------------------------------------
@property (nonatomic, assign) BOOL navigationBarHidden; //default NO
@property (nonatomic, strong, readonly) NavigationBar * navigationBar;
@property (nonatomic, copy) NSString * rightBarBtnName;

- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage;
- (void)barBackClick;
- (void)barLeftBtnClick:(NSUInteger)index;
- (void)barRightBtnClick:(NSUInteger)index;

//---------------------------内容-----------------------------------------
@property (nonatomic, strong) UIView * contentView; //不要使用self.view
@property (nonatomic, strong) UIImageView * contentViewImgView;

@property (nonatomic, assign) BG_Type imgViewBGType;
- (UIImageView *)imgAddCoverImg;


//--------------------------------------------------------------------
/// 切换竖屏显示
- (void)closeDeviceCrossScreen;

/// 弹toash
- (void)showToash:(NSString *)message;


//----------------------------网络----------------------------------------
/// 网络状态 -- 子类，如果需要，就重写 setter方法
@property (nonatomic, copy, readonly) NSString * networkStatus;

/// 网络变了
- (void)networkChange;

/// 显示没有网络状态  默认从顶部开始
- (void)showNullNetworkView;

/// 隐藏没有网络状态
- (void)hiddenNullNetworkView;


/// 刷新数据源---重新请求数据-注意清空分页数据源
- (void)networkReloadDataButtonClick;


//------------------------------跳转--------------------------------------
- (void)dismissToRootChangePop:(void (^)(void))completion;
- (void)dismissChangePop:(void (^)(void))completion;
- (void)preChangePushController:(BaseViewController *)c addBackButton:(BOOL)add;
+ (void)preChangePushController:(BaseViewController *)c addBackButton:(BOOL)add;


/// 不要用
//- (void)addTapGestureForView:(UIView *)view;
@end






@interface ViewKeyboardDeal : NSObject<UIGestureRecognizerDelegate>
@property (nonatomic, weak) UIView *associatedView;
@end
