//
//  RXSegmentViewController.h
//  RXExtenstion
//
//  Created by srxboys on 2018/2/6.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//
//分段控制器

#import "BaseViewController.h"

@interface BaseSegmentViewController : BaseViewController

@property(nonatomic, strong) NSArray <UIViewController *>*viewControllers;
@property(nonatomic) NSInteger selectedIndex;

@end





@interface UIViewController (BaseSegmentViewController)

- (BaseSegmentViewController *)rxSegmentParentViewController;

@end


/*
        使用: (样式是系统的，可以需要，如果是横滚动多个控制器，需要自定义封装了)
 
 @interface 红包列表 : BaseSegmentViewController
 @end
 
 @implementation 红包列表
 - (void)viewDidLoad {
     [super viewDidLoad];
 
     self.title = @"分段控制器";
     id a = [NSClassFromString(@"RXSearchViewController") new];
     id b = [NSClassFromString(@"RXMenuController") new];
     self.viewControllers = @[a, b];
 }
 @end
 
 */
