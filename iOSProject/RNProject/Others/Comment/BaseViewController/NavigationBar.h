//
//  NavigationBar.h
//  DragonYin
//
//  Created by srxboys on 2018/4/24.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//
// 未完成 请等待

#import <UIKit/UIKit.h>

@protocol NavigationBarDelegate <NSObject>
@required
- (void)navigationBarBackClick;

@optional
// index 0...
- (void)navigationBarLeftClick:(NSUInteger)leftIndex;
- (void)navigationBarRightClick:(NSUInteger)leftIndex;
@end




@interface NavigationBar : UIView

/**  标识，不可以被外部调用 */
- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

+ (instancetype)shareInstance;

@property (nonatomic, weak) id<NavigationBarDelegate> delegate;
@property (nonatomic, assign) BOOL rootViewController;
@property (nonatomic, copy) NSString  *title;
@property (nonatomic, strong) UILabel *titleLabel;

/* default set */
@property (nonatomic, strong) UIColor *barTintColor; //背景色   default nil
@property (nonatomic, strong) UIImage *barTintImage; //背景图
@property (nonatomic, strong) UIColor *barItemTitleColor;// left 、 right <==> default white
@property (nonatomic, assign, readonly) CGSize   barItemSize; // left 、 right <==> default (23,23)

- (void)updateNavLayout;
- (void)removeBarLeftAll;
- (void)removeBarRightAll;

/* barTitleView */
- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage;
- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage imageSize:(CGSize)imageSize;

/* leftBarButtonItem */
- (void)addBarLeftTitles:(NSArray<NSString *>*)titles;
- (void)addBarLeftTitle:(NSString *)title;
- (void)addBarLeftTitle:(NSString *)title color:(UIColor *)color;

- (void)addBarLeftImages:(NSArray<UIImage *>*)images;
- (void)addBarLeftImage:(UIImage *)image;
- (void)addBarLeftImage:(UIImage *)image imageSize:(CGSize)imageSize;


/* rightBarButtonItem */
- (void)addBarRightTitles:(NSArray<NSString *>*)titles;
- (void)addBarRightTitle:(NSString *)title;
- (void)addBarRightTitle:(NSString *)title color:(UIColor *)color;

- (void)addBarRightImages:(NSArray<UIImage *>*)images;
- (void)addBarRightImage:(UIImage *)image;
- (void)addBarRightImage:(UIImage *)image imageSize:(CGSize)imageSize;

@end
