//
//  BaseViewController.m
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseNavViewController.h"

#import "SDImageCache.h"
#import "TalkingData.h"
#import "SCMNetworkCheck.h"




@class ViewKeyboardDeal;
@interface BaseViewController ()<UIGestureRecognizerDelegate, CAAnimationDelegate, NavigationBarDelegate>
@property (nonatomic, copy) void(^animationComplete)(void);
@property (nonatomic, strong) CATransition * animalPrePushPop;


@property (nonatomic, strong) ViewKeyboardDeal * viewKeyboardDeal;
@property (nonatomic, strong) UITapGestureRecognizer * viewKeyboardDealTap;
@end

@implementation BaseViewController
@synthesize navigationBar = _navigationBar;

//改变 控制器的 状态栏的 颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate
{
    return YES;
}


-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    if(!self.isRotate) {
        return UIInterfaceOrientationPortrait;
    }
    return UIInterfaceOrientationPortrait|UIInterfaceOrientationLandscapeLeft|UIInterfaceOrientationLandscapeRight;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if(!self.isRotate) {
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskLandscapeLeft|UIInterfaceOrientationMaskLandscapeRight;
}
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [TalkingData trackPageEnd:SELF_CLASS_NAME];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    STATUS_BAR_STOP();
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(!self.isRotate) {
        [self closeDeviceCrossScreen];
    }
    
    [TalkingData trackPageBegin:SELF_CLASS_NAME];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navPopGestureRecognizerEnable = _navPopGestureRecognizerEnable;
}

/// 切换竖屏显示
- (void)closeDeviceCrossScreen {
    UIDevice *device = [UIDevice currentDevice];
    
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    if(isPad) {
        if (interfaceOrientation != UIInterfaceOrientationPortrait &&  interfaceOrientation != UIInterfaceOrientationLandscapeLeft && interfaceOrientation != UIInterfaceOrientationLandscapeRight) {
            [device setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        }
    }
    else {
        if (interfaceOrientation != UIInterfaceOrientationPortrait) {
            [device setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //状态栏影响UIScrollView的内容
    if(iOSAVALIABLE(8, 11)) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    //一定有要底色
    self.view.backgroundColor = MAIN_BACKGROUND_COLOR;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //允许屏幕旋转
    self.isRotate = isPad;
    
    //允许 右滑返回手势
    self.navPopGestureRecognizerEnable = YES;
    
    NOTI_ADD_OBNAME(@selector(reloadNetworkStatus), NOTINAME_NETWORK);
    
    _viewKeyboardDeal = [ViewKeyboardDeal new];
    _viewKeyboardDealTap = [UITapGestureRecognizer new];
    
    [self addTapGestureForView:self.view];
    
    self.animalPrePushPop = [CATransition animation];
    self.animalPrePushPop.delegate = self;
    
    /// 初始化 view
    _contentView = [UIView new];
    _contentView.clipsToBounds = YES;
    [self.view addSubview:_contentView];
    [self setImgViewBGType:0];
    [self.view addSubview:self.navigationBar];
    if(!self.navigationController.viewControllers) {
        self.navigationBar.rootViewController = YES;
    }
    [self setNavitaionControllerHidden:YES animated:NO];
    [self settingNavigateionGesture];
}

#pragma mark - NavigationBar
- (void)setNavigationBarHidden:(BOOL)navigationBarHidden {
    _navigationBarHidden = navigationBarHidden;
    self.navigationBar.hidden = navigationBarHidden;
    [self viewWillLayoutSubviews];
}

- (void)setNavitaionControllerHidden:(BOOL)hidden animated:(BOOL)animated {
    //导航栏
    [self.navigationController setNavigationBarHidden:hidden animated:animated];
    self.navigationController.navigationBarHidden = hidden;
    ////  [self.navigationController setToolbarHidden:YES animated:YES];
}

- (void)settingNavigateionGesture{
    //自定义/隐藏/修改返回等 导航栏，默认向右滑手势会消失
    __weak typeof(self) bself = self;    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        //当 页面是UINavigationController就会让【右滑手势】有效
        self.navigationController.interactivePopGestureRecognizer.delegate = bself;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.navigationController.viewControllers.count == 1)
    {
        //关闭主界面的右滑返回
        return NO;
    }
    else
    {
        return YES;
    }
}



- (NavigationBar *)navigationBar {
    if(!_navigationBar) {
        _navigationBar = [NavigationBar shareInstance];
        _navigationBar.delegate = self;
    }
    return _navigationBar;
}

- (void)setTitle:(NSString *)title {
    [super setTitle:title];
    if(StrBool(self.title)) {
        [self.navigationBar setTitle:self.title];
    }
}
- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage; {
    [self.navigationBar setTitle:title titleImage:titleImage];
}

- (void)setRightBarBtnName:(NSString *)rightBarBtnName {
    _rightBarBtnName = rightBarBtnName;
    [self.navigationBar removeBarRightAll];
    [self.navigationBar addBarRightTitle:rightBarBtnName];
}

// navigationBar delegate
- (void)navigationBarBackClick {
    [self barBackClick];
}
- (void)navigationBarLeftClick:(NSUInteger)leftIndex {
    [self barLeftBtnClick:leftIndex];
}
- (void)navigationBarRightClick:(NSUInteger)leftIndex {
    [self barRightBtnClick:leftIndex];
}


// goto sub
- (void)barBackClick {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)barLeftBtnClick:(NSUInteger)index {/*sub class run*/}
- (void)barRightBtnClick:(NSUInteger)index {/*sub class run*/}

#pragma mark - contentViewImgView
- (UIImageView *)contentViewImgView {
    if(!_contentViewImgView) {
        _contentViewImgView = [UIImageView new];
        _contentViewImgView.frame = self.contentView.bounds;
        _contentViewImgView.image = [UIImage imageNamed:@"img_bg"];
        _contentViewImgView.contentMode = UIViewContentModeScaleToFill;
        [self.contentView addSubview:_contentViewImgView];
    }
    return _contentViewImgView;
}


#pragma mark - setImgViewBGType
- (void)setImgViewBGType:(BG_Type)imgViewBGType {
    _imgViewBGType = imgViewBGType;
    
    self.contentViewImgView.Ex_height = self.contentView.Ex_height;
    
    if(imgViewBGType == BG_TypeTop) {
        //适配
        self.contentViewImgView.Ex_height = ACTURE_HEIGHT(160)-20+STATUS_BAR_HEIGHT;
    }
    else if(imgViewBGType == BG_TypeAllCover){
        [self imgAddCoverImg];
    }
    else if(imgViewBGType == BG_TypeNULL) {
        self.contentViewImgView.frame = CGRectZero;
    }
}

- (UIImageView *)imgAddCoverImg {
    [self.contentViewImgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIImageView * bottomImgCover = [UIImageView new];
    bottomImgCover.backgroundColor = [UIColor clearColor];
    bottomImgCover.image = [UIImage imageNamed:@"img_cover"];
    bottomImgCover.frame = self.contentViewImgView.bounds;
    CGFloat height = ACTURE_HEIGHT(325);
    bottomImgCover.Ex_y = self.contentViewImgView.Ex_height - height;
    bottomImgCover.Ex_height = height;
    [self.contentViewImgView addSubview:bottomImgCover];
    return bottomImgCover;
}

- (void)setNavPopGestureRecognizerEnable:(BOOL)navPopGestureRecognizerEnable {
    if(self.peekPop3DtouchNavPopGesEnable) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        return;
    }
    _navPopGestureRecognizerEnable = navPopGestureRecognizerEnable;
    if (self.navigationController.viewControllers.count > 1) {
        self.navigationController.interactivePopGestureRecognizer.enabled = _navPopGestureRecognizerEnable;
    }
    else {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        _navPopGestureRecognizerEnable = NO;
    }
}

#pragma mark - SCMNetworkCheck
- (void)reloadNetworkStatus {
    NSString * status = [SCMNetworkCheck shareNetworkCheck].statusString;
    [self setNetworkStatus:status];
}

- (void)setNetworkStatus:(NSString *)networkStatus {
    if(StrBool(_networkStatus)) {
        _networkStatus = networkStatus;
        [self networkChange];
    }
    else {
        _networkStatus = networkStatus;
    }
}

/// 网络变了
- (void)networkChange {
    
}

/// 显示没有网络状态  默认从顶部开始
- (void)showNullNetworkView {
    
}

/// 隐藏没有网络状态
- (void)hiddenNullNetworkView {
    
}


/// 刷新数据源---重新请求数据-注意清空分页数据源
- (void)networkReloadDataButtonClick {
    
}


#pragma mark - showToash
/// 弹toash
- (void)showToash:(NSString *)message {
    if(!StrBool(message)) return;
    UIView * navView = self.view;
    if(self.navigationController) {
        navView = self.navigationController.view;
    }
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
    [hud showHudLabel:NonEmptyString(message)];
}




#pragma mark 模态dismiss跳转 -> pop跳转
- (void)dismissToRootChangePop:(void (^)(void))completion {
    BaseViewController * c = self;
    if(self.navigationController.viewControllers.count > 1) {
        c = [self.navigationController.viewControllers firstObject];
        self.navigationController.viewControllers = @[c];
    }
    [c dismissChangePop:completion];
}

- (void)selfDismissChangePop {
    [self dismissChangePop:nil];
}

- (void)dismissChangePop:(void (^)(void))completion {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(completion) {
                completion();
            }
        });
    }
    else {
        if(completion) {
            self.animalPrePushPop.duration = 0.01;
            self.animalPrePushPop.type = kCATransitionFade;
        }
        else {
            self.animalPrePushPop.duration = 0.3;
            self.animalPrePushPop.type = kCATransitionMoveIn;
        }
        
        self.animalPrePushPop.subtype = kCATransitionFromLeft;
        [[APP_KEY_WINDOW layer] addAnimation:self.animalPrePushPop forKey:nil];
        self.animationComplete = completion;
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if(flag) {
        if(self.animationComplete) {
            self.animationComplete();
        }
    }
}



#pragma mark 模态跳转 -> push跳转
- (void)preChangePushController:(BaseViewController *)c addBackButton:(BOOL)add {
    if(self.navigationController.viewControllers.count > 0) {
        [self.navigationController pushViewController:c animated:YES];
    }
    else {
        [BaseViewController preChangePushController:c addBackButton:add];
    }
}

+ (void)preChangePushController:(BaseViewController *)c addBackButton:(BOOL)add {
    ///这个封装的不太智能，不太好，后期修改吧
    BaseNavViewController * nav = [[BaseNavViewController alloc] initWithRootViewController:c];
    [[PageRouter sharedPageRouter] present:nav];
}

- (void)addTapGestureForView:(UIView *)view {
    if(self.viewKeyboardDeal.associatedView ) {
        [self.viewKeyboardDeal.associatedView removeGestureRecognizer:self.viewKeyboardDealTap];
    }
    self.viewKeyboardDeal.associatedView = view;
    self.viewKeyboardDealTap.delegate = self.viewKeyboardDeal;
    [view addGestureRecognizer:self.viewKeyboardDealTap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
}

- (void)dealloc {
    RXLog(@"class %@ dealloc", SELF_CLASS_NAME);
    NOTI_REMOVE_SELF;
}
@end














@implementation ViewKeyboardDeal
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    [self.associatedView endEditing:YES];
    return NO;
}
@end
