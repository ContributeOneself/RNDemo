//
//  BaseTableViewController.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseTableViewController : BaseViewController<UITableViewDelegate, UITableViewDataSource>
/*
     需要子类 去适配frame [默认 UITableViewStylePlain]
     以及代理方法
 */
@property(nonatomic, strong) UITableView * tableView;
@property (nonatomic, assign) BOOL isShowingKeyboard;//如果cell有输入的一定要判断(针对布局用的)

@property (nonatomic, assign) CGFloat cellBaseHeight;



/*
     tableView.style = UITableViewStyleGrouped
     注意约束:
             1、已经self.tableView 约束了，先删除。然后重新初始化self.tableview
             2、给新的self.tableView 添加约束
             3、设置代理
 */
- (void)setTableStyleGrouped;




/*
一般这个不使用：
     tableView header footer 默认我处理是nil
 
     如果headers/footers 高度一样的
                 self.tableView.sectionHeaderHeight = 20;
                 self.tableView.sectionfooterHeight = 20;
     或者，请使用下面方法, 调用【UITableViewDelegate代理】自行给每个headers/footers赋值高度
 */
- (void)setTableViewHaveHeaderView;
- (void)setTableViewHaveFooterView;

/* /tableView.contentInset.top = 默认10， 下面方法是设置` 0 ` */
- (void)setTableViewContentInsetTopIsZero;

/* /tableView.contentInset.bottom = 默认0， 下面方法是设置` 40 ` */
- (void)setTableViewContentOutside;


/*
      待完善的 功能，添加 MJRefresh 的功能，子类只要调用方法
 */

@property (nonatomic, assign) NSInteger currentPage;//一般不用设置
@property (nonatomic, assign) Request requestType;//默认POST
@property (nonatomic, copy) NSString * method; //接口
@property (nonatomic, strong) Class modelClass;
@property (nonatomic, strong) NSDictionary * parameterts; //参数
@property (nonatomic, strong) NSArray * dataListArray; //数据源
- (void)refresh;
- (void)loadMore;


@end
