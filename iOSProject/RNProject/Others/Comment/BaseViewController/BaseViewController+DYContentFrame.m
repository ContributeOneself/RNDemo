//
//  BaseViewController+DYContentFrame.m
//  DragonYin
//
//  Created by srxboys on 2018/5/15.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController+DYContentFrame.h"
#import <objc/runtime.h>

@implementation BaseViewController (DYContentFrame)
+ (void)load {
    
    RXLog(@"current class name =%@\n", NSStringFromClass(self));
    
    //方法交换应该被保证，在程序中只会执行一次
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        /// init (其实可以写代码质量高的)
        //获得viewController的生命周期方法的selector
        SEL systemSel = @selector(viewWillLayoutSubviews);
        //自己实现的将要被交换的方法的selector
        SEL swizzSel = @selector(swiz_viewWillLayoutSubviews);
        //两个方法的Method
        Method systemMethod = class_getInstanceMethod([self class], systemSel);
        Method swizzMethod = class_getInstanceMethod([self class], swizzSel);
        
        //首先动态添加方法，实现是被交换的方法，返回值表示添加成功还是失败
        BOOL isAdd = class_addMethod(self, systemSel, method_getImplementation(swizzMethod), method_getTypeEncoding(swizzMethod));
        if (isAdd) {
            //如果成功，说明类中不存在这个方法的实现
            //将被交换方法的实现替换到这个并不存在的实现
            class_replaceMethod(self, swizzSel, method_getImplementation(systemMethod), method_getTypeEncoding(systemMethod));
        }else{
            //否则，交换两个方法的实现
            method_exchangeImplementations(systemMethod, swizzMethod);
        }
    });
}

- (void)swiz_viewWillLayoutSubviews {
    /// 如果旋转屏幕 主要调整 状态栏
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    [[UIApplication sharedApplication] setStatusBarOrientation:orientation];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    //如果旋转屏幕
    [self setImgViewBGType:self.imgViewBGType];
    
    CGFloat top = NAVI_BAR_HEIGHT;
    if(self.navigationBarHidden) {
        top = 0;
    }
    self.contentView.frame = CGRectMake(0, top, self.view.Ex_width, self.view.Ex_height - top);
    [self.navigationBar updateNavLayout];
    [self swiz_viewWillLayoutSubviews];
}
@end
