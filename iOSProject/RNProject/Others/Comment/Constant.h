//
//  Constant.h
//  SCM
//
//  Created by srx on 2017/9/14.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//
//

#ifndef Constant_h
#define Constant_h

/*
   作用域：(固定 定义用)
     采用哪台【接口服务器】
     控制所有【全局】打印控制、常量、通知、沙盒、缓存
     通用接口【API】
 
 */



//-------------------- 是否打印日志 --------------------/
// 0:不打印   1:打印 (和发布没有关系)
#define SEC_LOG_ENABLE 1

//-------------------- 是否打印接口日志 --------------------/
// 0:不打印   1:打印 (和发布没有关系)
#define SET_INTERFACE_LOG_ENABLE 0

/********************** 服务器 *************************/
// 0:正式环境Dis/GHS(发布) 1:测试环境Test (发布市场前请改为0)(可以手动修改)
#define SET_SERVER RELEASE_iOS

/********************** 统计、推送配置 ************/
//0:面向用户 1:面向开发人员 (发布市场前请改为0) 个推
#define SEC_UM_GeTui RELEASE_UM_GeTui



#if SET_SERVER == 0
    //正式
    #define SERVER_URL @"http://jzlonging.com"

#else
    //测试
    #define SERVER_URL @"http://jzlonging.com"
#endif

/********************** 渠道区分 ***************************/
//----- 把开发、测试  和 上线的区分 ------
#if DEBUG
    #define AppChannel @"debug真机"
#else
    #if SEC_UM_GeTui
        #define AppChannel @"release" //SEC_UM_GeTui == 1
    #else
         #define AppChannel @"App Store"   //SEC_UM_GeTui == 0
    #endif
#endif
//百度统计 3c834d6c96


#if SEC_UM_GeTui == 0
    /// 个推
    #define GLOBAL_GETUI_APP_ID      @""
    #define GLOBAL_GETUI_APP_KEY     @""
    #define GLOBAL_GETUI_APP_SECRET  @""

    /// TalkingDataSDK 统计
    #define GLOBAL_TALK_DATA_ID      @"454AF17883C7DC438FFA8D2464D7672F"

    /// 友盟 统计/分享
    #define GLOBAL_Statistics_APPKET @""

    // 微信
    #define GLOBAL_WXAPP_ID          @""
    #define GLOBAL_WXAPP_SECRET      @""

//@"y50eGwdGy3mFAzJsniPaCuS3TNICGMk8"

#else
    /// 个推【测试】
    #define GLOBAL_GETUI_APP_ID      @""
    #define GLOBAL_GETUI_APP_KEY     @""
    #define GLOBAL_GETUI_APP_SECRET  @""

    /// TalkingDataSDK【测试】 统计
    #define GLOBAL_TALK_DATA_ID      @"98D2E52E54C743F29481E6CED3F34521"

    /// 友盟 统计/分享【测试】
    #define GLOBAL_Statistics_APPKET @""

    // 微信【测试】
    #define GLOBAL_WXAPP_ID          @""
    #define GLOBAL_WXAPP_SECRET      @""
#endif




/********************** 通用常量 *************************/
#pragma mark  -- 通用常量 --
#define CONST_APP_ID                  @"1093039580"

#define CONST_NET_NONE               @"const_network_none"
#define CONST_NET_UNKOWN             @"const_network_unknow"
#define CONST_NET_WIFI               @"const_network_wifi"
#define CONST_NET_PHONE              @"const_network_phone"

#define CONST_LANGUAGE_ZN            @"zh-Hans"
#define CONST_LANGUAGE_EN            @"en"



/********************** 通知定义 ***************************/
#pragma mark  -- 通知定义 -- 未使用
#define NOTINAME_APNS_PUSH          @"APP_ApnsPush_noti" //推送消息--通知【带值】
#define NOTINAME_NETWORK            @"App_network_status" //网络状态

/********************** 沙盒 ******************************/
#pragma mark  -- 沙盒 -- 未使用
#define Sandbox_APP_UDID            @"Sandbox_DY_APP_UDID" //UDID
#define Sandbox_USER_ID             @"Sandbox_UserLoginedId" //userID
#define Sandbox_ACCESS_TOKEN        @"Sandbox_ACCESS_TOKEN"  //accessToken
#define Sandbox_APP_LANGUAGE        @"Sandbox_app_language"

/********************** 缓存定义 ***************************/
#pragma mark  -- 缓存定义 -- 未使用
//#define CACHE_ADDRESS_LOCALJSON     @"AdressLocalJson.json" //网络请求地址 --缓存
//#define CACHE_FINAANCE_CONTACTS     @"CACHE_FinanceContacts" //财务联系人
//#define CACHE_PROFILE_GENERAL_NAME  @"CACHE_PROFILE_GENERAL_NAME" //我的基本资料


/********************** 接口定义 ***************************/
#pragma mark  -- 接口定义 --
/** 一、登录相关接口 **/
#define API_USER_LOGIN              @"/account/rest/user/login"    //1.登录接口(POST)
#define API_USER_LOGOUT             @"/account/rest/user/logout"   //2.退出接口(GET)
#define API_USER_REGISTER           @"/account/rest/user/creating" //3.注册(POST)
#define API_USER_FORGET_PASSWORD    @"/account/rest/user/forgetPassword_" //15.忘记密码(GET)
//#define API_USER_UPDATE_PS          @"/account/rest/user/updatePassword" //修改密码(userID/ps)(POST)
//#define API_USER_UPDATE_PS_ON       @"/account/rest/user/updatePassword_" //修改密码(userID、oldPs/newPs)(POST) 没用

#define API_USER_ORIGIN_ALL         @"/account/rest/region/byAll" //籍贯列表
#define API_USER_MODIFY             @"/account/rest/user/updating" //4.修改个人信息(POST)

#define API_UPLOAD_FILE             @"/account/rest/bankcard/uploadFile" //上传文件

/** 二、登录相关接口 **/
#define API_BANK_APPLY_LIST         @"/account/rest/bankcard/findByUserId" //5.已经申请的银行列表(GET)
#define API_BANK_LIST               @"/account/rest/bank/findByBankList"   //6.银行列表(GET)(根据用户ID获取未开户的银行)
#define API_BANK_BRANCH_LIST        @"/account/rest/bank/findBankBranchesInfoAll" //8. 支行列表（暂时不用(GET)
#define API_BANK_ACC_OPEN           @"/account/rest/bankcard/creating"  //9. 开户接口(POST)
#define API_BANK_ACC_OPEN_AGREEMENT @"/account/rest/bank/getgAreementByBankId"  //7.开户协议(GET)

/** 三、短信 **/
#define API_MESSAGE_SEND                @"/account/rest/sms/sendMessage" //10.发送短信(GET)
#define API_MESSAGE_AUTH                @"/account/rest/sms/smsAuth"  //11.短信验证(GET)

/* 四、web页 */
#define API_WEB_OPEN_ACC_INFO           @"/apph5/bank_introduce.html" //开户介绍
#define API_WEB_BANK_CLAUSE_AGREEMNET   @"/apph5/bank_agreement.html" //银行条款协议
#define API_WEB_REGISTER_AGREEMNET      @"/apph5/longling_agreement.html" //龙吟注册协议
#define API_WEB_GOODS                   @"/apph5/goods.html" //商品
#define API_WEB_GOODS_DETAIL            @"/apph5/goods_detail.html" //商品详情
#define API_WEB_WODE_ORDER              @"/apph5/myorder.html" //我的订单
#define API_WEB_WODE_ORDER_DETAIL       @"/apph5/myorder.html" //我的订单详情&下订单
#define API_WEB_PRODUCT                 @"/apph5/product.html" //理财产品
#define API_WEB_PRODUCT_DETAIL          @"/apph5/product_detail.html" //产品详情
#endif 







