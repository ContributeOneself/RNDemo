//
//  Localizable.h
//  SCM
//
//  Created by srx on 2017/11/28.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//
// 国际化语言 (未使用)

/*
    使用
    规范 NSLocalizedString(key, comment)
         第一个参数key是从Localizable.strings这个文件中读取对应的key-value值。
         第二个参数comment可以是nil，可以是一段为空的字符串，也可以是对key的注释
    例子 NSLocalizedString(LOCAL_TAB_HOME, nil)
 
 // 改进后，使用  ``` Localized(LOCAL_TAB_HOME) ```
 
    定义
    所有以 LOCAL开头
 
    日后，以微信为例可以随意切换语言
 
 
    国际化编译错误
    /Users/srx/srxboys/gitlabel/SCMiOS/Comment/Language/en.lproj/Localizable.strings:0: error: read failed: The data couldn’t be read because it isn’t in the correct format
     那是因为，《   "key"="value";  》 有忘记写`分号`。
 */

#ifndef Localizable_h
#define Localizable_h

//-----------------------------------------------------------------------
#pragma mark ----------Tab 的定义------------
/// 首页
#define  LOCAL_TAB_HOME @"LOCAL_TAB_HOME"
/// 功能
#define  LOCAL_TAB_FUNC @"LOCAL_TAB_FUNC"
/// 消息
#define  LOCAL_TAB_NEWS @"LOCAL_TAB_NEWS"
/// 我的
#define  LOCAL_TAB_PROFILE @"LOCAL_TAB_PROFILE"

//-----------------------------------------------------------------------
#pragma mark ----------XX 的定义------------

#define LOCAL_SIGN_IN    @"Sign in"
#define LOCAL_NOTICE     @"Notice"
#define LOCAL_ALL        @"All"
#define LOCAL_UNREAD     @"Unread"
#define LOCAL_NAME       @"Name"
#define LOCAL_REAL_NAME  @"Real Name"
#define LOCAL_PHONE      @"Phone"
#define LOCAL_FAX        @"FAX"
#define LOCAL_EMAIL      @"Email"
#define LOCAL_ADDRESS    @"Address"

#define LOCAL_SUN        @"Sun"
#define LOCAL_MON        @"Mon"
#define LOCAL_TUE        @"Tue"
#define LOCAL_WED        @"Wed"
#define LOCAL_THU        @"Thu"
#define LOCAL_FRI        @"Fri"
#define LOCAL_SAT        @"Sat"

#define LOCAL_DETAIL     @" Detail"

#define LOCAL_CLOSE      @"Close"

#define LOCAL_REQUEST_FAIL @"Request failed"

#define LOCAL_LOGIN_LEFT_PUZZLE @"LOCAL_LOGIN_LEFT_PUZZLE"

//-----------------------------------------------------------------------
#pragma mark ----------功能 的定义------------
#define LOCAL_FUNC_SEL_DATE @"LOCAL_FUNC_SEL_DATE"

//-----------------------------------------------------------------------
#pragma mark ----------消息 的定义------------

//-----------------------------------------------------------------------
#pragma mark ----------我的 的定义------------
//基本资料
#define LOCAL_ME_IN_BASICINFO @"LOCAL_ME_IN_BASICINFO"
//修改密码
#define LOCAL_ME_IN_MODPS @"LOCAL_ME_IN_MODPS"

//修改密码
#define LOCAL_ME_IN_MODSAFEPHONE @"LOCAL_ME_IN_MODSAFEPHONE"

//退出登录
#define LOCAL_ME_IN_SIGNOUT @"LOCAL_ME_IN_SIGNOUT"


//-----------------------------------------------------------------------
#endif /* Localizable_h */
