//
//  DYMessage.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#ifndef DYMessage_h
#define DYMessage_h

/*
    页面可能有重复的UI、重复的判断
    后
 
    需要 提示(toash) 消息(message)
 
    避免重复书写可能出现的错误，这里通用定义，一处改，处处改
 */

#define MS_WAITING               @"请稍后..."

#define MS_IDCARD_ERROR          @"请输入有效的身份证号码"
#define MS_PHONE_ERROR           @"请输入正确的手机号"
#define MS_CAPTCHA_NIL           @"验证码不可为空"
#define MS_CAPTCHA_ERROR         @"请输入正确的验证码"
#define MS_PASSWORD_ERROR        @"请输入有效的密码"
#define MS_PASSWORD_LEN_ERROR    @"密码不少于6位"
#define MS_PASSWORD_DIFFER_ERROR @"新旧密码不同"
#define MS_ADDRESS_ERROR         @"请输入详细地址"











#endif /* DYMessage_h */
