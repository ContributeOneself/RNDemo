//
//  PageRouter.m
//  DragonYin
//
//  Created by srxboys on 2018/4/23.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "PageRouter.h"
#import "PageRouterConfig.h"

#import "Constant.h"
#import "AppDelegate.h"

/** 【需要初始化的单例】 **/
#import "RXUDID.h"
#import "DYUserCache.h"
#import "SCMAPNSManager.h"
#import "RXInterfacePrint.h"
#import "SCMNetworkCheck.h"

#import "TalkingData.h"


@interface PageRouter ()
@property (nonatomic, strong) dispatch_queue_t ioQueue;
@property (nonatomic, weak) UIViewController* currentController;
@property (nonatomic, strong) PageRouterConfig * config;
@end

@implementation PageRouter

+ (instancetype)sharedPageRouter {
    static PageRouter * __pageRouter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        [self configGlobal];
        
        __pageRouter = [[PageRouter alloc] initConfigGlobal];
#if DEBUG
        //接口打印
        [RXInterfacePrint shareInterfacePrint];
#endif
        
    });
    return __pageRouter;
}

- (instancetype)initConfigGlobal
{
    self = [super init];
    if (self) {
        //内部处理的在这里
        //        _tabBarController = [SCMTabBarViewController new];
        //        _loginInController = [SCMLoginInController new];
        
//        _ioQueue = dispatch_queue_create("pageRouter_srxboys", DISPATCH_QUEUE_SERIAL);
        //        self.tabBarController.edgesForExtendedLayout = UIRectEdgeNone;
        
        self.config = [[PageRouterConfig alloc] init];
    }
    return self;
}

- (UIViewController *)appearanceVC {
    return [self.config appearanceVC];
}

+ (void)configGlobal {
    //导航栏 只返回箭头，不要文字
    
    /****所有单例初始化的地方******/
    //该设备的唯一标识UDID
    USER_D_SAVE(Sandbox_APP_UDID, [RXUDID getUMSUDID]);
    // 推送初始化
    [[SCMAPNSManager shareSCMAPNSManager] registerForRemoteNotification];
    
    [SCMNetworkCheck shareNetworkCheck];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [USER_INFO updateBankListAPI];
    });
    
    [TalkingData sessionStarted:GLOBAL_TALK_DATA_ID withChannelId:AppChannel];
    
    
    // 全局改导航栏
    UINavigationBar *bar = [UINavigationBar appearance];
    bar.tintColor = [UIColor whiteColor];
    bar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [bar setBackgroundImage:[UIImage imageNamed:@"img_bg"]
                                     forBarMetrics:UIBarMetricsDefault];
    // 去掉 线条
    [bar setShadowImage:[UIImage new]];
    
    // 状态栏 >= iOS7  , 在baseNav里面设置了
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


- (void)push:(UIViewController *)vc {
    UIViewController * currentC = [self currentController];
    if([currentC isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController *)currentC) pushViewController:vc animated:YES];
    }
    else {
        [[currentC navigationController] pushViewController:vc animated:YES];
    }
}

- (void)pop {
    UIViewController * currentC = [self currentController];
    if([currentC isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController *)currentC) popViewControllerAnimated:YES];
    }
    else {
        [[currentC navigationController] popViewControllerAnimated:YES];
    }
}

- (void)popToRoot {
    [self popToRootWithAnimated:YES];
}

- (void)popToRootWithAnimated:(BOOL)animated {
    UIViewController * currentC = [self currentController];
    if([currentC isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController *)currentC) popToRootViewControllerAnimated:YES];
    }
    else {
        [[currentC navigationController] popToRootViewControllerAnimated:YES];
    }
}

- (void)present:(UIViewController *)vc {
    [self present:vc animated:NO];
}

- (void)present:(UIViewController *)vc animated:(BOOL)animated {
    [[self currentController] presentViewController:vc animated:animated completion:nil];
}

- (void)dismiss {
    [self dismiss:nil];
}

- (void)dismiss:(void (^)(void))completion {
    [self dismissAnimated:NO completion:completion];
}

- (void)dismissAnimated:(BOOL)animated {
    [self dismissAnimated:animated completion:nil];
}

- (void)dismissAnimated:(BOOL)animated completion:(void (^)(void))completion {
    [[self currentController] dismissViewControllerAnimated:animated completion:completion];
}

- (UIViewController *)currentController {
    UIViewController* presentedVC = [[UIApplication sharedApplication].delegate window].rootViewController;
    while (presentedVC.presentedViewController) {
        presentedVC = presentedVC.presentedViewController;
    }
    return presentedVC;
}






#pragma mark - url跳转


@end



@implementation UIViewController(PageRouter)
- (PageRouter *)pageRouter {
    return [PageRouter sharedPageRouter];
}

+ (PageRouter *)pageRouter {
    return [PageRouter sharedPageRouter];
}
@end
