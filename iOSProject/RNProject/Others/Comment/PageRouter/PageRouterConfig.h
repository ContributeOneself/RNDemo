//
//  PageRouterConfig.h
//  DragonYin
//
//  Created by srxboys on 2018/4/23.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

//  router 路由跳转配置

#import <Foundation/Foundation.h>

@interface PageRouterConfig : NSObject
@property (nonatomic, copy, readonly) UIViewController * appearanceVC;
@end
