//
//  PageRouterConfig.m
//  DragonYin
//
//  Created by srxboys on 2018/4/23.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "PageRouterConfig.h"

#import "BaseNavViewController.h"

#import "DYHomeViewController.h"

@interface PageRouterConfig()
@end

@implementation PageRouterConfig

- (instancetype)init
{
    self = [super init];
    if (self) {
        DYHomeViewController * homeC = [[DYHomeViewController alloc] init];
        BaseNavViewController * nav = [[BaseNavViewController alloc] initWithRootViewController:homeC];
        _appearanceVC = nav;
    }
    return self;
}

@end
