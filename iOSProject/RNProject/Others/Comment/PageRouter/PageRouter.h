//
//  PageRouter.h
//  DragonYin
//
//  Created by srxboys on 2018/4/23.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageRouter : NSObject

/**  标识，不可以被外部调用 */
- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

+ (instancetype)sharedPageRouter;
@property (nonatomic, copy, readonly) UIViewController * appearanceVC;

- (void)push:(UIViewController *)vc;
- (void)pop;
- (void)popToRoot;
- (void)popToRootWithAnimated:(BOOL)animated;

- (void)present:(UIViewController *)vc;
- (void)present:(UIViewController *)vc animated:(BOOL)animated;
- (void)dismiss;
- (void)dismiss:(void (^)(void))completion;
- (void)dismissAnimated:(BOOL)animated;
- (void)dismissAnimated:(BOOL)animated completion:(void (^)(void))completion;

- (UIViewController *)currentController;

// private:
//- (void)pushURL:(NSURL *)url animated:(BOOL)animated;
//
//- (void)presentURL:(NSURL *)url;
//
//- (BOOL)isAppeared:(NSURL *)url;

@end




@interface UIViewController(PageRouter)
@property (nonatomic, readonly, strong) PageRouter *pageRouter;
+ (PageRouter *)pageRouter;
@end






/**
 *    通过PageRouter 以url形式打开的page需要实现此接口
 */
@protocol PageRouterURL <NSObject>

/// 将url携带的参数设置给page的属性。
@optional
- (void)setUrlParameter:(NSDictionary *)dict;

@end
