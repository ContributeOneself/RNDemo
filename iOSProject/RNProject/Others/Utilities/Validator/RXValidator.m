//
//  RXValidator.m
//  DragonYin
//
//  Created by srxboys on 2018/4/22.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "RXValidator.h"

#define _Error(_domain, _code, _userInfo) [[NSError alloc] initWithDomain:_domain code:_code userInfo:_userInfo]
#define RXError(_error) _Error(@"RXValidator" ,0, @{NSLocalizedDescriptionKey:_error})

@implementation RXValidator

+ (BOOL)isValidNum:(NSString *)string {
    if(string.length == 0) NO;
    /// 数字
    NSString *regex = @"^\\d+$";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([pred evaluateWithObject:string]) {
        return YES ;
    }else
        return NO;
}

+ (BOOL)isValidCharOrNum:(NSString *)string {
    if(string.length == 0) NO;
    //字母或数字
    NSString *regex = @"^(\\d+$)|([a-zA-Z]+$)";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([pred evaluateWithObject:string]) {
        return YES ;
    }else
        return NO;
}

+ (BOOL)isValidInputPs:(NSString *)string {
    if(string.length == 0) NO;
    //字母或数字或符号
    NSString *regex = @"^(\\d+$)|([a-zA-Z]+$)|(._\\$%&\\*\\!$)";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([pred evaluateWithObject:string]) {
        return YES ;
    }else
        return NO;
}

+ (BOOL)isValidAuthIDCard:(NSString *)idCard {
    if(idCard.length == 0) NO;
    //15 & 18身份证
    NSString *regex = @"//(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([pred evaluateWithObject:idCard]) {
        return YES ;
    }else
        return NO;
}

+ (BOOL)isValidPhoneValidate:(NSString *)phone error:(NSError **)error{
    if(phone.length == 0) NO;
    *error = nil;
    if ([phone length] == 0) {
        *error = RXError(@"请输入手机号");
        return NO;
    }
    
    NSString *regex = @"^1[3456789]\\d{9}$";
    NSPredicate * pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    if ([phone length] != 11 || ![pred evaluateWithObject:phone]) {
        *error = RXError(@"手机号码错误");
        return NO;
    }
    return YES;
}

+ (BOOL)isValidatorPassword:(NSString *)ps {
    if(ps.length == 0) NO;
    //6-16位【数字、字母、符号】最少2项组成
    NSString *regex = @"^(?=.*[a-zA-Z0-9].*)(?=.*[a-zA-Z\\W].*)(?=.*[0-9\\W].*).{6,16}$";
    NSPredicate *   pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    if ([pred evaluateWithObject:ps]) {
        return YES ;
    }else
        return NO;
}

+ (BOOL) isValidEmailAddress:(NSString*)candidate {
    if(candidate.length == 0) NO;
    if (candidate == nil || candidate.length == 0)
    {
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

+(BOOL)isValidMobileNumber:(NSString*)string {
    if(string.length == 0) NO;
    NSString *regex = @"^1[0-9]{10}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:string];
    return isMatch;
}


+ (BOOL)isEmojiTextFiled:(UITextField *)textField inputText:(NSString *)inputText{
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage;// 键盘输入模式
    // 简体中文输入，包括简体拼音，健体五笔，简体手写
    UITextRange *selectedRange = [textField markedTextRange];
    //获取高亮部分
    UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
    return [self isEmoji:inputText textInputModelLanaguage:lang textPosition:position];
}

+ (BOOL)isEmojiTextView:(UITextView *)textView inputText:(NSString *)inputText{
    
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage;// 键盘输入模式
    // 简体中文输入，包括简体拼音，健体五笔，简体手写
    UITextRange *selectedRange = [textView markedTextRange];
    //获取高亮部分
    UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
    return [self isEmoji:inputText textInputModelLanaguage:lang textPosition:position];
}

+ (BOOL)isEmoji:(NSString *)text textInputModelLanaguage:(NSString *)lanaguage textPosition:(UITextPosition *)position {
    if ([lanaguage rangeOfString:@"zh-Hans"].location != NSNotFound ||[lanaguage isEqualToString:@"emoji"]) {
        
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            NSString *string=@"";
            for (int i = 0; i < text.length; ++i)
            {
                NSRange range = NSMakeRange(i, 1);
                NSString *subString = [text substringWithRange:range];
                const char *cString = [subString UTF8String];
                //不可输入表情
                if (cString) {
                    string = [NSString stringWithFormat:@"%@%@",string,subString];
                }else{
                    //                    [self showSettingToast:@"不支持输入Emoji表情符号" inView:textView.inputAccessoryView];
                    return YES;
                }
            }
        }
    }
    return NO;
}
@end
