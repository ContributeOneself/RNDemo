//
//  RXValidator.h
//  DragonYin
//
//  Created by srxboys on 2018/4/22.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RXValidator : NSObject
/// 数字
+ (BOOL)isValidNum:(NSString *)string;

/// //字母和数字
+ (BOOL)isValidCharOrNum:(NSString *)string;

+ (BOOL)isValidInputPs:(NSString *)string;

///15 & 18身份证
//+ (BOOL)isValidAuthIDCard:(NSString *)idCard;

//+ (BOOL)isValidPhoneValidate:(NSString *)phone error:(NSError **)error;

///6-16位【数字、字母、符号】最少2项组成
+ (BOOL)isValidatorPassword:(NSString *)ps;

///是否为邮箱
+ (BOOL) isValidEmailAddress:(NSString*)candidate;

/// 是否为手机号
+(BOOL)isValidMobileNumber:(NSString*)string;

///判断 是否 包含: 表情、图片 (输入)
+ (BOOL)isEmojiTextFiled:(UITextField *)textField inputText:(NSString *)inputText;
+ (BOOL)isEmojiTextView:(UITextView *)textView inputText:(NSString *)inputText;

@end
