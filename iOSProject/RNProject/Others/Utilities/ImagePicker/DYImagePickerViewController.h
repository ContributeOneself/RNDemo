//
//  DYImagePickerViewController.h
//  DragonYin
//
//  Created by srxboys on 2018/4/30.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^CompletionBlock)(UIImage * image, BOOL isCancel);

typedef NS_ENUM(NSInteger, SourceType) {
    SourceTypeCamera        = UIImagePickerControllerSourceTypeCamera,          //相机
    SourceTypePhotoLibrary  = UIImagePickerControllerSourceTypePhotoLibrary,    //图库
    SourceTypePhotosAlbum   = UIImagePickerControllerSourceTypeSavedPhotosAlbum //相册
};


@interface DYImagePickerViewController : UIImagePickerController

@property(nonatomic, strong) UIView * customCameraOverlayView;

- (void)openImagePickerForController:(UIViewController*)controller  sourceType:(SourceType)type completion:(CompletionBlock)completion;

@end


/*
      调用者【注意】
 
    1、创建这个对象
    2、使用 openImagePickerForController 即可
 
    3、 无需代理、无需判断任何逻辑，统统这里处理
 
 */
