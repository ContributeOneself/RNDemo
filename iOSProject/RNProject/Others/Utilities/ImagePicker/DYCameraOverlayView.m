//
//  DYCameraOverlayView.m
//  DragonYin
//
//  Created by srxboys on 2018/5/2.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYCameraOverlayView.h"
@implementation DYCameraOverlayView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configSelf];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configSelf];
    }
    return self;
}

- (void)configSelf {
    self.userInteractionEnabled = NO;
    self.backgroundColor = [UIColor clearColor];
    
    _hintLabel = [[UILabel alloc] init];
    _hintLabel.center = self.center;
    _hintLabel.backgroundColor = [UIColor clearColor];
    _hintLabel.textAlignment = NSTextAlignmentCenter;
    _hintLabel.font = [UIFont systemFontOfSize:18];
    _hintLabel.textColor = [UIColor whiteColor];
    //        _hintLabel.transform = CGAffineTransformMakeRotation(M_PI_2); //旋转
    [self addSubview:_hintLabel];
}


- (void)setCenter:(CGPoint)center {
    [super setCenter:center];
    _hintLabel.center = self.center;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    _hintLabel.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
}

- (void)drawRect:(CGRect)rect {
    
    //水印图片
    UIImage * watermark = [UIImage imageNamed:@"water_mark"];
    if(watermark) {
        [watermark drawInRect:rect];
    }
}

@end
