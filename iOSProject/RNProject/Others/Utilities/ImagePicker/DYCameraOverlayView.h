//
//  DYCameraOverlayView.h
//  DragonYin
//
//  Created by srxboys on 2018/5/2.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DYCameraOverlayView : UIView
@property(nonatomic, strong, readonly) UILabel *hintLabel;
@end
