//
//  DYImagePickerViewController.m
//  DragonYin
//
//  Created by srxboys on 2018/4/30.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYImagePickerViewController.h"

//判断用户是否有权限访问相机
#import <AVFoundation/AVCaptureDevice.h>
#import <AVFoundation/AVMediaFormat.h>

@interface DYImagePickerViewController ()
<
UINavigationControllerDelegate,
UIImagePickerControllerDelegate
>
@property (nonatomic, strong) UIViewController * superController;
@property (nonatomic, copy) CompletionBlock completionBlock;

@end

@implementation DYImagePickerViewController

//改变 控制器的 状态栏的 颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}


- (void)openImagePickerForController:(UIViewController *)controller  sourceType:(SourceType)type completion:(CompletionBlock)completion {

    if(!controller) {
        controller = [[PageRouter sharedPageRouter] currentController];
    }
    self.superController = controller;
    
    
    weak(weakSelf);
    if(type == SourceTypeCamera) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied) {
                //权限打开相机
                [weakSelf showAlertMessage:@"请为龙吟开启相机权限：手机设置->隐私->相机->龙吟（打开）" leftTitle:@"确定" rightTitle:@"取消" actionHandler:^(NSInteger clickIndex) {
                    if(clickIndex == -1) return ;
                    [weakSelf openSetting];
                }];
            }
            else {
                weakSelf.completionBlock = completion;
                //打开相机
                [weakSelf loadImageWithController:controller sourceType:SourceTypeCamera];
            }
            
        }
        else {
            [weakSelf showToash:@"相机不可用"];
        }
    }
    else if(type == SourceTypePhotoLibrary) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            weakSelf.completionBlock = completion;

            [self loadImageWithController:controller sourceType:SourceTypePhotoLibrary];
        }else {
            [self showToash:@"相册不可用"];
        }
    }
    else if(type == SourceTypePhotosAlbum) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
            [self loadImageWithController:controller sourceType:SourceTypePhotosAlbum];
        }else {
            [self showToash:@"相册不可用"];
        }
    }
}

//调用相册或者相机的方法
- (void)loadImageWithController:(UIViewController *)c sourceType:(SourceType)sourceType {
    self.sourceType = (UIImagePickerControllerSourceType)sourceType;
    if(self.customCameraOverlayView) {
        self.customCameraOverlayView.frame = self.view.bounds;
        self.cameraOverlayView = self.customCameraOverlayView;
    }
    self.allowsEditing = YES;
    self.delegate = self;
    weak(weakSelf);
    [c presentViewController:self animated:YES completion:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf setNeedsStatusBarAppearanceUpdate];
            [weakSelf.superController setNeedsStatusBarAppearanceUpdate];
        });
    }];
}

//实现UIImagePickerController协议方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    weak(weakSelf);
    
    UIImage * image = [info valueForKey:UIImagePickerControllerEditedImage];
    image = [[UIImage alloc] initWithData:UIImageJPEGRepresentation(image, 0.1)];

    [weakSelf dismissViewControllerAnimated:YES completion:^{
        [weakSelf.superController setNeedsStatusBarAppearanceUpdate];
        if(weakSelf.completionBlock) {
            weakSelf.completionBlock(image, info == nil);
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    weak(weakSelf);
    [weakSelf dismissViewControllerAnimated:NO completion:^{
        [weakSelf.superController setNeedsStatusBarAppearanceUpdate];
        if(weakSelf.completionBlock) {
            weakSelf.completionBlock(nil, YES);
        }
    }];
}

- (void)openSetting {
    NSURL *url = [NSURL URLWithString:@"Prefs:root=Photos"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        // 系统的 相机/相册 设置界面
        [[UIApplication sharedApplication] openURL:url];
    } else {
        // 系统大于10的时候直接打开当前App的设置界面
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}


- (void)showToash:(NSString *)message {
    UIView * navView = self.superController.view;
    if(self.superController.navigationController) {
        navView = self.superController.navigationController.view;
    }
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
    [hud showHudLabel:NonEmptyString(message)];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
