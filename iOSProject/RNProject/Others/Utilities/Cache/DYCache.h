//
//  DYCache.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@interface DYCache : NSObject
+ (instancetype)defaultCache;
@property(nonatomic, strong, readonly) NSString *filename;

- (instancetype)initWithFilename:(NSString *)filename;

+ (instancetype)cacheWithFilename:(NSString *)filename;

+ (void)removeFile:(NSString *)filename;

- (void)setObject:(NSString *)obj forKey:(NSString *)key;
//加密缓存
- (void)setObject:(NSString *)obj forKey:(NSString *)key encrypted:(BOOL)encrypted;
- (id)objectForKey:(NSString *)key;
//获取时解密
- (id)objectForKey:(NSString *)key encrypted:(BOOL)encrypted;

- (void)removeObjectForKey:(NSString *)key;

- (NSString *)filePath;
@end
