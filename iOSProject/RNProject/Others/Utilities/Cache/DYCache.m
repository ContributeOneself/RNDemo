//
//  DYCache.m
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYCache.h"
#import "RXEncrypt.h"

#define RootCacheDirname @"rx_dy_cache"
#define DefaultFilename  @"default.plist"


@implementation DYCache
#pragma mark - 初始化
+ (instancetype)defaultCache {
    return [[self alloc] initWithFilename:DefaultFilename];
}

- (instancetype)initWithFilename:(NSString *)filename {
    self = [super init];
    if(self) {
        _filename = filename;
    }
    return self;
}

+ (instancetype)cacheWithFilename:(NSString *)filename {
    return [[self alloc] initWithFilename:filename];
}

#pragma mark - 删除目录
+ (void)removeFile:(NSString *)filename {
    NSString *filePath = [[self userCacheDirPath] stringByAppendingPathComponent:filename];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        if (error) {
            NSLog(@"%@",error);
        }
    }
}

#pragma mark - 设置缓存值
- (void)setObject:(NSString *)obj forKey:(NSString *)key {
    [self setObject:obj forKey:key encrypted:NO];
}

//加密缓存
- (void)setObject:(NSString *)obj forKey:(NSString *)key encrypted:(BOOL)encrypted {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:[self filePath]];
    if (dict == nil)
        dict = [NSMutableDictionary dictionary];
    
    if (encrypted) {
        
        NSString *encryptString = RXEncrypt->BASE64_encodeData(obj);
        RXEncrypt_destory;
        [dict setObject:encryptString forKey:key];
        [self writeToFile:dict];
    } else {
        [dict setObject:obj forKey:key];
        [self writeToFile:dict];
    }
}

- (void)writeToFile:(NSDictionary *)dict
{
    BOOL ok = [dict writeToFile:[self filePath] atomically:YES];
    if (!ok)
        NSLog(@"write file failed");
}

#pragma mark - 获取缓存值
- (id)objectForKey:(NSString *)key {
    return [self objectForKey:key encrypted:NO];
}

//获取时解密
- (id)objectForKey:(NSString *)key encrypted:(BOOL)encrypted {
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[self filePath]];
    NSString *value = dict[key];
    if (encrypted) {
        NSString *decryptString = RXEncrypt->BASE64_decodeData(value);
        RXEncrypt_destory;
        return decryptString;
    } else {
        return value;
    }
}

#pragma mark - 删除缓存值
- (void)removeObjectForKey:(NSString *)key {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:[self filePath]];
    [dict removeObjectForKey:key];
    [self writeToFile:dict];
}



+ (NSString *)userCacheDirPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = paths[0];
    NSString *cacheDirPath = [docPath stringByAppendingPathComponent:RootCacheDirname];
    if (![[NSFileManager defaultManager] fileExistsAtPath:cacheDirPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheDirPath withIntermediateDirectories:YES attributes:nil error:NULL];
    return cacheDirPath;
}

- (NSString *)filePath
{
    NSString *filePath = [[DYCache userCacheDirPath] stringByAppendingPathComponent:self.filename];
    return filePath;
}

@end
