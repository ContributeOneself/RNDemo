//
//  SCMHud.m
//  SCM
//
//  Created by srx on 2017/12/8.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMHud.h"



@implementation MBProgressHUD (SCMHud)

/// 转菊花 + 文字 (竖 显示)
- (void)showHudLabel:(NSString *)txt {
    self.mode = MBProgressHUDModeText;
    self.label.numberOfLines = 0;
    self.label.text = txt;
    [self hideAnimated:YES afterDelay:1.5f];
}

/// 转菊花 + 标题 + 内容 (竖 显示)
- (void)showHudLabel:(NSString *)txt detail:(NSString *)detail {
    self.mode = MBProgressHUDModeText;
    self.label.numberOfLines = 0;
    self.label.text = txt;
    self.detailsLabel.text = detail;
    [self hideAnimated:YES afterDelay:1.5f];
}

///  进度加载 + 文字(如: 下载中、loading)
- (void)showHudDeterminateLoading:(NSString *)loadingTxt {
    self.mode = MBProgressHUDModeDeterminate;
    self.label.numberOfLines = 0;
    self.label.text = loadingTxt;
    [self hideAnimated:YES afterDelay:1.5f];
}

///  进度加载(内圈描边)
- (void)showHudProgress:(NSProgress *)progress labelTxt:(NSString *)txt {
    self.mode = MBProgressHUDModeDeterminate;
    self.progressObject = progress;
    self.label.numberOfLines = 0;
    self.label.text = txt;
}

///  进度加载(描线)
- (void)showHudAnnularProgress:(NSProgress *)progress labelTxt:(NSString *)txt{
    self.mode = MBProgressHUDModeAnnularDeterminate;
    self.progressObject = progress;
    self.label.numberOfLines = 0;
    self.label.text = txt;
}

///  进度加载(横向 进度加载)
- (void)showHudBarProgress:(NSProgress *)progress labelTxt:(NSString *)txt {
    self.mode = MBProgressHUDModeDeterminateHorizontalBar;
    self.progressObject = progress;
    self.label.numberOfLines = 0;
    self.label.text = txt;
}


- (void)showHudLoading:(NSString *)txt {
    self.mode = MBProgressHUDModeIndeterminate;
    self.label.numberOfLines = 0;
    self.label.text = txt;
}

- (void)showHudLoading:(NSString *)txt detail:(NSString *)detail {
    self.mode = MBProgressHUDModeIndeterminate;
    self.label.numberOfLines = 0;
    self.label.text = txt;
    self.detailsLabel.text = detail;
}

- (void)hiddenHud {
    [self hideAnimated:YES];
}

@end
