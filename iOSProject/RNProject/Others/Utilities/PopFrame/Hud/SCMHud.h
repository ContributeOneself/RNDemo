//
//  SCMHud.h
//  SCM
//
//  Created by srx on 2017/12/8.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (SCMHud)


/*
    使用说明(下载页面，在下载类里面做了)
    【 这里不是扩展，建议用原生的写，这里可以看做为demo 】
 
    一、普通页面(仅仅 转菊花)
     MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
 
    二、如果想其他样式
     [hud showHudLabel:@"错误"];
     ...
 
    三、
     dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
         // Do something useful in the background and update the HUD periodically.
             处理内部内容
         dispatch_async(dispatch_get_main_queue(), ^{
             [hud hideAnimated:YES]; 、 [hud hiddenHud]
         });
     });
 
    三、消失
     [hud hiddenHud] 、 [hud hideAnimated:YES];
 */

/// 转菊花 + 文字 (竖 显示) (1.5s 后自动消失)
- (void)showHudLabel:(NSString *)txt;
/// 转菊花 + 标题 + 内容 (竖 显示)(1.5s 后自动消失)
- (void)showHudLabel:(NSString *)txt detail:(NSString *)detail;

///  进度加载 + 文字(如: 下载中、loading)(1.5s 后自动消失)
- (void)showHudDeterminateLoading:(NSString *)loadingTxt;

///  进度加载 + 文字(如: 取消、关闭) 自己写吧！
///  进度加载(内圈描边)
- (void)showHudProgress:(NSProgress *)progress labelTxt:(NSString *)txt;
///  进度加载(描线)
- (void)showHudAnnularProgress:(NSProgress *)progress labelTxt:(NSString *)txt;
///  进度加载(横向 进度加载)
- (void)showHudBarProgress:(NSProgress *)progress labelTxt:(NSString *)txt;


- (void)showHudLoading:(NSString *)txt;
- (void)showHudLoading:(NSString *)txt detail:(NSString *)detail;

- (void)hiddenHud;

@end
