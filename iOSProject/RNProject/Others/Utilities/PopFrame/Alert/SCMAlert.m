//
//  SCMAlert.m
//  SCM
//
//  Created by srx on 2017/12/6.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMAlert.h"
#import "SCMAlertController.h"

@implementation NSObject (SCMToash)

- (void)showAlertMessage:(NSString *)message {
    [self showAlertMessage:message actionHandler:nil];
}

- (void)showAlertMessage:(NSString *)message actionHandler:(void (^)(NSInteger))actionHandler {
    [self showAlertMessage:message leftTitle:nil rightTitle:Localized(LOCAL_CLOSE) actionHandler:actionHandler];
}

- (void)showAlertMessage:(NSString *)message leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle actionHandler:(void(^)(NSInteger clickIndex))actionHandler {
    [self showAlertTitle:nil message:message leftTitle:leftTitle rightTitle:rightTitle actionHandler:actionHandler];
}


- (void)showAlertTitle:(NSString *)title message:(NSString *)message {
    [self showAlertTitle:title message:message leftTitle:nil rightTitle:Localized(LOCAL_CLOSE) actionHandler:nil];
}

- (void)showAlertTitle:(NSString *)title message:(NSString *)message leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle actionHandler:(void(^)(NSInteger clickIndex))actionHandler {
    
    if(!StrBool(title) && !StrBool(message) &&
       !StrBool(message) && !StrBool(leftTitle)
       && !StrBool(rightTitle)) {
        return;
    }
    
    if(!StrBool(leftTitle) && !StrBool(rightTitle)) {
        rightTitle = @"确定";
    }
    
    SCMAlertController * alertVC = [SCMAlertController alertControllerWithTitle:NonEmptyString(title) message:NonEmptyString(message) preferredStyle:UIAlertControllerStyleAlert];
    
    if(StrBool(leftTitle)) {
        UIAlertAction * trueAction = [UIAlertAction actionWithTitle:leftTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            //确定
            if(actionHandler) {
                actionHandler(0);
            }
        }];
        
        [alertVC addAction:trueAction];
    }
    
    if(StrBool(rightTitle)) {
        UIAlertAction * trueCancel = [UIAlertAction actionWithTitle:rightTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //取消
            if(actionHandler) {
                actionHandler(-1);
            }
        }];
        
        [alertVC addAction:trueCancel];
    }
    
    [[PageRouter sharedPageRouter] present:alertVC];
}


- (void)showAlertTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle trueTitle:(NSString *)trueTitle actionHandler:(void(^)(NSInteger clickIndex))actionHandler {
    
    if(!StrBool(title) && !StrBool(message) &&
       !StrBool(message) && !StrBool(trueTitle)
       && !StrBool(cancelTitle)) {
        return;
    }
    
    SCMAlertController * alertVC = [SCMAlertController alertControllerWithTitle:NonEmptyString(title) message:NonEmptyString(message) preferredStyle:UIAlertControllerStyleAlert];
    
    if(StrBool(cancelTitle)) {
        UIAlertAction * trueCancel = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            //取消
            if(actionHandler) {
                actionHandler(-1);
            }
        }];
        
        [alertVC addAction:trueCancel];
    }
    
    if(StrBool(trueTitle)) {
        UIAlertAction * trueAction = [UIAlertAction actionWithTitle:trueTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            //确定
            if(actionHandler) {
                actionHandler(0);
            }
        }];
        
        [alertVC addAction:trueAction];
    }
   
    
    [[PageRouter sharedPageRouter] present:alertVC];
}



- (void)showSheetTitle:(NSString *)title message:(NSString *)message nameArray:(NSArray <NSString *>*)nameArray cancelName:(NSString *)cancelName actionHandler:(void(^)(NSInteger clickIndex))actionHandler {
    
    SCMAlertController * alertVC = [SCMAlertController alertControllerWithTitle:NonEmptyString(title) message:NonEmptyString(message) preferredStyle:UIAlertControllerStyleActionSheet];
    
    for(NSInteger i = 0; i < nameArray.count; i++) {
        NSString * name = nameArray[i];
        if(StrBool(name)) {
            UIAlertAction * trueAction = [UIAlertAction actionWithTitle:name style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //确定
                if(actionHandler) {
                    actionHandler(i);
                }
            }];            
            [alertVC addAction:trueAction];
        }
    }
    
    if(StrBool(cancelName)) {
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:cancelName style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            //取消
            if(actionHandler) {
                actionHandler(-1);
            }
        }];
        
        [alertVC addAction:cancelAction];
    }
    
    [[PageRouter sharedPageRouter] present:alertVC];
}

@end
