//
//  DYTextAlertView.m
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYTextAlertView.h"

@implementation DYTextAlertView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat scroHeight = 0;
    CGFloat paddingSpace = 20;
    CGRect baseFrame = CGRectMake(0, 0, self.contentWidth-_buttonHeight, CGFLOAT_MAX);
    
    scroHeight += paddingSpace;
    if(self.titleLabel) {
        CGFloat titleHeight = [_titleLabel textRectForBounds:baseFrame limitedToNumberOfLines:0].size.height;
        titleHeight = ceilf(titleHeight);
        if(titleHeight> 0) {
            _titleLabel.frame = CGRectMake(paddingSpace, scroHeight, self.contentWidth-_buttonHeight, titleHeight);
            scroHeight += titleHeight + 10;
        }
        else {
            _titleLabel.frame = CGRectZero;
        }
    }
    
    if(self.messageLabel) {
        CGFloat messageHeight = [_messageLabel textRectForBounds:baseFrame limitedToNumberOfLines:0].size.height;
        messageHeight = ceilf(messageHeight);
        if(messageHeight > 0) {
            _messageLabel.frame = CGRectMake(paddingSpace, scroHeight, self.contentWidth-_buttonHeight, messageHeight);
            scroHeight += messageHeight + paddingSpace;
        }
        else {
            _messageLabel.frame = CGRectZero;
            scroHeight += 10;
        }
    }
    else {
        scroHeight += 10;
    }
    
    scroHeight -= paddingSpace;
    self.contentScrollViewSize = CGSizeMake(self.contentWidth, scroHeight);
    
    if(scroHeight > self.maxContentHeight - 50.5) {
        self.contentScrollViewSize = CGSizeMake(self.contentWidth, scroHeight+paddingSpace);
        scroHeight = self.maxContentHeight - 50.5;
        self.contentScrollView.showsVerticalScrollIndicator = YES;
    }
    else {
        self.contentScrollView.showsVerticalScrollIndicator = NO;
    }
    
    self.contentScrollView.frame = CGRectMake(0, 0, self.contentWidth, scroHeight);
    CGFloat height = scroHeight + paddingSpace + 50.5;
    CGFloat top = ceilf((SCREEN_HEIGHT - height)/2.0);
    CGFloat left = ceilf((SCREEN_WIDTH - self.contentWidth)/2.0);
    self.contentView.frame = CGRectMake(left, top, self.contentWidth, height);
    
    top = height - 50 - 0.5;
    _horizontalLineView.frame = CGRectMake(0, top, self.contentWidth, 0.5);
    top += 0.5;
    
    if(self.cancelButtonHidden) {
        _rightButton.frame = CGRectMake(0, top, self.contentWidth, 50);
    }
    else {
        CGFloat middleX = ceilf((self.contentWidth - 0.5)/2.0);
        _cancelButton.frame = CGRectMake(0, top, middleX, 50);
        if(self.verticalLineLongHidden) {
            _verticalLineView.frame = CGRectMake(middleX, top, 0.5, 50);
        }
        else {
            _verticalLineView.frame = CGRectMake(middleX, top+10, 0.5, 50-20);
        }
        left = middleX+0.5;
        _rightButton.frame = CGRectMake(left, top, self.contentWidth-left, 50);
    }
    
    _closeButton.frame = CGRectMake(self.contentWidth -10-16-10, 0, 16+20, 16+20);
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _buttonHeight = 40;
        
        _closeButton = [self getBaseButton];
        [_closeButton setImage:[UIImage imageNamed:@"icon_16x_close"] forState:UIControlStateNormal];
        [self.contentView addSubview:_closeButton];
        
        _titleLabel = [UILabel new];
        _titleLabel.numberOfLines = 0;
//        _titleLabel.font = [YLUIUtils fontOfSize:18 weight:YLUIFontWeight_medium];
//        _titleLabel.textColor = FONT_YL_COLOR_BLACK_90;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentScrollView addSubview:_titleLabel];
        
        _messageLabel = [UILabel new];
        _messageLabel.numberOfLines = 0;
//        _messageLabel.font = [YLUIUtils fontOfSize:15 weight:YLUIFontWeight_regular];
//        _messageLabel.textColor = FONT_YL_COLOR_BLACK_60;
        [self.contentScrollView addSubview:_messageLabel];
        
        _horizontalLineView = [self getLineView];
        _verticalLineView = [self getLineView];
        
        _cancelButton = [self getCancelButton];
        _rightButton = [self getRightButton];
        
        self.closeButtonHidden = NO;
        self.cancelButtonHidden = YES;
    }
    return self;
}

- (UIView *)getLineView {
    UIView * view = [UIView new];
//    view.backgroundColor = FONT_YL_COLOR_BLACK(0.5);
    [self.contentView addSubview:view];
    return view;
}

- (UIButton *)getBaseButton {
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn.titleLabel.font = [YLUIUtils fontOfSize:18 weight:YLUIFontWeight_medium];
    [btn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:btn];
    return btn;
}

- (UIButton *)getCancelButton {
    UIButton * btn = [self getBaseButton];
    btn.backgroundColor = [UIColor whiteColor];
//    [btn setTitleColor:FONT_YL_COLOR_BLACK_90 forState:UIControlStateNormal];
    return btn;
}
- (UIButton *)getRightButton {
    UIButton * btn = [self getBaseButton];
    btn.backgroundColor = [UIColor whiteColor];
//    [btn setTitleColor:FONT_YL_COLOR_BLUE forState:UIControlStateNormal];
    return btn;
}

- (void)buttonClick:(UIButton *)btn {
    if(btn == _closeButton) {
        if(self.closeHandler) {
            self.closeHandler();
        }
    }
    else if(btn == _cancelButton) {
        if(self.cancelHandler) {
            self.cancelHandler();
        }
    }
    else if(btn == _rightButton) {
        if(self.openHandler) {
            self.openHandler();
        }
    }
    
    [self dismiss];
}


- (void)setCloseButtonHidden:(BOOL)closeButtonHidden {
    _closeButtonHidden = closeButtonHidden;
    _closeButton.hidden = closeButtonHidden;
}

- (void)setCancelButtonHidden:(BOOL)cancelButtonHidden {
    _cancelButtonHidden = cancelButtonHidden;
    _cancelButton.hidden = cancelButtonHidden;
}

- (void)setVerticalLineHidden:(BOOL)verticalLineHidden {
    _verticalLineHidden = verticalLineHidden;
    _verticalLineView.hidden = verticalLineHidden;
}

- (void)setHorizontalLineHidden:(BOOL)horizontalLineHidden {
    _horizontalLineHidden = horizontalLineHidden;
    _horizontalLineView.hidden = horizontalLineHidden;
}


- (void)setTitle:(NSString *)title {
    _title = title;
    _titleLabel.text = title;
    [self setNeedsLayout];
}

- (void)setMessage:(NSString *)message {
    _message = message;
    _messageLabel.text = message;
    [self setNeedsLayout];
}

- (void)setCancelButtonTitle:(NSString *)cancelButtonTitle {
    _cancelButtonTitle = cancelButtonTitle;
    [_cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
}

- (void)setRightButtonTitle:(NSString *)rightButtonTitle {
    _rightButtonTitle = rightButtonTitle;
    [_rightButton setTitle:rightButtonTitle forState:UIControlStateNormal];
}
@end
