//
//  DYBaseAlertView.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//
// 基类

#import <UIKit/UIKit.h>

typedef void(^DissmissHandler)(void);

@interface DYBaseAlertView : UIView

@property (nonatomic, copy) UIViewController * presentedViewController; //该值 pre 本页面

//UI
@property (nonatomic, copy, readonly) UIView * backView;    //背景(黑色 0.5 透明度)
@property (nonatomic, copy, readonly) UIView * contentView; // 主内容view（添加 确定、取消按钮）
@property (nonatomic, copy, readonly) UIScrollView * contentScrollView;// 滚动图(添加内容，超过一屏，可以滚动 标题)

//property
@property (nonatomic, assign) CGFloat contentViewCornerRadius;//默认圆角 10(主内容view)
@property (nonatomic, assign) CGSize contentScrollViewSize;   //(scrollView)
@property (nonatomic, assign) CGFloat contentWidth;   //默认 300
@property (nonatomic, assign, readonly) CGFloat maxContentHeight; //主内容最大的高度
@property (nonatomic, copy) DissmissHandler dissmissHandler;

//function
- (void)show NS_REQUIRES_SUPER;
- (void)showWithViewController:(UIViewController *)vc NS_REQUIRES_SUPER;

- (void)dismiss NS_REQUIRES_SUPER;


- (void)showAnimal NS_REQUIRES_SUPER;
- (void)hiddenAnimal NS_REQUIRES_SUPER;
@end
