//
//  DYBaseAlertView.m
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYBaseAlertView.h"

@interface DYBaseAlertView()
@property (nonatomic, copy) UIViewController * viewController;
@end

@implementation DYBaseAlertView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.frame = [[UIScreen mainScreen] bounds];
    _backView.frame = self.bounds;
}

#pragma mark - init

- (instancetype)init {
    self = [super init];
    if (self) {
        _contentWidth = 300;
        
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor blackColor];
        _backView.alpha = 0.5;
        [self addSubview:_backView];
        
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor whiteColor];
        _contentView.layer.cornerRadius = 10.0f;
        _contentView.clipsToBounds = YES;
        [self addSubview:_contentView];
        
        _contentScrollView = [UIScrollView new];
        _contentScrollView.backgroundColor = [UIColor clearColor];
        _contentScrollView.clipsToBounds = YES;
        _contentScrollView.showsVerticalScrollIndicator = NO;
        _contentScrollView.showsHorizontalScrollIndicator = NO;
        _contentScrollView.bounces = NO;
        [_contentView addSubview:_contentScrollView];
        
        
    }
    return
    
    self;
}


#pragma mark - property
- (void)setContentViewCornerRadius:(CGFloat)contentViewCornerRadius {
    _contentViewCornerRadius = contentViewCornerRadius;
    _contentView.layer.cornerRadius = contentViewCornerRadius;
}

- (void)setContentScrollViewSize:(CGSize)contentScrollViewSize {
    _contentScrollViewSize = contentScrollViewSize;
    _contentScrollView.contentSize = contentScrollViewSize;
}

- (CGFloat)maxContentHeight {
    return SCREEN_HEIGHT - STATUS_BAR_HEIGHT*2;
}

- (UIViewController *)viewController {
    if(!_viewController) {
        _viewController = [UIViewController new];
    }
    return _viewController;
}

- (UIViewController *)presentedViewController {
    if(!_presentedViewController) {
        UIViewController* presentedVC = [[UIApplication sharedApplication].delegate window].rootViewController;
        while (presentedVC.presentedViewController) {
            presentedVC = presentedVC.presentedViewController;
        }
        _presentedViewController = presentedVC;
    }
    return _presentedViewController;
}

#pragma mark function & block

- (void)showWithViewController:(UIViewController *)vc {
    _presentedViewController = vc;
    [self show];
}

- (void)show {
    [self layoutIfNeeded];
    
    [self.presentedViewController addChildViewController:self.viewController];
    [self.presentedViewController.view addSubview:self.viewController.view];
    [self.viewController.view addSubview:self];
    [self showAnimal];
}

- (void)dismiss {
    if(_dissmissHandler) {
        _dissmissHandler();
    }
    if(_viewController.view.superview) {
        [_viewController.view removeFromSuperview];
        [_viewController removeFromParentViewController];
    }
    [self removeFromSuperview];
    [self hiddenAnimal];
}

- (void)showAnimal {
    CAKeyframeAnimation *popAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    popAnimation.duration = 0.2;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.05, 1.05, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    popAnimation.values = values;
    [_contentView.layer addAnimation:popAnimation forKey:@"srxboys_animl"];
}

- (void)hiddenAnimal {
    [_contentView.layer removeAnimationForKey:@"srxboys_animl"];
}

@end
