//
//  DYTextAlertView.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYBaseAlertView.h"

typedef void(^CancelHandler)(void);
typedef void(^OpenHandler)(void);

@interface DYTextAlertView : DYBaseAlertView
@property (nonatomic, copy) UIButton * closeButton;
@property (nonatomic, copy) UILabel  * titleLabel;
@property (nonatomic, copy) UILabel  * messageLabel;
@property (nonatomic, copy) UIView   * horizontalLineView;
@property (nonatomic, copy) UIView   * verticalLineView;
@property (nonatomic, copy) UIButton * cancelButton;
@property (nonatomic, copy) UIButton * rightButton;

@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * message;
@property (nonatomic, assign) BOOL     closeButtonHidden; //默认 开启
@property (nonatomic, assign) BOOL     cancelButtonHidden;//默认 关闭
@property (nonatomic, assign) BOOL     horizontalLineHidden;
@property (nonatomic, assign) BOOL     verticalLineHidden;
@property (nonatomic, assign) BOOL     verticalLineLongHidden; //竖线通长, 默认 不通长
@property (nonatomic, copy) NSString * cancelButtonTitle;
@property (nonatomic, copy) NSString * rightButtonTitle;
@property (nonatomic, assign) CGFloat  buttonHeight; //默认 40

// block & function
@property (nonatomic, copy) CancelHandler closeHandler;
@property (nonatomic, copy) CancelHandler cancelHandler;
@property (nonatomic, copy) OpenHandler   openHandler;
@end
