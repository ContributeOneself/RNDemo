//
//  DYImageTextAlertView.h
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYTextAlertView.h"

@interface DYImageTextAlertView : DYTextAlertView
//UI
@property (nonatomic, copy) UIImageView * imageView;
@property (nonatomic, copy) UIView   * customView; //自定义view


//property
@property (nonatomic, copy) NSString * imageName;

@property (nonatomic, assign) BOOL     imageViewHidden;
@property (nonatomic, assign) CGFloat  imageViewHeight;//默认 140
@property (nonatomic, assign) CGFloat  customHeight;  //自定义view 默认40

@end
