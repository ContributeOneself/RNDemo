//
//  DYImageTextAlertView.m
//  DragonYin
//
//  Created by srxboys on 2018/4/20.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYImageTextAlertView.h"

@implementation DYImageTextAlertView
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat scroHeight = 0;
    if(!self.imageViewHidden) {
        self.imageView.frame = CGRectMake(0, 0, self.contentWidth, self.imageViewHeight);
        scroHeight += self.imageViewHeight;
    }
    else {
        scroHeight = 6;
    }
    
    CGFloat paddingSpace = 20;
    CGFloat bottom = 30;
    CGRect baseFrame = CGRectMake(0, 0, self.contentWidth-self.buttonHeight, CGFLOAT_MAX);
    scroHeight += paddingSpace;
    if(self.titleLabel) {
        CGFloat titleHeight = [self.titleLabel textRectForBounds:baseFrame limitedToNumberOfLines:0].size.height;
        titleHeight = ceilf(titleHeight);
        if(titleHeight> 0) {
            self.titleLabel.frame = CGRectMake(paddingSpace, scroHeight, self.contentWidth-self.buttonHeight, titleHeight);
            scroHeight += titleHeight + 10;
        }
        else {
            self.titleLabel.frame = CGRectZero;
        }
    }
    
    if(self.messageLabel) {
        CGFloat messageHeight = [self.messageLabel textRectForBounds:baseFrame limitedToNumberOfLines:0].size.height;
        messageHeight = ceilf(messageHeight);
        if(messageHeight > 0) {
            self.messageLabel.frame = CGRectMake(paddingSpace, scroHeight, self.contentWidth-self.buttonHeight, messageHeight);
            scroHeight += messageHeight + paddingSpace;
        }
        else {
            self.messageLabel.frame = CGRectZero;
            scroHeight += 10;
        }
    }
    else {
        scroHeight += 10;
    }
    
    if(self.customView) {
        scroHeight = scroHeight-paddingSpace+15;
        self.customView.frame = CGRectMake(0, scroHeight, self.contentWidth, self.customHeight);
        scroHeight += self.customHeight + paddingSpace;
    }
    
    scroHeight -= paddingSpace;
    self.contentScrollViewSize = CGSizeMake(self.contentWidth, scroHeight);
    
    if(scroHeight > self.maxContentHeight - self.buttonHeight - bottom) {
        self.contentScrollViewSize = CGSizeMake(self.contentWidth, scroHeight+paddingSpace);
        scroHeight = self.maxContentHeight - self.buttonHeight - bottom;
        self.contentScrollView.showsVerticalScrollIndicator = YES;
    }
    else {
        self.contentScrollView.showsVerticalScrollIndicator = NO;
    }
    
    
    self.contentScrollView.frame = CGRectMake(0, 0, self.contentWidth, scroHeight);
    
    CGFloat height = scroHeight + paddingSpace + self.buttonHeight + bottom;
    CGFloat top = ceilf((SCREEN_HEIGHT - height)/2.0);
    CGFloat left = ceilf((SCREEN_WIDTH - self.contentWidth)/2.0);
    self.contentView.frame = CGRectMake(left, top, self.contentWidth, height);
    self.closeButton.frame = CGRectMake(self.contentWidth -10-16-10, 0, 16+20, 16+20);
    
    top = height - bottom - self.buttonHeight;
    if(self.cancelButtonTitle.length) {
        CGFloat btnWidth = 125;
        CGFloat left = ceilf((self.contentWidth - btnWidth*2-10)/2.0);
        self.cancelButton.frame = CGRectMake(left, top, btnWidth, self.buttonHeight);
        self.rightButton.frame = CGRectMake(left+btnWidth+10, top, btnWidth, self.buttonHeight);
    }
    else {
        self.rightButton.frame = CGRectMake(paddingSpace, top, self.contentWidth - paddingSpace*2, self.buttonHeight);
    }
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _imageViewHeight = 140;
        
        _imageView = [UIImageView new];
        [self.contentScrollView addSubview:_imageView];
    }
    return self;
}

- (void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    _imageView.image = [UIImage imageNamed:imageName];
}

- (void)setCustomView:(UIView *)customView {
    if(_customView) {
        [_customView removeFromSuperview];
    }
    _customView = customView;
    [self.contentScrollView addSubview:customView];
    [self setNeedsLayout];
}

- (void)show {
    [self layoutIfNeeded];
    [super show];
}

@end
