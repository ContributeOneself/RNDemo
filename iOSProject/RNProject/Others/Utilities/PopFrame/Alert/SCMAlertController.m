//
//  SCMAlertController.m
//  SCM
//
//  Created by srx on 2017/12/15.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMAlertController.h"

@interface SCMAlertController ()

@end

@implementation SCMAlertController

//改变 控制器的 状态栏的 颜色
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
