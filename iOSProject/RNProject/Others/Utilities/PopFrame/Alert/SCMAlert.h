//
//  SCMAlert.h
//  SCM
//
//  Created by srx on 2017/12/6.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSObject(SCMToash)
/**
 * alert
 *
 *  项目中常用
 *  cancel->clickIndex = -1
 *  left = true(红色)
 *  right = cancel(蓝色)
 */
- (void)showAlertMessage:(NSString *)message;
- (void)showAlertMessage:(NSString *)message actionHandler:(void(^)(NSInteger clickIndex))actionHandler;
- (void)showAlertMessage:(NSString *)message leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle actionHandler:(void(^)(NSInteger clickIndex))actionHandler;

- (void)showAlertTitle:(NSString *)title message:(NSString *)message;
- (void)showAlertTitle:(NSString *)title message:(NSString *)message leftTitle:(NSString *)leftTitle rightTitle:(NSString *)rightTitle actionHandler:(void(^)(NSInteger clickIndex))actionHandler;


/**
 * alert
 *
 *  更新版本用
 *  cancel->clickIndex = -1
 *  cancelTitle = cancel(蓝色)
 *  trueTitle = true(红色)
 */
- (void)showAlertTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle trueTitle:(NSString *)trueTitle actionHandler:(void(^)(NSInteger clickIndex))actionHandler;


/**
 * sheet
 *
 *  cancel->clickIndex = -1
 */

- (void)showSheetTitle:(NSString *)title message:(NSString *)message nameArray:(NSArray <NSString *>*)nameArray cancelName:(NSString *)cancelName actionHandler:(void(^)(NSInteger clickIndex))actionHandler;


@end
