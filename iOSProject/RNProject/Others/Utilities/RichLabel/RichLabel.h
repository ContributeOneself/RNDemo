//
//  RichLabel.h
//  DragonYin
//
//  Created by srxboys on 2018/4/25.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface RichLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets contentInset;

@property (nonatomic, copy) NSString *iconName;

@property (nonatomic, assign) CGSize iconSize;
//@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, strong) UIColor *anchorTextColor;

@property (nonatomic, strong) UIColor *anchorTextHighlightColor;

//@property (nonatomic, copy) NSString *text;

- (void)setHighlightText:(NSString *)highlightText
                   index:(NSUInteger)index
               withColor:(UIColor *)color;


- (void)setLinkWithText:(NSString *)text
                  index:(NSUInteger)index
             attributes:(NSDictionary *)attributes
              tapAction:(void(^)(void))tapAction;

- (CGSize)sizeWithConstraint:(CGSize)constaintSize;

@end
