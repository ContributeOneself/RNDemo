//
//  SCMNoPasteTextField.m
//  SCM
//
//  Created by srx on 2017/12/14.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "DYNoPasteTextField.h"

@implementation DYNoPasteTextField

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
    
    /*
    下面的禁止，不禁止 copy出去
     
    //禁止粘贴
    if(action== @selector(paste:)) return NO;
    
    // 禁止选择
    if(action== @selector(select:)) return NO;
    
    // 禁止全选
    if(action== @selector(selectAll:)) return NO;
    
    return [super canPerformAction:action withSender:sender];
     */
}

@end
