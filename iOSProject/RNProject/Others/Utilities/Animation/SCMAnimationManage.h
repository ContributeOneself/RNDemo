//
//  SCMAnimationManage.h
//  SCM
//
//  Created by srx on 2017/11/9.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SCM_ANIMATION) {
    SCM_ANIMATION_REVEAL = 0,// 翻页效果
    SCM_ANIMATION_RIPPLE ,   // 波纹效果
    SCM_ANIMATION_SUCK       // 吸收效果
};

@interface SCMAnimationManage : NSObject
/** 动画效果
 * @param view 执行动画的view
 * @param effect 动画效果
 * @param next  YES 下一个翻页效果。NO返回翻动效果
 */
+ (void)animationWithView:(UIView *)view andEffect:(SCM_ANIMATION)effect isNext:(BOOL)next;

/** 点击效果
 * @param view 执行动画的view
 */
+ (void)clickEffectAnimationForView:(UIView *)view;



/** 晃动效果
 * @param view 执行动画的view
 */
+ (void)animationEarthquakeWithView:(UIView *)view;





/*
 需要增加(看情况是否 改变上面的结构 <--> 一切以方便使用、简明的方式)
 
 1、抖动效果
 2、放大、缩小
 3、旋转
 4、渐入渐出

 ....

 最好链式开发(点语法)
 
 
 */


@end
