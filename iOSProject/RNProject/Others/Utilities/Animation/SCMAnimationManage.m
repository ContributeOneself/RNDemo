//
//  SCMAnimationManage.m
//  SCM
//
//  Created by srx on 2017/11/9.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMAnimationManage.h"

@implementation SCMAnimationManage
+ (void)animationWithView:(UIView *)view andEffect:(SCM_ANIMATION)effect isNext:(BOOL)next
{
    CATransition * transition = [CATransition animation];
    if (next == YES) {
        // 向下翻页
        switch (effect) {
            case SCM_ANIMATION_REVEAL:
                transition.type = @"pageUnCurl";
                transition.subtype = kCATransitionFromLeft;
                break;
            case SCM_ANIMATION_RIPPLE:
                transition.type = @"rippleEffect";
                transition.subtype = kCATransitionFromLeft;
                break;
            case SCM_ANIMATION_SUCK:
                transition.type = @"suckEffect";
                transition.subtype = kCATransitionFromLeft;
                break;
        }
    } else {
        switch (effect) {
            case SCM_ANIMATION_REVEAL:
                transition.type = @"pageCurl";
                transition.subtype = kCATransitionFromLeft;
                
                break;
            case SCM_ANIMATION_RIPPLE:
                transition.type = @"rippleEffect";
                transition.subtype = kCATransitionFromRight;
                break;
            case SCM_ANIMATION_SUCK:
                transition.type = @"suckEffect";
                transition.subtype = kCATransitionFromRight;
                break;
        }
    }
    transition.duration = 0.5;
    [view.layer addAnimation:transition forKey:nil];
    
}

+ (void)clickEffectAnimationForView:(UIView *)view
{
    CABasicAnimation * scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.fromValue = [NSNumber numberWithFloat:1.3];
    scaleAnimation.toValue = [NSNumber numberWithFloat:0.7];
    scaleAnimation.duration = 0.1;
    scaleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [view.layer addAnimation:scaleAnimation forKey:nil];
}








+ (void)animationEarthquakeWithView:(UIView *)view {
    CGFloat t = 2.0;
    CGAffineTransform leftQuake  =CGAffineTransformTranslate(CGAffineTransformIdentity, t,-t);
    CGAffineTransform rightQuake =CGAffineTransformTranslate(CGAffineTransformIdentity,-t, t);
    
    view.transform = leftQuake;  // starting point
    
    [UIView beginAnimations:@"earthquake" context:(__bridge void * _Nullable)(view)];
    [UIView setAnimationRepeatAutoreverses:YES];// important
    [UIView setAnimationRepeatCount:5];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(earthquakeEnded:finished:context:)];
    
    view.transform = rightQuake;// end here & auto-reverse
    
    [UIView commitAnimations];
}

+ (void)earthquakeEnded:(NSString*)animationID finished:(NSNumber*)finished context:(void*)context {
    if([finished boolValue]) {
        UIView* item =(__bridge UIView*)context;
        item.transform = CGAffineTransformIdentity;
    }
}
@end
