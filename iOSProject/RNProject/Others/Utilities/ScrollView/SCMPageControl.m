//
//  SCMPageControl.m
//  SCM
//
//  Created by srx on 2017/9/16.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMPageControl.h"

@interface SCMPageControl ()
@property (nonatomic, assign) CGAffineTransform nomalTransform;
@end

@implementation SCMPageControl

- (instancetype)init
{
    self = [super init];
    if (self) {
        _dotSpace = 10;
        _dotDiameterOn = 8;
        _dotDiameterOff = 5;
        _onColor = [UIColor whiteColor];
        _offColor = [UIColor lightGrayColor];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setNumPages:(NSInteger)numPages {
    _numPages = numPages;
    
    [self removeAllSubViews];
    
    NSInteger count = numPages - 1;
    CGFloat width = self.frame.size.width;
    CGFloat middle = (width-self.dotDiameterOff*count-self.dotDiameterOn-count*self.dotSpace)/2;
    
    for(NSInteger i = 0; i < numPages; i++) {
        CGFloat x = i * self.dotDiameterOff +i *self.dotSpace+middle;
        UIView * dotView = [[UIView alloc] initWithFrame:CGRectMake(x, 0, self.dotDiameterOff, self.dotDiameterOff)];
        dotView.layer.cornerRadius = self.dotDiameterOff/2.0;
        dotView.backgroundColor = self.offColor;
        dotView.clipsToBounds = YES;
        dotView.tag = i;
        [self addSubview:dotView];
        if(i == 0) {
            _nomalTransform = dotView.transform;
        }
    }
    
    [self changeSelectedPage];
}

- (void)removeAllSubViews {
    for (UIView * view in self.subviews) {
        [view removeFromSuperview];
    }
}

- (void)setCurrentPage:(NSInteger)currentPage {
    if(currentPage >= self.subviews.count) {
        currentPage = 0;
    }
    _currentPage = currentPage;
    [self changeSelectedPage];
}

- (void)changeSelectedPage {
    
    if(self.subviews.count<self.currentPage || self.currentPage < 0) {
        return;
    }
    
    CGFloat diffTransformFloat = (self.dotDiameterOn - self.dotDiameterOff)/self.dotDiameterOff;
    
    for (UIView * dotView in self.subviews) {
        if(dotView.tag == self.currentPage) {
            dotView.transform = CGAffineTransformMakeScale(1+diffTransformFloat, 1+diffTransformFloat);
            dotView.backgroundColor = self.onColor;
        }
        else {
            dotView.transform = self.nomalTransform;
            dotView.backgroundColor = self.offColor;
        }
    }
}

@end
