//
//  SCMPageControl.h
//  SCM
//
//  Created by srx on 2017/9/16.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCMPageControl : UIControl
/// 控件间距离(默认10)
@property (nonatomic, assign) CGFloat dotSpace;

/// 选择的控件大小(默认8)
@property (nonatomic, assign) CGFloat dotDiameterOn;

/// 未选择的控件的大小(默认5)
@property (nonatomic, assign) CGFloat dotDiameterOff;

/// 选择的控件颜色(默认白色)
@property (nonatomic, copy) UIColor * onColor;

/// 未选择的控件颜色(默认灰色)
@property (nonatomic, copy) UIColor * offColor;


@property (nonatomic, assign) NSInteger numPages;
@property (nonatomic, assign) NSInteger currentPage;

@end
