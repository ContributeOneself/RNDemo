//
//  BaseViewController+Facepp.m
//  DragonYin
//
//  Created by srxboys on 2018/5/4.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController+Facepp.h"

#import "FCPPSDK.h"
#import "DYFCPPBankcard.h"


UIKIT_STATIC_INLINE NSError* DYFaceNullDataError(NSString * desc) {
    return  [[NSError alloc] initWithDomain:@"FCPPSDK" code:NSURLErrorUnknown userInfo:@{NSLocalizedDescriptionKey:NonEmptyString(desc)}];
}


@implementation BaseViewController (Facepp)
#pragma mark - 验证身份证 正面图片
- (void)FCPPIDCardImage:(UIImage *)image ocrIDCardCompletion:(void (^)(FCIDCardModel *idCardModel, NSError *error))completion {
    //压缩
    NSData * imgData = UIImageJPEGRepresentation(image, 0.5);
    image = [UIImage imageWithData:imgData];
    
    FCPPOCR *ocr = [[FCPPOCR alloc] initWithImage:image];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ocr ocrIDCardCompletion:^(id info, NSError *error) {
        [hud hideAnimated:YES];
        if (info) {
            NSArray *array = info[@"cards"];
            if(!ArrBool(array)) {
                completion(nil, DYFaceNullDataError(@"是身份证图片吗？麻烦重新拍照"));
                return;
            }
            NSDictionary * cardDict = array[0];
            if(!DictBool(cardDict)) {
                completion(nil, DYFaceNullDataError(@"是身份证图片吗？麻烦重新拍照"));
                return;
            }
            
            NSString *imageId = info[@"image_id"];
            FCIDCardModel * idCardModel = [FCIDCardModel fcppIDCardModelWithCardsDict:cardDict];
            idCardModel.imageId = imageId;
            
            completion(idCardModel, nil);
            
        }else{
            NSData *errorData = error.userInfo[@"com.alamofire.serialization.response.error.data"];
            if (errorData) {
                NSString *errorStr = [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding];
                completion(nil, DYFaceNullDataError(errorStr));
            }else{
                completion(nil, DYFaceNullDataError(@"网络请求失败，稍后重试"));
            }
        }
        
    }];
}

#pragma mark - 人脸比对
- (void)FCPPCompareImage1:(UIImage *)image1 image2:(UIImage *)image2 completion:(void (^)(NSInteger index, NSError *error))completion {
    //压缩
    NSData * imgData1 = UIImageJPEGRepresentation(image1, 0.5);
    image1 = [UIImage imageWithData:imgData1];
    NSData * imgData2 = UIImageJPEGRepresentation(image2, 0.5);
    image2 = [UIImage imageWithData:imgData2];
    
    FCPPFace *face1 = [[FCPPFace alloc] initWithImage:image1];
    FCPPFace *face2 = [[FCPPFace alloc] initWithImage:image2];
    
    MBProgressHUD * hud =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [face1 compareFaceWithOther:face2 completion:^(id info, NSError *error) {
        [hud hiddenHud];
        if (info) {
            NSArray *face1 = info[@"faces1"];
            NSArray *face2 = info[@"faces2"];
            if (face1.count == 0) {
                completion(0,DYFaceNullDataError(@"图片1没有检测到人脸"));
                return ;
            }
            if(face2.count == 0){
                completion(1,DYFaceNullDataError(@"图片2没有检测到人脸"));
                return;
            }
            
            NSNumber *confidence = info[@"confidence"];
            NSDictionary *thresholds = info[@"thresholds"];
            NSMutableString *string = [NSMutableString string];
            [string appendFormat:@"置信度: %@\n",confidence];
            [string appendFormat:@"不同误识率下的置信度阈值:\n"];
            [thresholds enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
                [string appendFormat:@"%@ : %@\n",key,obj];
            }];
            
            [string appendString:@"\n比对结果:\n"];
            
            CGFloat confidenceValue = confidence.floatValue;
            CGFloat maxThreshold = [thresholds[@"1e-5"] floatValue];
//            CGFloat midThreshold = [thresholds[@"1e-4"] floatValue];
            CGFloat minThreshold = [thresholds[@"1e-3"] floatValue];
            
            //用户可根据自己的业务场景,选择不同的阈值.要求严格选择max,宽松则选择min
            if (confidenceValue >= maxThreshold) {
                //是否是同一个人: 可能性高
                completion(-1, nil);
            }else if (confidenceValue <= minThreshold){
                //是否是同一个人: 可能性低
                completion(-1,DYFaceNullDataError(@"和身份证照片不匹配"));
            }else{
                //是否是同一个人: 可能性一般"
                completion(-1,DYFaceNullDataError(@"和身份证照片不匹配"));
            }
        }else{
            NSData *errorData = error.userInfo[@"com.alamofire.serialization.response.error.data"];
            if (errorData) {
                NSString *errorStr = [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding];
                completion(-1, DYFaceNullDataError(errorStr));
            }else{
                completion(-1, DYFaceNullDataError(@"网络请求失败，稍后重试"));
            }
        }
    }];
}


- (void)FCPPBankCardImage:(UIImage *)image completion:(void (^)(FCBankCardModel * bankCardModel, NSError *error))completion {
    //压缩
    NSData * imgData = UIImageJPEGRepresentation(image, 0.5);
    image = [UIImage imageWithData:imgData];
    
    DYFCPPBankcard *ocr = [[DYFCPPBankcard alloc] initWithImage:image];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ocr ocrBankCardBetaCompletion:^(id info, NSError *error) {
        [hud hideAnimated:YES];
        if (info) {
            NSArray *array = info[@"bank_cards"];
            if(!ArrBool(array)) {
                completion(nil, DYFaceNullDataError(@"是银行卡图片吗？麻烦重新拍照"));
                return;
            }
            NSDictionary * cardDict = array[0];
            if(!DictBool(cardDict)) {
                completion(nil, DYFaceNullDataError(@"是银行卡图片吗？麻烦重新拍照"));
                return;
            }
            
            NSString * bankNameNumber = cardDict[@"number"];
            if(!StrBool(bankNameNumber)) {
                completion(nil, DYFaceNullDataError(@"是银行卡图片吗？麻烦重新拍照"));
                return;
            }
            
            NSString *imageId = info[@"image_id"];
            FCBankCardModel * bankCardModel = [FCBankCardModel new];
            bankCardModel.imageId = imageId;
            bankCardModel.bankCardNumber = bankNameNumber;
            
            completion(bankCardModel, nil);
            
        }else{
            NSData *errorData = error.userInfo[@"com.alamofire.serialization.response.error.data"];
            if (errorData) {
                NSString *errorStr = [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding];
                completion(nil, DYFaceNullDataError(errorStr));
            }else{
                completion(nil, DYFaceNullDataError(@"网络请求失败，稍后重试"));
            }
        }
        
    }];
}
@end
