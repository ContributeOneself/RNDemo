//
//  DYFCPPBankcard.h
//  DragonYin
//
//  Created by srxboys on 2018/5/5.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "FCPPApi.h"

@interface DYFCPPBankcard : FCPPApi
/**
 银行卡识别 https://console.faceplusplus.com.cn/documents/10069553
 
 @param completion 结果回调
 */
- (void)ocrBankCardBetaCompletion:(void(^)(id info,NSError *error))completion;

@end
