//
//  BaseViewController+Facepp.h
//  DragonYin
//
//  Created by srxboys on 2018/5/4.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//
// Face++ (FCPPSDK)

#import "BaseViewController.h"
#import "DYFCPPModel.h"

@interface BaseViewController (Facepp)
/// 身份证验证 正反面图(info 原始数据， idCardModel处理过的数据模型， error是否有错)
- (void)FCPPIDCardImage:(UIImage *)image ocrIDCardCompletion:(void (^)(FCIDCardModel *idCardModel, NSError *error))completion;

/// 人脸比对
- (void)FCPPCompareImage1:(UIImage *)image1 image2:(UIImage *)image2 completion:(void (^)(NSInteger index, NSError *error))completion;

/// 银行卡识别
- (void)FCPPBankCardImage:(UIImage *)image completion:(void (^)(FCBankCardModel * bankCardModel, NSError *error))completion;
@end
