//
//  BaseViewController+UIPickerView.h
//  DragonYin
//
//  Created by srxboys on 2018/5/4.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController.h"

/// 是否选择的日期， 日期随时变化
typedef void(^PickerBlock)(BOOL isSelectd, NSDate * date);

@interface BaseViewController (UIPickerView)

//下面是 针对固定UI布局的样式，如果想要不同的，请参考第二个进行自定义view布局。

/// 1 日期
- (void)showDatePicker:(NSString *)dateString pickerBlock:(PickerBlock)pickerBlock;

/// 2 中间字
- (void)showLabelPicker:(NSArray<NSString *>*)array pickerBlock:(void(^)(NSString *chars))pickerBlock;

@end
