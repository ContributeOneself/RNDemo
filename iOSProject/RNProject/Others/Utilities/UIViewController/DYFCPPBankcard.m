//
//  DYFCPPBankcard.m
//  DragonYin
//
//  Created by srxboys on 2018/5/5.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYFCPPBankcard.h"

@implementation DYFCPPBankcard
- (instancetype)initWithImage:(UIImage *)image{
    if (self = [super initWithImage:image]) {
        self.image = [image fixImageWithMaxSize:CGSizeMake(4096, 4096)];
    }
    return self;
}


- (void)ocrBankCardBetaCompletion:(void(^)(id info,NSError *error))completion{
    // 接口写死了
    NSString *url = [NSString stringWithFormat:@"https://api-cn.faceplusplus.com/cardpp/beta/ocrbankcard"];
    [self POST:url completion:completion];
}

- (void)POST:(NSString *)url completion:(void(^)(id info,NSError *error))completion{
    if (!isChina) {
        NSLog(@"Error-->This method is only for China");
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    
    if (self.image) {
        NSString *baseStr = self.image.base64String;
        [param setObject:baseStr forKey:@"image_base64"];
        //        [param setObject:self.image.imageData forKey:@"image_file"];
    }else if (self.imageUrl){
        if ([self.imageUrl hasPrefix:@"http"]) {
            [param setObject:self.imageUrl forKey:@"image_url"];
        }else{
            NSLog(@"please input a vaild url");
        }
    }
    
    [FCPPApi POST:url param:param completion:completion];
}
@end
