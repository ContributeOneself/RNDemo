//
//  BaseViewController+Navigation.m
//  DragonYin
//
//  Created by srxboys on 2018/5/4.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController+Navigation.h"

@implementation BaseViewController (Navigation)
/*
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    /// 如果旋转屏幕 主要调整 状态栏
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    [[UIApplication sharedApplication] setStatusBarOrientation:orientation];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    //如果旋转屏幕
    [self setImgViewBGType:self.imgViewBGType];
    
    //上面的和 baseViewController设置一样
    
    //布局Navigation  title+image
    [self changeNavigationTitleBounds];
}


#pragma mark - change navigation frame
- (void)changeNavigationTitleBounds {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeNavigationFrame) object:nil];
    [self performSelector:@selector(changeNavigationFrame) withObject:nil afterDelay:0.1];
}

- (void)changeNavigationFrame {
    /// 布局Navigation  title+image
    
    CGFloat leftItemWidth = MAX(self.navigationItem.leftBarButtonItem.width, [self getBarButtonItemWidth:self.navigationItem.leftBarButtonItem]);
    CGFloat rightItemWidth = MAX(self.navigationItem.rightBarButtonItem.width, [self getBarButtonItemWidth:self.navigationItem.rightBarButtonItem]);
    
    
    CGFloat navigationWidth = [UIScreen mainScreen].bounds.size.width;
    if(rightItemWidth == 0) {
        rightItemWidth = leftItemWidth;
    }
    
    CGFloat titleImgViewLeft = 5;
    CGFloat titleViewWidth = navigationWidth - leftItemWidth - rightItemWidth - 40 - titleImgViewLeft*2;
    
    if(self.navigationItem.titleView.bounds.size.width == titleViewWidth) {
        // 已经设置过 则无需费事
        return;
    }
    
    UILabel * titleLabel = nil;
    UIImageView * titleImg = nil;
    CGSize imageSize = CGSizeZero;
    for (UIView *view in self.navigationItem.titleView.subviews) {
        if([view isKindOfClass:[UILabel class]]) {
            titleLabel = (UILabel *)view;
        }
        else if([view isKindOfClass:[UIImageView class]]) {
            titleImg = (UIImageView *)view;
            imageSize = titleImg.bounds.size;
        }
    }
    
    CGFloat left = 0;
    CGFloat top = 0;
    CGFloat height = 44;
    CGFloat titleWidth = 0;
    CGFloat imageLW = imageSize.width?imageSize.width+titleImgViewLeft:0;
    self.navigationItem.titleView.frame = CGRectMake(leftItemWidth+20+titleImgViewLeft, STATUS_BAR_HEIGHT, titleViewWidth, height);
    
    if(titleLabel) {
        CGRect baseFrame = CGRectMake(0, 0, titleViewWidth-imageLW, height);
        titleWidth  = ceilf([titleLabel textRectForBounds:baseFrame limitedToNumberOfLines:1].size.width);
    }
    
    if(titleWidth > 0) {
        CGFloat titleLeft = (titleViewWidth - titleWidth)/2 - imageLW + left;
        titleLabel.frame = CGRectMake(titleLeft, top, titleWidth, height);
        
        if(imageLW > 0){
            titleImg.frame = CGRectMake(titleLeft+titleWidth+ titleImgViewLeft, (height - imageSize.height)/2, imageSize.width, imageSize.height);
        }
    }
    else if(titleImg){
        if(imageSize.width > titleViewWidth) {
            imageSize.width = titleViewWidth;
        }
        titleImg.frame = CGRectMake(left + titleImgViewLeft + (titleViewWidth - imageSize.width)/2, (height - imageSize.height)/2, imageSize.width, imageSize.height);
    }
}

- (CGFloat)getBarButtonItemWidth:(UIBarButtonItem *)item {
    return item.customView.bounds.size.width;
}



#pragma mark - set Navigation
- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage {
    [self setTitle:title titleImage:titleImage titleImageSize:CGSizeMake(28, 28)];
}


- (void)setTitle:(NSString *)title titleImage:(UIImage *)titleImage titleImageSize:(CGSize)titleImageSize {
    
    UILabel * titleLabel = [UILabel new];
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = title;
    
    UIImageView * titleImgView = [UIImageView new];
    titleImgView.frame = CGRectMake(0, 0, titleImageSize.width, titleImageSize.height);
    titleImgView.image = titleImage;
    
    UIView * titleView = [UIView new];
    [titleView addSubview:titleLabel];
    [titleView addSubview:titleImgView];
    self.navigationItem.titleView = titleView;
    
    [self changeNavigationTitleBounds];
}
 */
@end
