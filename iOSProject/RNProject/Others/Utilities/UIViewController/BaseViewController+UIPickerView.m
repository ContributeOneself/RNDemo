//
//  BaseViewController+UIPickerView.m
//  DragonYin
//
//  Created by srxboys on 2018/5/4.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "BaseViewController+UIPickerView.h"
#import "RXPickerView.h"
#import "RXDatePicker.h"

@implementation BaseViewController (UIPickerView)
- (void)showDatePicker:(NSString *)dateString pickerBlock:(PickerBlock)pickerBlock {
    
    //初始化
    RXDatePicker * picker = [RXDatePicker new];
    [self.view addSubview:picker];
    
    // 默认日期(进入时的日期)
    if(StrBool(dateString)) {
        [picker setNomalDateString:dateString];
    }
    else {
        NSDate * date = [NSDate date];
        picker.nomalDate = date;
    }
    
    // 回调
    picker.pickerDateBlock = ^(BOOL isShow, NSDate *date) {
        if(pickerBlock) {
            pickerBlock(isShow, date);
        }
    };
    
    //显示 单独调用
    [picker show];
}



- (void)showLabelPicker:(NSArray<NSString *> *)array pickerBlock:(void (^)(NSString *chars))pickerBlock {
    if(!ArrBool(array)) return;
    
    // 初始化
    RXPickerView * picker = PXPICKER_VIEW_ALLOC;
    
    // 行高
    picker.itemHeight = 50;
    
    // 数据(可以为数据模型等)
    picker.items = array;
    
    // 准备 picker获取 的view
    [picker setItemViewBlock:^UIView *(UIView *reusingView, NSString * item, NSArray *selectedComponents) {
        UILabel * label = [[UILabel alloc] init];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = COLOR_333;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = NonEmptyString(item);
        return label;
    }];
    
    // 点击完成按钮的回调block
    [picker showWithDoneBlock:^(NSInteger index, NSString * item, NSArray * selectedComponents) {
        if(pickerBlock) {
            pickerBlock(NonEmptyString(item));
        }
    }];
    
}

@end
