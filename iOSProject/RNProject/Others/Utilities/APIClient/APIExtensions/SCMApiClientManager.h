//
//  SCMApiClient.h
//  SCM
//
//  Created by srx on 2017/9/14.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"



//请求网络类型
typedef NS_ENUM(NSInteger, MethodType) {
    POST = 0,
    GET = 1,
//    DELETE = 2,
//    PUT = 3,
//    PATCH = 4,
//    OPTIONS = 5,
//    TRACE = 6,
//    HEAD = 7,
//    CONNECT = 8,
};

@interface SCMApiClientManager : NSObject
+ (NSURLSessionDataTask *)requestHttpData:(MethodType)methodType urlString:(NSString *)urlString parameters:(NSDictionary *)paramsDict progress:(void(^)(NSProgress * progress))progressBlock  successBlock:(void (^)(NSURLSessionDataTask * task, id  responseObject))successBlock failureBlock:(void (^)(NSURLSessionDataTask *  task, NSError * error))failureBlock;

+ (NSURLSessionDataTask *)Upload:(NSString *)urlString parameters:(NSDictionary *)paramsDict constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block progress:(void(^)(NSProgress * progress))progressBlock successBlock:(void (^)(NSURLSessionDataTask * task, id  responseObject))successBlock failureBlock:(void (^)(NSURLSessionDataTask *  task, NSError * error))failureBlock;

+ (NSURLSessionDownloadTask *)downloadURL:(NSString *)urlString destinationPath:(NSString *)destinationPath progress:(void(^)(NSProgress * progress))progressBlock successBlock:(void (^)(NSURLSessionDownloadTask * task,NSURL * filepath))successBlock failureBlock:(void (^)(NSURLSessionDownloadTask *  task, NSError *error))failureBlock;
@end




