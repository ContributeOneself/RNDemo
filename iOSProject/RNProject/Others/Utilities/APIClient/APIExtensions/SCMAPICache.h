//
//  SCMAPICache.h
//  SCM
//
//  Created by srx on 2017/10/16.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//
//主要解决，页面离开 页面相应的请求也就放弃了(针对AFN)

#import <UIKit/UIKit.h>

@interface SCMAPICache : NSObject

//添加缓存(对象绑定接口，同一个接口只保留一次。不同对象的同一个接口不管)
- (void)addCacheDataWithMethod:(NSString *)method URLDataTask:(NSURLSessionDataTask *)URLTask;
- (void)addCacheDownWithMethod:(NSString *)method URLDownTask:(NSURLSessionDownloadTask*)URLTask;

//移除缓存 (请求结束 对象所绑定接口，进行单步移除)
- (void)removeDataCacheWithMethod:(NSString *)method;
- (void)removeDownCacheWithMethod:(NSString *)method;

//移除该对象的所有缓存
- (void)removeAllCacheWithObject;

@end
