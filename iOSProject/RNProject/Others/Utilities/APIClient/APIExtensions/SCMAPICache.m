//
//  SCMAPICache.m
//  SCM
//
//  Created by srx on 2017/10/16.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#define APICACHE_ENABLE 1

#define ALog(format, ...) NSLog((@"%s [L%d] " format), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#ifdef DEBUG
    #define CacheLog(format, ...) ALog(format, ##__VA_ARGS__)
#else
    #define CacheLog(...)
#endif



#import "SCMAPICache.h"

static SCMAPICache * __apiCache = nil;

@interface SCMAPICache()<NSCacheDelegate>
@property (nonatomic, strong, nullable) NSCache * URLDataCache;
@property (nonatomic, strong, nullable) NSCache * URLDownCache;

//数据操作安全
@property (nonatomic, strong, nullable) dispatch_queue_t ioQueue;
@property (nonatomic, assign) BOOL isDealloc;
@end

@implementation SCMAPICache

- (instancetype)init
{
    self = [super init];
    if (self) {
        //初始的操作
        _ioQueue = dispatch_queue_create("com.SCMAPICACHE.srxboys", DISPATCH_QUEUE_SERIAL);
        _URLDataCache = [[NSCache alloc] init];
        _URLDataCache.name = @"请求数据缓存";
        _URLDataCache.delegate = self;
        _URLDownCache = [[NSCache alloc] init];
        _URLDownCache.name = @"下载数据缓存";
        _URLDownCache.delegate = self;
    }
    return self;
}



- (void)addCacheDataWithMethod:(NSString *)method URLDataTask:(NSURLSessionDataTask *)URLTask {
    __weak typeof(self)weakSelf = self;
    dispatch_async(weakSelf.ioQueue, ^{
        weakSelf.isDealloc = NO;
        [weakSelf.URLDataCache setObject:URLTask forKey:method];
    });
}

- (void)addCacheDownWithMethod:(NSString *)method URLDownTask:(NSURLSessionDownloadTask*)URLTask {
    __weak typeof(self)weakSelf = self;
    dispatch_async(weakSelf.ioQueue, ^{
        weakSelf.isDealloc = NO;
        [weakSelf.URLDownCache setObject:URLTask forKey:method];
    });
}

- (void)removeDataCacheWithMethod:(NSString *)method {
    __weak typeof(self)weakSelf = self;
    dispatch_async(weakSelf.ioQueue, ^{
        weakSelf.isDealloc = NO;
        [weakSelf.URLDataCache removeObjectForKey:method];
    });
}


- (void)removeDownCacheWithMethod:(NSString *)method; {
    __weak typeof(self)weakSelf = self;
    dispatch_async(_ioQueue, ^{
        weakSelf.isDealloc = NO;
        [weakSelf.URLDownCache removeObjectForKey:method];
    });
}

- (void)removeAllCacheWithObject {
    __weak typeof(self)weakSelf = self;
    dispatch_async(_ioQueue, ^{
        weakSelf.isDealloc = YES;
        [weakSelf.URLDataCache removeAllObjects];
        [weakSelf.URLDownCache removeAllObjects];
    });
}

- (void)cache:(NSCache *)cache willEvictObject:(id)obj {
    __weak typeof(self)weakSelf = self;
    dispatch_async(self.ioQueue, ^{
        if(weakSelf.isDealloc) {
            CacheLog(@"objc = %@", obj);
        }
        
        if([obj isKindOfClass:[NSURLSessionDataTask class]]) {
            NSURLSessionDataTask * dataTask = (NSURLSessionDataTask *)obj;
            dispatch_async(dispatch_get_main_queue(), ^{
                //取消请求
                [dataTask cancel];
            });
        }
        else if([obj isKindOfClass:[NSURLSessionDownloadTask class]]) {
            NSURLSessionDownloadTask * downTask = (NSURLSessionDownloadTask *)obj;
            dispatch_async(dispatch_get_main_queue(), ^{
                //取消下载
                [downTask cancel];
            });
        }
        
    });
}

- (void)dealloc {
    _isDealloc = YES;
    [_URLDataCache removeAllObjects];
    [_URLDownCache removeAllObjects];
}

@end
