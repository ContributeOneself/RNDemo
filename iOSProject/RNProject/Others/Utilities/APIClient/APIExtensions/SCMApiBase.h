//
//  SCMApiBase.h
//  SCM
//
//  Created by srx on 2017/10/17.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//
//暂时没有用到

#ifndef SCMApiBase_h
#define SCMApiBase_h

#import <UIKit/UIKit.h>
//错误码
typedef NS_ENUM(NSInteger, StatusCode) {
    StatusCode200 = 200, //处理成功标识
    StatusCode500 = 500, //参数错误标识
    StatusCode501 = 501, //服务器处理异常标识
    StatusCode502 = 502, //时间错误标识
    StatusCode503 = 503, //处理失败标识
    StatusCodeNoKnow
};

#endif /* SCMApiBase_h */

