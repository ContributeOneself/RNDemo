//
//  SCMApiClient.m
//  SCM
//
//  Created by srx on 2017/9/14.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMApiClientManager.h"

#import "NSBundle+Extensions.h"
#import "Constant.h"
#import "RXEncrypt.h"


#define HTTPtimeoutInterval 30


@implementation SCMApiClientManager

#pragma mark ---当前时间--
//+ (NSString *)getCurrentTime:(NSDate *)date {
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT+0800"];
//    [dateFormatter setTimeZone:gmt];
//    NSString *timeStamp = [dateFormatter stringFromDate:date];
//    return timeStamp;
//}

/*
+ (NSDictionary *)constructParameters: (NSDictionary *)params
{
    NSMutableDictionary* newParams = [[NSMutableDictionary alloc] init];

    NSDate * nowDate = [NSDate date];
    //时间标识(手机本地时间)  timestamp
    double timestamp = [nowDate timeIntervalSince1970];
    NSString * timestampStr = [NSString stringWithFormat:@"%.0f", timestamp];
    [newParams setObject:timestampStr forKey:@"timestamp"];
    
    //请求唯一标识  requestId
    NSString * uuid = USER_D_GET(Sandbox_APP_UDID);
    [newParams setObject:NonEmptyString(uuid) forKey:@"requestId"];
    
    //手机操作系统 phoneSystem iOS
    [newParams setObject:@"iOS" forKey:@"phoneSystem"];
    
    //手机型号 phoneType  iphone 4,9
    NSString * device = [NSBundle bundlePhoneModel];
    [newParams setObject:device forKey:@"phoneType"];

    //accessToken
    NSString * accessToken = GET_ACCESSTOKEN;
    [newParams setObject:NonEmptyString(accessToken) forKey:@"accessToken"];
    if(StrBool(accessToken)) {
        //MD5安全加密
         //accessToken+timestamp+uuid
        NSString * paraStr = @"1211a3b917bc4f538a75985e050d52a8";
        paraStr = [paraStr stringByAppendingString:timestampStr];
        paraStr = [paraStr stringByAppendingString:uuid];
        NSString * sign = RXEncrypt->MD5_32Bit_Digest(paraStr);
        memset(&paraStr, 0, sizeof(paraStr));
        RXEncrypt_destory;
        //签名加密
        [newParams setObject:sign forKey:@"sign"];
    }
    
    //手机版本号 version
    [newParams setObject:[NSBundle bundleVersion] forKey:@"version"];
    
//    NSString * umeng_channel = AppChannel;
//    [newParams setObject:umeng_channel forKey:@"umeng_channel"];
    return newParams;
}

/// 通用字段 添加 请求头里面
+ (void)setManager:(AFHTTPSessionManager*)manager withHeades:(NSDictionary *)dict{
    AFHTTPSessionManager * httpManger = (AFHTTPSessionManager *)manager;
    for(NSString * key in dict) {
        [httpManger.requestSerializer setValue:dict[key] forHTTPHeaderField:key];
    }
}
*/

+ (NSURLSessionDataTask *)requestHttpData:(MethodType)methodType urlString:(NSString *)urlString parameters:(NSDictionary *)paramsDict progress:(void(^)(NSProgress * progress)) progressBlock  successBlock:(void (^)(NSURLSessionDataTask * task, id  responseObject))successBlock failureBlock:(void (^)(NSURLSessionDataTask *  task, NSError * error))failureBlock {
    
    NSParameterAssert(urlString);
    NSParameterAssert(paramsDict);
    
     NSMutableString * str = [NSMutableString stringWithString:urlString];
    RXLog(@"请求网址=%@\n%@\n%@\n", str, methodType == POST?@"POST":@"GET",paramsDict);
 
    AFHTTPSessionManager * manager = [self shareHttpClient];
    //通用字段 添加 请求头里面
//     [self setManager:manager withHeades:[self constructParameters:nil]];
    
    NSURLSessionDataTask * task = nil;
    if(methodType == POST) {
        task = [manager POST:urlString parameters:paramsDict progress:^(NSProgress * _Nonnull uploadProgress) {
            /**加载进度*/
            progressBlock(uploadProgress);
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            /**请求完成并【成功】的操作*/
            successBlock(task, responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            /**请求完成并【失败】的操作*/
            failureBlock(task, error);
            //错误码
    //        NS_ERROR_ENUM(NSURLErrorDomain)
            RXLogError(@"error=\n%@", error.description);
        }];
    }
    else {
        task = [manager GET:urlString parameters:paramsDict progress:^(NSProgress * _Nonnull downloadProgress) {
            /**加载进度*/
            progressBlock(downloadProgress);
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            /**请求完成并【成功】的操作*/
            successBlock(task, responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            /**请求完成并【失败】的操作*/
            failureBlock(task, error);
//                    NS_ERROR_ENUM(NSURLErrorDomain)
            RXLogError(@"error=\n%@", error.description);
        }];
    }
    return task;
}


+ (NSURLSessionDataTask *)Upload:(NSString *)urlString parameters:(NSDictionary *)paramsDict constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block progress:(void(^)(NSProgress * progress))progressBlock successBlock:(void (^)(NSURLSessionDataTask * task, id  responseObject))successBlock failureBlock:(void (^)(NSURLSessionDataTask *  task, NSError * error))failureBlock  {
    
    NSParameterAssert(urlString);
    NSParameterAssert(paramsDict);
    
    RXLog(@"上传网址=%@\n%@\n", urlString, paramsDict);
    
    AFHTTPSessionManager * manager = [self shareHttpClient];
    //通用字段 添加 请求头里面
//    [self setManager:manager withHeades:[self constructParameters:nil]];
    
    NSURLSessionDataTask * task = [manager POST:urlString parameters:paramsDict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSParameterAssert(formData);
        block(formData);
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        /**下载进度*/
        progressBlock(uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        /**请求完成并【成功】的操作*/
        successBlock(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        /**请求完成并【失败】的操作*/
        failureBlock(task, error);
        //错误码
        //        NS_ERROR_ENUM(NSURLErrorDomain)
        RXLogError(@"error=\n%@", error.description);
    }];
    return task;
}

+ (NSURLSessionDownloadTask *)downloadURL:(NSString *)urlString destinationPath:(NSString *)destinationPath progress:(void(^)(NSProgress * progress))progressBlock successBlock:(void (^)(NSURLSessionDownloadTask * task,NSURL * filepath))successBlock failureBlock:(void (^)(NSURLSessionDownloadTask *  task, NSError *error))failureBlock {
    
    NSParameterAssert(urlString);
    
    RXLog(@"下载网址=%@\n", urlString);
    
    //远程地址
    NSURL *URL = [NSURL URLWithString:urlString];
    //请求
    NSURLRequest * request = [NSURLRequest requestWithURL:URL];
//    NSURLRequest * request = [self downloadDiyRequestWith:URL];
//    [request h];
    AFURLSessionManager * downloadManager = [self shareURLClient];
    //通用字段 添加 请求头里面
    
   NSURLSessionDownloadTask* task  = nil;
    
   task = [downloadManager downloadTaskWithRequest:request  progress:^(NSProgress * _Nonnull downloadProgress) {
         /**下载进度*/
       progressBlock(downloadProgress);
   } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
       //写入 文件路径 和 文件名字
       NSString * targetStr = [destinationPath stringByAppendingPathComponent:response.suggestedFilename];
       //手动 utf-8
//       NSString *transString = [NSString stringWithString:[targetStr stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
       
       targetStr = [targetStr stringByRemovingPercentEncoding];
       
       /**block的返回值, 要求返回一个URL就是文件的位置的路径*/
       NSURL * targetURL = [NSURL fileURLWithPath:targetStr];
       return targetURL;
   } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
       /**下载完成操作*/
       if(error) {
           RXLogError(@"error=\n%@", error.description);
           if(failureBlock) {
               failureBlock(task, error);
           }
       }
       else {
           if(successBlock) {
               successBlock(task, filePath);
           }
       }
   }];

    [task resume];
    return task;
}

//用http、https请求
+ (AFHTTPSessionManager *)shareHttpClient {
    //------ http请求 --------
    static AFHTTPSessionManager * managerHttp = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //如果manger为第一次创建会需要这个 配置
        NSURLSessionConfiguration * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        //超时设置
        configuration.timeoutIntervalForRequest = HTTPtimeoutInterval;
        //实现 共享session,较少http 3次握手等操作。
        managerHttp = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
        managerHttp.requestSerializer.timeoutInterval = HTTPtimeoutInterval;
        managerHttp.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", nil];
    
         /// 用于 看看 服务器 哪里 返回不是标准的Json数据 //建议不要打开
        managerHttp.requestSerializer = [AFJSONRequestSerializer serializer];
        managerHttp.responseSerializer = [AFJSONResponseSerializer serializer];
        
    });
    return managerHttp;
}


//download的手柄
+ (AFURLSessionManager *)shareURLClient {
    //实现 共享session,较少http 3次握手等操作。
    static AFURLSessionManager * _manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //默认配置 60s的请求时长等
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        //AFN3.0+基于封住URLSession的句柄
        _manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    });
    return _manager;
}
/*
+ (NSURLRequest *)downloadDiyRequestWith:(NSURL *)url {
    NSDictionary * dict = [self constructParameters:nil];
    NSMutableURLRequest * requestM = [NSMutableURLRequest new];
    for(NSString * key in dict) {
        [requestM addValue:dict[key] forHTTPHeaderField:key];
    }
    
    [requestM addValue:@"text/html;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    [requestM setHTTPMethod:@"POST"];
    
    //设置缓存策略
    [requestM setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    requestM.URL = url;
    
    return requestM;
}
*/
@end
