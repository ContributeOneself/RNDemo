//
//  SCMDownloadFileCache.h
//  SCM
//
//  Created by srx on 2017/11/17.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//



#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SCMDownloadFileCache : NSObject
+ (SCMDownloadFileCache *)shareSCMDownloadFileCache;
+ (NSString *)getFilepathForKey;
- (void)storeFileItems:(NSArray<NSString *> *)fileItems;
@end


/*
 针对SCM 下载zip/pdf/work/excel...   zip/rar/7zZip...
 */
