//
//  SCMDownloadFileCache.m
//  SCM
//
//  Created by srx on 2017/11/17.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMDownloadFileCache.h"

#import <CommonCrypto/CommonDigest.h>

static const NSInteger kDefaultCacheMaxCacheAge = 60 * 60 * 24 * 7; // 1 week

@interface SCMDownloadFileCache()
@property (nonatomic, nullable, strong) dispatch_queue_t ioQueue;
@end


@implementation SCMDownloadFileCache

+ (SCMDownloadFileCache *)shareSCMDownloadFileCache {
    static SCMDownloadFileCache * _downloadHandle = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _downloadHandle = [SCMDownloadFileCache new];
    });
    return _downloadHandle;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        //队列 先进先出
        _ioQueue = dispatch_queue_create("SCMDownloadFileCache_srxboys", DISPATCH_QUEUE_SERIAL);
        
        //内存警告
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAllObjects) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}


/// 文件名(md5后，拼接 扩展名)
- (nullable NSString *)cachedFileNameForKey:(nullable NSString *)key {
    const char *str = key.UTF8String;
    if (str == NULL) {
        str = "";
    }
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    NSString *filename = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%@",
                          r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10],
                          r[11], r[12], r[13], r[14], r[15], [key.pathExtension isEqualToString:@""] ? @"" : [NSString stringWithFormat:@".%@", key.pathExtension]];
    
    return filename;
}

@end
