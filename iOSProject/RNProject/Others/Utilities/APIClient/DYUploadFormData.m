//
//  DYUploadFormData.m
//  DragonYin
//
//  Created by srxboys on 2018/5/7.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "DYUploadFormData.h"
#import "RXEncrypt.h"

UIKIT_STATIC_INLINE NSString * appendMimeType(NSString * fileType) {
    if(!fileType.length)  return @"none";
    return [NSString stringWithFormat:@"image/%@", fileType];
}


@interface DYUploadFormData()
@property (nonatomic, copy) NSString *fileType;
@end

@implementation DYUploadFormData
- (void)setData:(NSData *)data {
    _data = data;
    
#pragma mark -获取图片格式
    /** 图片 */
    uint8_t c;
    [data getBytes:&c length:1];
    switch (c) {
        case 0xFF: //FFD8FF(.JPEG .JPE  .JPG)
            self.fileType = @"jpeg";
            break;
        case 0x89: //89504E47
            self.fileType = @"png";
            break;
        case 0x47: //47494638(git、"GIF 87A"、"GIF 89A")
            self.fileType = @"gif";
            break;
        case 0x49: //49492A00
        case 0x4D: //424D
            self.fileType = @"tiff";
        case 0x52:
        {
            // R as RIFF for WEBP
            if (data.length < 12) {
                self.fileType = @"";;
                break;
            }
            
            NSString *testString = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(0, 12)] encoding:NSASCIIStringEncoding];
            if ([testString hasPrefix:@"RIFF"] && [testString hasSuffix:@"WEBP"]) {
                self.fileType = @"webP";
            }
            break;
        }
    }
    
    _mimeType = appendMimeType(self.fileType);
}

- (NSString *)fileType {
    if(!_fileType) {
        _fileType = @"";
    }
    return _fileType;
}


- (NSString *)name {
    if(!_name) {
        _name = @"";
    }
    return _name;
}

- (NSString *)fileName {
    if(!_fileName) {
//        _fileName = @"";
        _fileName = [NSString stringWithFormat:@"a.%@", self.fileType];
    }
    return _fileName;
}

@end
