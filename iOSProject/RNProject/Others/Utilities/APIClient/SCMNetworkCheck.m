//
//  SCMNetworkCheck.m
//  SCM
//
//  Created by srx on 2017/12/8.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMNetworkCheck.h"
#import "AFNetworking.h"
#import "Constant.h"

@implementation SCMNetworkCheck
+ (SCMNetworkCheck *)shareNetworkCheck {
    static SCMNetworkCheck * _networkCheck = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _networkCheck = [[SCMNetworkCheck alloc] initConfig];
    });
    return _networkCheck;
}

- (instancetype)initConfig
{
    self = [super init];
    if (self) {
        _statusString = CONST_NET_NONE;
        [self getNetworkStatus];
    }
    return self;
}


- (void)getNetworkStatus {
    // 1.获得网络监控的管理者
    AFNetworkReachabilityManager *mgr = [AFNetworkReachabilityManager sharedManager];
    
    weak(weakSelf);
    
    // 2.设置网络状态改变后的处理
    [mgr setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 当网络状态改变了, 就会调用这个block
        switch (status)
        {
            case AFNetworkReachabilityStatusUnknown:
                //"未知网络"
                [weakSelf setStatusString:CONST_NET_UNKOWN];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                // 没有网络(断网)
                [weakSelf setStatusString:CONST_NET_NONE];
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN: // 手机自带网络
                //手机自带网络"
                [weakSelf setStatusString:CONST_NET_PHONE];
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi: // WIFI
                //"WIFI"
                [weakSelf setStatusString:CONST_NET_WIFI];
                break;
            default:
                // 没有网络(断网)
                [weakSelf setStatusString:CONST_NET_NONE];
                break;
        }
    }];
    [mgr startMonitoring];
}

- (void)setStatusString:(NSString *)statusString {
    _statusString = statusString;
    @try {
        NOTI_POST(NOTINAME_NETWORK);
    } @catch (NSException *exception) {
        RXLog(@"是不是控制器没有remove noti");
    } @finally {
        //断言 只在debug模式下起作用
        NSAssert(1, @"需要看看bug");
    }
}









/** 不是实时更新网络 只是判断当前状态 */
+ (BOOL) isAppleNetworkEnable {
    
    BOOL bEnable = NO;
    
    NSString * url = @"www.apple.com";//也可以是公司的网址
    
    SCNetworkReachabilityRef ref = SCNetworkReachabilityCreateWithName(NULL, [url UTF8String]);
    SCNetworkReachabilityFlags flags;

    bEnable = SCNetworkReachabilityGetFlags(ref, &flags);
    
    CFRelease(ref);
    
    if(bEnable) {
        // kSCNetworkReachabilityFlagsReachable：能够连接网络
        // kSCNetworkReachabilityFlagsConnectionRequired：能够连接网络，但是首先得建立连接过程
        // kSCNetworkReachabilityFlagsIsWWAN：判断是否通过蜂窝网覆盖的连接，比如EDGE，GPRS或者目前的3G.主要是区别通过WiFi的连接。
        
        BOOL flagsReachable = ((flags & kSCNetworkFlagsReachable) != 0);
        
        BOOL connectionRequired = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
        
        BOOL nonWiFi = flags & kSCNetworkReachabilityFlagsTransientConnection;
        
        bEnable = ((flagsReachable && !connectionRequired) || nonWiFi) ? YES : NO;
        
        //        BOOL nonWiFi2 = kSCNetworkReachabilityFlagsIsWWAN;
        //        RXLog(@"nonWiFi2 = %d", nonWiFi2);
    }
    
    return bEnable;
}

@end
