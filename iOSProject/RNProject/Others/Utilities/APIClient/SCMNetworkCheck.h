//
//  SCMNetworkCheck.h
//  SCM
//
//  Created by srx on 2017/12/8.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SCMNetworkCheck : NSObject
/**  标识，不可以被外部调用 */
- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

+ (SCMNetworkCheck *)shareNetworkCheck;
/** 实时网络状态 */
@property (nonatomic, copy, readonly) NSString * statusString;

/** 不是实时更新网络 只是判断当前状态 */
+ (BOOL) isAppleNetworkEnable;
@end
