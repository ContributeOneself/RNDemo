//
//  UIResponder+APIClient.h
//  SCM
//
//  Created by srx on 2017/10/18.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//
/*
 【使用】 时导入一个文件  SCMApiClient_h
 
 */
#import <UIKit/UIKit.h>
#import "AFURLRequestSerialization.h"

//接口json 头，处理
@interface Response : NSObject
@property (nonatomic, copy) NSString * message; //服务器 错误描述
@property (nonatomic, assign) BOOL status; //状态(1=success 0=fail)
@property (nonatomic, strong) id content; //结果
+ (Response*)responseWithDict:(NSDictionary*)dict;
@end


typedef NS_ENUM(NSInteger,Request) {
    RequestPost = 0,
    RequestGet
};

@class SCMAPICache;

@interface UIResponder (APIClient)

/**
 *
 *  @brief 请求接口 (没有强调 不可为nil, 参数可以为nil)
 *
 *  @param RequestType   请求形式(默认Post)
 *  @param method        接口   **不可为nil**
 *  @param className     请求结果自动初始化数据模型(nil 不自动处理)
 *  @param paramsDict    请求参数(字典)
 *  @param successBlock  请成功，以block形式返回
 *  @param failureBlock  请求失败，以block形式返回
 */
- (void)RequestType:(Request)RequestType method:(NSString *)method className:(Class)className paramsDict:(NSDictionary *)paramsDict successBlock:(void (^)(Response * responseObject))successBlock failureBlock:(void (^)(NSError * error))failureBlock;

- (void)GetMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict className:(Class)className successBlock:(void (^)(Response * responseObject))successBlock failureBlock:(void (^)(NSError * error))failureBlock;

- (void)POSTMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict className:(Class)className successBlock:(void (^)(Response * responseObject))successBlock failureBlock:(void (^)(NSError * error))failureBlock;

/**
 *
 *  @brief 上传文件并请求接口(需要调试)
 *
 *  @param method        接口
 *  @param block         上传文件/多文件到服务器的文件设置(自定义)
 *  @param successBlock  请成功，以block形式返回
 *  @param failureBlock  请求失败，以block形式返回
 */
- (void)UploadMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict
constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
        successBlock:(void (^)(Response * responseObject))successBlock
        failureBlock:(void (^)(NSError * error))failureBlock;

- (void)UploadDiyMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict
constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block progress:(void(^)(NSProgress * progress))progressBlock
        successBlock:(void (^)(Response * responseObject))successBlock
        failureBlock:(void (^)(NSError * error))failureBlock;


/**
 *
 *  @brief 普通下载一个文件(需要调试)
 *
 *  @param urlString         地址
 *  @param destinationPath   下载到哪里
 *  @param successBlock      请成功，以block形式返回 finishPath==最后保留路径
 *  @param failureBlock      请求失败，以block形式返回 finishPath==最后保留路径
 */
- (void)downloadRequestWithURL:(NSString *)urlString destinationPath:(NSString *)destinationPath
                  successBlock:(void (^)(NSString * targetPath))successBlock
                  failureBlock:(void (^)(NSString * downloadPath, NSError * error))failureBlock;

/**
 *
 *  @brief 下载压缩(可能多个)(需要调试)
 *
 *  @param urlString         地址
 *  @param successBlock      请成功，以block形式返回
 *  @param failureBlock      请求失败，以block形式返回
 *
 *  有缓存的，就返回
 *  没有缓存，往 沙盒(Library->cache)目录里面下载，解压或者不解压，移动到规定的目录里面，然后返回
 *
 
 SCMFileHandleCache.m 是针对缓存，是否每次启动都清空缓存
 #if DEBUG
     //测试用的
     [_yyCache removeAllObjects];
     NSFileManager * fileManager = [NSFileManager defaultManager];
     [fileManager removeItemAtPath:self.decompressOptionPath error:nil];
 #endif
 
 */
- (void)DownloadWithURL:(NSString *)urlString
                  successBlock:(void (^)(NSString * targetPath))successBlock
                  failureBlock:(void (^)(NSString * downloadPath, NSError * error))failureBlock;


/*
     主动去销毁所有请求(只对 请求数据，不对上传、下载做缓存和清除缓存)
     UIView/Controller 的dealloc不必调用，我已hook了
 */
- (void)cancelAllRequest;

@end
