//
//  DYUploadFormData.h
//  DragonYin
//
//  Created by srxboys on 2018/5/7.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DYUploadFormData : NSObject
@property (nonatomic, strong) NSData   *data;   //图片二进制
@property (nonatomic, copy)   NSString *name;    //上传到服务器那个文件夹
@property (nonatomic, copy)   NSString *fileName;//上传到服务器的文件名
@property (nonatomic, copy, readonly)  NSString  *mimeType;//上传的文件类型
@end
