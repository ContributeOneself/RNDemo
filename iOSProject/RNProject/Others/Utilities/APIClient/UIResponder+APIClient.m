//
//  UIResponder+APIClient.m
//  SCM
//
//  Created by srx on 2017/10/18.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "UIResponder+APIClient.h"

#import "Constant.h"

#import "SCMApiClientManager.h"
#import "SCMAPICache.h"
#import <MJExtension.h>

#import <objc/runtime.h>
#import "DYDataModel.h"

// 因category不能添加属性，只能通过关联对象的方式。
static const char * API_CACHE_KEY = "scm_api_cache_key";

@interface Response()
@property (nonatomic, copy) SCMAPICache * apiCache;
@end

@implementation Response
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        _message = DictObject(dict, @"message");
        _status = [NonEmptyString(DictObject(dict, @"status")) isEqualToString:@"success"]?YES:NO;
        _content = DictObject(dict, @"content");
    }
    return self;
}

+ (Response *)responseWithDict:(NSDictionary *)dict {
    return [[Response alloc] initWithDict:dict];
}
@end


@implementation UIResponder (APIClient)
- (void)GetMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict className:(Class)className successBlock:(void (^)(Response * responseObject))successBlock failureBlock:(void (^)(NSError * error))failureBlock {
    [self RequestType:RequestGet method:method className:className paramsDict:paramsDict successBlock:successBlock failureBlock:failureBlock];
}

- (void)POSTMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict className:(Class)className successBlock:(void (^)(Response *))successBlock failureBlock:(void (^)(NSError *))failureBlock {
    [self RequestType:RequestPost method:method className:className paramsDict:paramsDict successBlock:successBlock failureBlock:failureBlock];
}
    
- (void)RequestType:(Request)RequestType method:(NSString *)method className:(Class)className  paramsDict:(NSDictionary *)paramsDict successBlock:(void (^)(Response * responseObject))successBlock failureBlock:(void (^)(NSError * error))failureBlock {
    NSParameterAssert(method);
    if(paramsDict == nil) paramsDict = @{};
    
    __block SCMAPICache * _apiCache = self.apiCache;
    
    NSString * urlString = [SERVER_URL stringByAppendingString:method];
    
    MethodType type = POST;
    if(RequestType == RequestGet) {
        type = GET;
    }
    
    UIView * hudView = [self getCurrentControllerNavigationView];
    
    NSURLSessionDataTask * sessionDataTask = [SCMApiClientManager requestHttpData:type urlString:urlString parameters:paramsDict progress:^(NSProgress *progress) {
        /*
             @property int64_t totalUnitCount;     需要下载文件的总大小
             @property int64_t completedUnitCount; 当前已经下载的大小
             CGFloat progressValue = 1.0 * progress.completedUnitCount / progress.totalUnitCount;
         
            如果需要进度条，还需要再次封装 本工具
            通常，请求接口都是不需要的，所以没有写 多余的代码
        */
    } successBlock:^(NSURLSessionDataTask *task, id responseObject) {
        Response * responseResult = [Response responseWithDict:responseObject];
        
        if(!responseResult.status) {
            //显示toast
            if(hudView) {
                MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:hudView animated:YES];
                [hud showHudLabel:responseResult.message];
            }
        }
        else {
            if(className && responseResult.content) {
                __weak id objc = nil;
                if(ArrBool(responseResult.content)) {
                    objc = [className mj_objectArrayWithKeyValuesArray:responseResult.content];
                }
                else if(DictBool(responseResult.content)){
                    objc = [className mj_objectWithKeyValues:responseResult.content];
                }
                else {
                    objc = responseResult.content;
                }
                responseResult.content = objc;
//                NSLog(@"responseResult.content=%@", responseResult.content);
            }
            
        }
        if(successBlock) successBlock(responseResult);
        
        //移除缓存
        [_apiCache removeDataCacheWithMethod:method];
    } failureBlock:^(NSURLSessionDataTask *task, NSError *error) {
        if(error.code == NSURLErrorCancelled) {
            //用户自己取消(或者 多次请求同一接口)，不做处理
        }
        else {
            if(hudView) {
                if(error.code == NSURLErrorTimedOut) {
                    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:hudView animated:YES];
                    [hud showHudLabel:@"请求超时, 请检查网络连接状态"];
                }
                else if(error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorCannotConnectToHost) {
                    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:hudView animated:YES];
                    [hud showHudLabel:@"无法连接到服务器"];
                }
            }
        }
        if(failureBlock) failureBlock(error);
        //移除缓存
        [_apiCache removeDataCacheWithMethod:method];
    }];
    
    //添加缓存
    [_apiCache addCacheDataWithMethod:method URLDataTask:sessionDataTask];
}


- (void)UploadMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>formData))block successBlock:(void (^)(Response *))successBlock failureBlock:(void (^)(NSError *))failureBlock {
    
    __block UIView * navView = [self getCurrentControllerNavigationView];
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
    NSProgress * progressObject = [NSProgress new];
    [hud showHudAnnularProgress:progressObject labelTxt:@"上传中..."];
    
    weak(weakSelf);
    [self UploadDiyMethod:method paramsDict:paramsDict constructingBodyWithBlock:^(id<AFMultipartFormData>formData) {
        block(formData);
    } progress:^(NSProgress *progress) {
        if(@available( iOS 11.0 , *)) {
            progressObject.fileTotalCount = progress.fileTotalCount;
            progressObject.fileCompletedCount = progress.fileCompletedCount;
        }
        else {
            progressObject.completedUnitCount = progress.completedUnitCount;
            progressObject.totalUnitCount = progress.totalUnitCount;
        }
    } successBlock:^(Response *responseObject) {
        [hud hideAnimated:NO];
        if(!responseObject.status) {
            navView = [weakSelf getCurrentControllerNavigationView];
            [navView showAlertMessage:responseObject.message];
        }
        if(successBlock) successBlock(responseObject);
    } failureBlock:^(NSError *error) {
        [hud hiddenHud];
        if(failureBlock) failureBlock(error);
    }];
}

- (void)UploadDiyMethod:(NSString *)method paramsDict:(NSDictionary *)paramsDict constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block progress:(void (^)(NSProgress *))progressBlock successBlock:(void (^)(Response *))successBlock failureBlock:(void (^)(NSError *))failureBlock {
    NSParameterAssert(method);
    if(paramsDict == nil) paramsDict = @{};
    NSParameterAssert(paramsDict);
    
    __block SCMAPICache * _apiCache = self.apiCache;
    NSString * urlString = [SERVER_URL stringByAppendingString:method];
    __block UIView * navView = [self getCurrentControllerNavigationView];
    
    weak(weakSelf);
    
    NSURLSessionDataTask * sessionDataTask = [SCMApiClientManager Upload:urlString parameters:paramsDict constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        block(formData);
    } progress:^(NSProgress *progress) {
        progressBlock(progress);
    } successBlock:^(NSURLSessionDataTask *task, id responseObject) {
        Response * responseResult = [Response responseWithDict:responseObject];
        if(successBlock) successBlock(responseResult);
        //移除缓存
        [_apiCache removeDataCacheWithMethod:method];
    } failureBlock:^(NSURLSessionDataTask *task, NSError *error) {
        if(error.code == NSURLErrorCancelled) {
            //用户自己取消(或者 多次请求同一接口)，不做处理
        }
        else {
            navView = [weakSelf getCurrentControllerNavigationView];
            if(error.code == NSURLErrorTimedOut) {
                MBProgressHUD * uploadFailHud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
                [uploadFailHud showHudLabel:@"请求超时, 请检查网络连接状态"];
            }
            else if(error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorCannotConnectToHost) {
                MBProgressHUD * uploadFailHud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
                [uploadFailHud showHudLabel:@"无法连接到服务器"];
            }
        }
        
        if(failureBlock) failureBlock(error);
        //移除缓存
        [_apiCache removeDataCacheWithMethod:method];
    }];
    
    //添加缓存
    [_apiCache addCacheDataWithMethod:method URLDataTask:sessionDataTask];
}

- (void)downloadRequestWithURL:(NSString *)urlString destinationPath:(NSString *)destinationPath
                  successBlock:(void (^)(NSString * targetPath))successBlock
                  failureBlock:(void (^)(NSString * downloadPath, NSError * error))failureBlock {
    
    NSParameterAssert(urlString);
    NSParameterAssert(destinationPath);
    
    UIView * navView = [self getCurrentControllerNavigationView];
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
    NSProgress * progressObject = [NSProgress new];
    [hud showHudAnnularProgress:progressObject labelTxt:@"上传中..."];
    
    [SCMApiClientManager downloadURL:urlString destinationPath:destinationPath progress:^(NSProgress *progress) {
        if(@available( iOS 11.0 , *)) {
            progressObject.fileTotalCount = progress.fileTotalCount;
            progressObject.fileCompletedCount = progress.fileCompletedCount;
        }
        else {
            progressObject.completedUnitCount = progress.completedUnitCount;
            progressObject.totalUnitCount = progress.totalUnitCount;
        }
    } successBlock:^(NSURLSessionDownloadTask *task, NSURL *filepath) {
        [hud hiddenHud];
        if(successBlock) {
            NSString * path = [filepath path];
            successBlock(path);
        }
    } failureBlock:^(NSURLSessionDownloadTask *task, NSError * error) {
        [hud hideAnimated:NO];
        if(error.code == NSURLErrorCancelled) {
            //用户自己取消(或者 多次请求同一接口)，不做处理
        }
        else {
            if(error.code == NSURLErrorTimedOut) {
                MBProgressHUD * downloadFailHud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
                [downloadFailHud showHudLabel:@"请求超时, 请检查网络连接状态"];
            }
            else if(error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorCannotConnectToHost) {
                MBProgressHUD * downloadFailHud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
                [downloadFailHud showHudLabel:@"无法连接到服务器"];
            }
        }
        if(failureBlock) {
            failureBlock(destinationPath, error);
        }
    }];
    
}

- (void)DownloadWithURL:(NSString *)urlString
           successBlock:(void (^)(NSString * targetPath))successBlock
           failureBlock:(void (^)(NSString * downloadPath, NSError * error))failureBlock {
    NSParameterAssert(urlString);
    
    //如果存在 return;
    
    UIView * navView = [self getCurrentControllerNavigationView];
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
    NSProgress * progressObject = [NSProgress new];
    [hud showHudAnnularProgress:progressObject labelTxt:@"下载中"];
    
    NSString * destinationPath = LIBRARY_CACHE();
    
    //不存在 进行下载
    [SCMApiClientManager downloadURL:urlString destinationPath:destinationPath progress:^(NSProgress *progress) {
        progressObject.completedUnitCount = progress.completedUnitCount;
        progressObject.totalUnitCount = progress.totalUnitCount;
        if(@available( iOS 11.0 , *)) {
            progressObject.fileTotalCount = progress.fileTotalCount;
            progressObject.fileCompletedCount = progress.fileCompletedCount;
        }
    } successBlock:^(NSURLSessionDownloadTask *task, NSURL *filepath) {
        [hud hiddenHud];
        
        /*是否是单个文件，目录，还是。。。*/
        /*是否有文件夹*/
        NSString * path = [filepath path];
        successBlock(path);
        
    } failureBlock:^(NSURLSessionDownloadTask *task, NSError * error) {
        [hud hiddenHud];
        
        MBProgressHUD * comHud = [MBProgressHUD showHUDAddedTo:navView animated:YES];
        UIImage *image = [[UIImage imageNamed:@"img_calendar_close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        comHud.customView = imageView;
        comHud.mode = MBProgressHUDModeCustomView;
        comHud.label.text = @"下载失败";
        [comHud hideAnimated:YES afterDelay:1.5f];
        comHud.completionBlock = ^{
            if(failureBlock) {
                failureBlock(destinationPath, error);
            }
        };
    }];
}


- (UIView *)getCurrentControllerNavigationView {
    UIView * hudView = nil;
    if([self isKindOfClass:[UIView class]]) {
        hudView = (UIView*)self;
    }
    else if([self isKindOfClass:[UIViewController class]]) {
        UIViewController * c = (UIViewController *)self;
        hudView = c.view;
    }
    
    if(!hudView) {
        UIViewController * c = [[PageRouter sharedPageRouter] currentController];
        if([c isKindOfClass:[UIViewController class]]) {
            hudView = c.view;
        }
        if(c.navigationController) {
            hudView = c.navigationController.view;
        }
    }
    return hudView;
}


- (UIViewController *)getCurrentController{
    UIResponder *next = self.nextResponder;
    do {
        //判断响应者是否为视图控制器
        if ([next isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)next;
        }
        next = next.nextResponder;
    } while (next != nil);
    
    return nil;
}


- (void)cancelAllRequest {
    //移除所有请求缓存
    [self.apiCache removeAllCacheWithObject];
}



//load方法会在类第一次加载的时候被调用
//调用的时间比较靠前，适合在这个方法里做方法交换
+ (void)load {
    
    RXLog(@"current class name =%@\n", NSStringFromClass(self));
    
    //方法交换应该被保证，在程序中只会执行一次
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        
/// init (其实可以写代码质量高的)
        //获得viewController的生命周期方法的selector
        SEL systemSel = @selector(init);
        //自己实现的将要被交换的方法的selector
        SEL swizzSel = @selector(swiz_init);
        //两个方法的Method
        Method systemMethod = class_getInstanceMethod([self class], systemSel);
        Method swizzMethod = class_getInstanceMethod([self class], swizzSel);
        
        //首先动态添加方法，实现是被交换的方法，返回值表示添加成功还是失败
        BOOL isAdd = class_addMethod(self, systemSel, method_getImplementation(swizzMethod), method_getTypeEncoding(swizzMethod));
        if (isAdd) {
            //如果成功，说明类中不存在这个方法的实现
            //将被交换方法的实现替换到这个并不存在的实现
            class_replaceMethod(self, swizzSel, method_getImplementation(systemMethod), method_getTypeEncoding(systemMethod));
        }else{
            //否则，交换两个方法的实现
            method_exchangeImplementations(systemMethod, swizzMethod);
        }
        
//---------------------------------------------------------------------
/// 页面消失的时候
        
        //获得viewController的生命周期方法的viewDidDisappear
        systemSel = @selector(viewDidDisappear:);
        //自己实现的将要被交换的方法的selector
        swizzSel = @selector(swiz_dealloc);
        //两个方法的Method
        systemMethod = class_getInstanceMethod([self class], systemSel);
        swizzMethod = class_getInstanceMethod([self class], swizzSel);
        
        //首先动态添加方法，实现是被交换的方法，返回值表示添加成功还是失败
        isAdd = class_addMethod(self, systemSel, method_getImplementation(swizzMethod), method_getTypeEncoding(swizzMethod));
        if (isAdd) {
            //如果成功，说明类中不存在这个方法的实现
            //将被交换方法的实现替换到这个并不存在的实现
            class_replaceMethod(self, swizzSel, method_getImplementation(systemMethod), method_getTypeEncoding(systemMethod));
        }else{
            //否则，交换两个方法的实现
            method_exchangeImplementations(systemMethod, swizzMethod);
        }
        
        
    });
}


- (instancetype)swiz_init
{
    UIResponder * selfObject = [self swiz_init];
    if(selfObject) {
        SCMAPICache * cache = [[SCMAPICache alloc] init];
        if(cache) {
            selfObject.apiCache = cache;
        }
    }
    return selfObject;
}


- (void)swiz_dealloc {
    [self cancelAllRequest];
}



- (SCMAPICache *)apiCache {
    return objc_getAssociatedObject(self, API_CACHE_KEY);
}

- (void)setApiCache:(SCMAPICache *)apiCache
{
    // 第一个参数：给哪个对象添加关联
    // 第二个参数：关联的key，通过这个key获取
    // 第三个参数：关联的value
    // 第四个参数:关联的策略
    objc_setAssociatedObject(self, API_CACHE_KEY, apiCache, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
