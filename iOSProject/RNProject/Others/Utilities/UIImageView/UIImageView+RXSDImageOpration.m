//
//  UIImageView+RXSDImageOpration.m
//  RX
//
//  Created by srx on 2017/9/19.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "UIImageView+RXSDImageOpration.h"
#import "AFNetworkReachabilityManager.h"

UIKIT_STATIC_INLINE NSError* RXSDImageError(NSString * desc) {
    return  [[NSError alloc] initWithDomain:@"RXSDImageOpration" code:NSURLErrorCannotFindHost userInfo:@{NSLocalizedDescriptionKey:NonEmptyString(desc)}];
}


@implementation UIImageView (RXSDImageOpration)
- (void)scm_setImageWithOriginalImage:(NSString *)originalImageUrl ThumbImage:(NSString *)thumbImageUrl {
    [self scm_setImageWithOriginalImage:originalImageUrl ThumbImage:thumbImageUrl placeholderImage:nil];
}

- (void)scm_setImageWithOriginalImage:(NSString *)originalImageUrl ThumbImage:(NSString *)thumbImageUrl placeholderImage:(UIImage *)placeholde {
    [self scm_setImageWithOriginalImage:originalImageUrl ThumbImage:thumbImageUrl placeholderImage:placeholde completed:nil];
}

- (void)scm_setImageWithOriginalImage:(NSString *)originalImageUrl ThumbImage:(NSString *)thumbImageUrl placeholderImage:(UIImage *)placeholder completed:( SDExternalCompletionBlock)completedBlock {
    
    NSURL * originalURL = [NSURL URLWithString:originalImageUrl];
    NSString *originalImagekey = [[SDWebImageManager sharedManager] cacheKeyForURL:originalURL];
    UIImage * bigImage = [[SDImageCache sharedImageCache] imageFromCacheForKey:originalImagekey];
    // 是否有缓存【高清图片】
    if (bigImage) {
        self.image = bigImage;
        
        if(completedBlock) {
            completedBlock(bigImage, nil, 200, nil);
        }
    }
    else {
        NSURL * thumbURL = [NSURL URLWithString:thumbImageUrl];
        if(!thumbURL) {
            //网址都不对，怎么可能下载图片呢？
            self.image = placeholder;
            if(completedBlock) {
                completedBlock(placeholder, RXSDImageError(@"缩略图网址错误"), SDImageCacheTypeNone,thumbURL);
            }
            return;
        }
        
        AFNetworkReachabilityManager * manager = [AFNetworkReachabilityManager sharedManager];
        switch (manager.networkReachabilityStatus) {
            case AFNetworkReachabilityStatusUnknown:
            // 未知网络
            case AFNetworkReachabilityStatusNotReachable:
            {
            //没有网络(断网)
                //SDWebImage  缓存key
                NSString *thumbKey = [[SDWebImageManager sharedManager] cacheKeyForURL:thumbURL];
            
                // 是否有【磁盘】缓存【缩略图片】
                UIImage *thumbnailImage = [[SDImageCache sharedImageCache] imageFromCacheForKey:thumbKey];
            
                if(!thumbnailImage) {
                    self.image = thumbnailImage;
                    
                    if(completedBlock) {
                        completedBlock(thumbnailImage, nil,SDImageCacheTypeDisk,thumbURL);
                    }
                }
                else {
                    // 是否有【内存】缓存【缩略图片】
                    thumbnailImage = [[SDImageCache sharedImageCache] imageFromCacheForKey:thumbKey];
                    if(thumbnailImage) {
                        self.image = thumbnailImage;
                        
                        if(completedBlock) {
                            completedBlock(thumbnailImage, nil,SDImageCacheTypeMemory,thumbURL);
                        }
                    }
                    else {
                        self.image = placeholder;
                        if(completedBlock) {
                            completedBlock(placeholder, RXSDImageError(@"缩略图没有缓存"), SDImageCacheTypeNone,thumbURL);
                        }
                    }
                }
            }
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
            {
            // 手机自带网络
                [self sd_setImageFIFOWithURL:thumbURL placeholderImage:placeholder completed:completedBlock];
            }
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {
            // WIFI
                [self sd_setImageFIFOWithURL:originalURL placeholderImage:placeholder completed:completedBlock];
            }
                break;
            default:
                break;
        }
    }
}

@end
