//
//  UIImageView+RXSDImageOpration.h
//  RX
//
//  Created by srx on 2017/9/19.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"
#import "UIImageView+fadeInFadeOut.h"

@interface UIImageView (RXSDImageOpration)

/**
 *  下载图片（网络状态检测,是否下载高清图）
 *
 *  @param originalImageUrl 原始图
 *  @param thumbImageUrl    缩略图
 *   placeholderImage       默认图
 *
 */

- (void)scm_setImageWithOriginalImage:(NSString *)originalImageUrl ThumbImage:(NSString *)thumbImageUrl;

- (void)scm_setImageWithOriginalImage:(NSString *)originalImageUrl ThumbImage:(NSString *)thumbImageUrl placeholderImage:(UIImage *)placeholde;

- (void)scm_setImageWithOriginalImage:(NSString *)originalImageUrl ThumbImage:(NSString *)thumbImageUrl placeholderImage:(UIImage *)placeholder completed:( SDExternalCompletionBlock)completedBlock;


/**
       手机网络下，去下载高清图，请使用 #import "UIImageView+fadeInFadeOut.h"
 
       或者
       
       你在自己封装下吧！
 */

@end
