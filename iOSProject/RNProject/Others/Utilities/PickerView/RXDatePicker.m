//
//  RXDatePicker.m
//  RXExtenstion
//
//  Created by srx on 16/8/31.
//  Copyright © 2016年 https://github.com/srxboys. All rights reserved.
//
// 基本 日期picker 自定义封装(可以在这个基础上封装自己想要的 更多样式)
//2015年 写的，方法、逻辑 处理等等，都不是很好，我懒得不想改了

#import "RXDatePicker.h"

#define PICK_SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define PICK_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define PICKER_HEIGHT 216
#define PICKER_TOOL_HEIGHT 44

#define viewAnimal 0.3

#define ButtonLeft 18

@interface RXDatePicker ()
@property (nonatomic, strong) UIView * keyBoardView;
@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, strong) UIView * backView;

@property (nonatomic, assign) CGRect viewHiddenFrame;
@property (nonatomic, assign) CGRect viewShowFrame;

@property (nonatomic, strong) NSDate* currentSelectDate;

@property (nonatomic, assign) BOOL isFist;
@property (nonatomic, strong) UITableView * tempSuperView;
@end


@implementation RXDatePicker

- (instancetype)init {
    return [self initWithFrame:[UIScreen mainScreen].bounds];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        
        /*  如果系统导航栏没有隐藏需要+NAVI_BAR_HEIGHT
            如果隐藏了就不用+NAVI_BAR_HEIGHT
         
            所以这个封装的不是 很好
         */
        CGFloat keyBoardHeight = PICKER_HEIGHT + PICKER_TOOL_HEIGHT+NAVI_BAR_HEIGHT;
        _viewHiddenFrame = CGRectMake(0, frame.size.height, frame.size.width, keyBoardHeight);
        _viewShowFrame = CGRectMake(0, frame.size.height - keyBoardHeight, frame.size.width, keyBoardHeight);

        self.backgroundColor = [UIColor clearColor];
        self.hidden = YES;
        
        
        self.layer.masksToBounds = NO;
        
        _backView = [[UIView alloc] initWithFrame:self.bounds];
        _backView.backgroundColor = [UIColor blackColor];
        _backView.alpha = 0.0f;
        _backView.hidden = YES;
        _backView.userInteractionEnabled = NO;
        [self addSubview:_backView];
        
        
        [self configPicker];
    }
    return self;
}

- (void)configPicker {
    
    self.isFist = YES;
    
    _keyBoardView = [[UIView alloc] initWithFrame:_viewHiddenFrame];
    _keyBoardView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_keyBoardView];
    
    _currentSelectDate = [NSDate date];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, _viewHiddenFrame.size.width, PICKER_TOOL_HEIGHT)];
    UIBarButtonItem *spacerItemFixed = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacerItemFixed.width = 10;
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonClick)];
    [cancelItem setTintColor:UIColorRGB(51 , 51 , 51)];
    UIBarButtonItem *spacerItemCenter = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *okItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(trueButtonClick)];
    [okItem setTintColor:UIColorRGB(51 , 51 , 51)];
    toolbar.items = @[spacerItemFixed, cancelItem, spacerItemCenter, okItem, spacerItemFixed];
    [_keyBoardView addSubview:toolbar];


    _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, PICKER_TOOL_HEIGHT, _viewHiddenFrame.size.width, PICKER_HEIGHT)];
    _datePicker.backgroundColor = [UIColor whiteColor];
    _datePicker.clipsToBounds = YES;
    [_keyBoardView addSubview:_datePicker];
    
    //设置为中文
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    _datePicker.locale = locale;
    
    //通过setDatePickerMode方法 来设置UIDatePicker的样式
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    
    
    _datePicker.minimumDate = [self formatterStringToDate:@"1699-04-04"];
    _datePicker.maximumDate = [NSDate date];
    
    //日历今天，默认也是今天
    [_datePicker setCalendar:[NSCalendar currentCalendar]];
    
    //添加事件
    [_datePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
    
}

- (NSDate *)formatterStringToDate:(NSString *)dateString {
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    [inputFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate* date = [inputFormatter dateFromString:dateString];
    return date;
}


-(void)datePickerDateChanged:(UIDatePicker *)paramDatePicker {
    
    _isFist = false;
    
    if ([paramDatePicker isEqual:_datePicker]){
        RXLog(@"Selected date = %@", paramDatePicker.date);
        _currentSelectDate = paramDatePicker.date;
        self.pickerDateBlock(NO, _currentSelectDate);
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //[self trueButtonClick];
    [self hidden];
}

- (void)cancelButtonClick {
    //    self.isShow(false, @"");
    [self hidden];
}

- (void)trueButtonClick {
    
    if(self.pickerDateBlock) {
        self.pickerDateBlock(true, _currentSelectDate);
    }
    [self hidden];
}

- (void)show:(UITableView *)view {
    _tempSuperView = view;
    self.hidden = NO;
    
    //226 为【出生日期】= y + height
    //    TTLog(@"screenHeight =%.2f", ScreenHeight);
    //在table中 的 top + height
//    CGFloat dataHeight = NAVI_BAR_HEIGHT + 226;
    
//    CGFloat height = (PICK_SCREEN_HEIGHT - dataHeight) < _viewShowFrame.size.height ? _viewShowFrame.size.height - (PICK_SCREEN_HEIGHT - dataHeight) : 0;
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:viewAnimal animations:^{
        weakSelf.backView.alpha = 0.4f;
        weakSelf.keyBoardView.frame = weakSelf.viewShowFrame;
//        weakSelf.tempSuperView.contentOffset = CGPointMake(0, height);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)show {
    self.hidden = NO;
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:viewAnimal animations:^{
        weakSelf.backView.alpha = 0.4f;
        weakSelf.keyBoardView.frame = weakSelf.viewShowFrame;
    } completion:^(BOOL finished) {
        
    }];
}


- (void)hidden {
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:viewAnimal animations:^{
        weakSelf.backView.alpha = 0;
        weakSelf.keyBoardView.frame  = weakSelf.viewHiddenFrame;
        weakSelf.tempSuperView.contentOffset = CGPointMake(0, 0);
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}


- (void)setBirthdayDate:(NSDate *)birthdayDate {
    _datePicker.date = birthdayDate;
}

- (void)setNomalDate:(NSDate *)nomalDate {
    _datePicker.date = nomalDate;
}

- (NSDate *)nomalDate {
    return _datePicker.date;
}


- (void)setNomalDateString:(NSString *)nomalDateString {
    _datePicker.date = [self formatterStringToDate:nomalDateString];
}


@end
