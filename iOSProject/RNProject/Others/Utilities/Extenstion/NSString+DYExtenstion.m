//
//  NSString+DYExtenstion.m
//  DragonYin
//
//  Created by srxboys on 2018/4/29.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "NSString+DYExtenstion.h"

@implementation NSString (DYExtenstion)
- (NSString *)ciphertext {
    if(!StrBool(self)) return nil;
    NSString * str = self;
    if([str containsString:@"*"]) return self;
    if(str.length > 8) {
        str = [self encryptedString:str len:8];
    }
    else if(self.length > 4) {
        str = [self encryptedString:str len:4];
    }
    return str;
}

- (NSString *)encryptedString:(NSString *)str len:(NSInteger)len {
    if(str.length < len) return str;
    
    NSString * startS = @"*";
    for(NSInteger i = 0; i < str.length - len; i ++) {
        startS = [startS stringByAppendingString:@"*"];
    }
    NSRange range = NSMakeRange(len/2, str.length-len);
    str = [str stringByReplacingCharactersInRange:range withString:startS];
    return str;
}

//获取拼音首字母(传入汉字字符串, 返回大写拼音首字母)
- (NSString *)firstCharactor {
    //转成了可变字符串
    NSMutableString *str = [NSMutableString stringWithString:self];
    //先转换为带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformMandarinLatin,NO);
    //再转换为不带声调的拼音
    CFStringTransform((CFMutableStringRef)str,NULL, kCFStringTransformStripDiacritics,NO);
    //转化为大写拼音
    NSString *pinYin = [str uppercaseString];
    pinYin = [pinYin stringByReplacingOccurrencesOfString:@" " withString:@""];
    //获取并返回首字母
    return pinYin;
}

- (NSComparisonResult)compareToVersion:(NSString *)version {
    if(!version) return NSOrderedDescending;
    NSComparisonResult result;
    NSString * versionSeparator = @".";
    
    result = NSOrderedSame;
    
    if(![self isEqualToString:version]){
        NSArray *thisVersion = [self componentsSeparatedByString:versionSeparator];
        NSArray *compareVersion = [version componentsSeparatedByString:versionSeparator];
        
        for(NSInteger index = 0; index < MAX([thisVersion count], [compareVersion count]); index++){
            NSInteger thisSegment = (index < [thisVersion count]) ? [[thisVersion objectAtIndex:index] integerValue] : 0;
            NSInteger compareSegment = (index < [compareVersion count]) ? [[compareVersion objectAtIndex:index] integerValue] : 0;
            
            if(thisSegment < compareSegment){
                result = NSOrderedAscending;
                break;
            }
            
            if(thisSegment > compareSegment){
                result = NSOrderedDescending;
                break;
            }
        }
    }
    
    return result;
}

@end
