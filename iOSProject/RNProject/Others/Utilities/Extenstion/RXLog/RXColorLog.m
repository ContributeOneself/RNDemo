//
//  RXColorLog.m
//  RXExtenstion
//
//  Created by srx on 2016/11/28.
//  Copyright © 2016年 https://github.com/srxboys. All rights reserved.
//

#import "RXColorLog.h"
#import "RXInterfacePrint.h"
#import "Constant.h"

@implementation RXColorLog
+ (void)printLog:(BOOL)isError file:(char *)file line:(int)line method:(NSString *)method content:(NSString *)format {
#ifdef DEBUG
    
    NSString *filePath = [[NSString stringWithUTF8String:file] lastPathComponent];
    
    if(SET_INTERFACE_LOG_ENABLE) {
        [[RXInterfacePrint shareInterfacePrint] printLogFile:filePath line:line method:method content:format];
    }
    else if(SEC_LOG_ENABLE) {
        
        
        NSString * headerIdentification = @"👇👇👇👇👇👇👇";
        NSString * footerIdentification = @"👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉👉";
        if(isError) {
            headerIdentification = @"❌❌❌❌❌❌❌";
            footerIdentification = @"🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺";
        }
        
        NSString *LogHeader = [NSString stringWithFormat:@": %@ \n   Ⓕⓘⓛⓔ ⏩ %@\n   Ⓛⓘⓝⓔ ⏩ %d\n   Ⓕⓤⓝⓒⓣⓘⓞⓝ ⏩",headerIdentification, filePath, line];
        
        format = [NSString stringWithFormat:@"\n%@", format];
        format = [format stringByAppendingString:@"\n\n"];
        format = [format stringByAppendingString:footerIdentification];
        printf("\n");
        if(isError) {
            NSLog(@"%@ %@ \n%@", LogHeader, method,format);
        }
        else {
            NSLog(@"%@ %@ \n%@", LogHeader, method,format);
        }
        printf("\n");
    }
#endif
}
@end
