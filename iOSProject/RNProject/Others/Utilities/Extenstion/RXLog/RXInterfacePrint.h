//
//  RXInterfacePrint.h
//  SCM
//
//  Created by srx on 2017/10/16.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RXInterfacePrint : NSObject
+ (RXInterfacePrint *)shareInterfacePrint;
- (void)printLogFile:(NSString *)filePath line:(int)line method:(NSString *)method content:(NSString *)format;
@end
