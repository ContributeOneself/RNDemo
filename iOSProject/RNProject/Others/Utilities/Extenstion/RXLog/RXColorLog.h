//
//  RXColorLog.h
//  RXExtenstion
//
//  Created by srx on 2016/11/28.
//  Copyright © 2016年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>

#if DEBUG
    #define RXLog(format, ...) RXLogger(NO, format, ##__VA_ARGS__)
    #define RXLogError(format, ...) RXLogger(YES, format, ##__VA_ARGS__)

    #define RXLogger(__isError,__format,  ...) [RXColorLog printLog:__isError file:__FILE__ line:__LINE__ method:[NSString stringWithFormat:@"%s", __PRETTY_FUNCTION__] content:[NSString stringWithFormat:(__format), ##__VA_ARGS__]]
#else
    #define RXLog(...)
    #define RXLogError(...)
#endif

@interface RXColorLog : NSObject
+ (void)printLog:(BOOL)isError file:(char *)file line:(int)line method:(NSString *)method content:(NSString *)format;
@end
