//
//  RXInterfacePrint.m
//  SCM
//
//  Created by srx on 2017/10/16.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "RXInterfacePrint.h"
#import "Constant.h"

static RXInterfacePrint * __interfacePrint = nil;

@interface RXInterfacePrint()
@property (nonatomic, copy) NSMutableArray * apiMethodArray;
@property (nonatomic, copy) NSMutableDictionary * apiDictionary;
@end

@implementation RXInterfacePrint
+ (RXInterfacePrint *)shareInterfacePrint {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __interfacePrint = [[RXInterfacePrint alloc] init];
    });
    return __interfacePrint;
}

- (instancetype)init {
    return [self initConfig];
}

- (instancetype)initConfig {
    if(__interfacePrint) {
        return __interfacePrint;
    }
    _apiMethodArray = [[NSMutableArray alloc] init];
    _apiDictionary = [[NSMutableDictionary alloc] init];
    return [super init];
}

- (void)printLogFile:(NSString *)filePath line:(int)line method:(NSString *)method content:(NSString *)format {
    
#ifdef DEBUG
    
    if([format rangeOfString:SERVER_URL].location == NSNotFound) return;
    
    if(self.apiMethodArray.count == 0) {
        [self.apiDictionary setObject:format forKey:method];
        [self.apiMethodArray addObject:method];
        NSLog(@"\n\nmethod=%@\n%@\n\n\n\n", method, format);
    }
    else {
        if(![self.apiMethodArray containsObject:method]) {
            [self.apiDictionary setObject:format forKey:method];
            [self.apiMethodArray addObject:method];
            NSLog(@"\n\nmethod=%@\n=%@\n\n\n\n", method, format);
        }
    }
#endif
}
@end
