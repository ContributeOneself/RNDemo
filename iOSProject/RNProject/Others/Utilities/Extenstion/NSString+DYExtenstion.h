//
//  NSString+DYExtenstion.h
//  DragonYin
//
//  Created by srxboys on 2018/4/29.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DYExtenstion)
/// 中间加密
- (NSString *)ciphertext;

/// //获取拼音首字母(传入汉字字符串, 返回大写拼音首字母，结果无音节)
- (NSString *)firstCharactor;

- (NSComparisonResult)compareToVersion:(NSString *)version;
@end
