//
//  NSHTTPCookie+DYExtenstion.m
//  DragonYin
//
//  Created by srxboys on 2018/5/14.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import "NSHTTPCookie+DYExtenstion.h"

@implementation NSHTTPCookie (DYExtenstion)
+ (BOOL)setCookieName:(NSString *)name value:(NSString *)value {
    name = StrFormatWhiteSpace(name);
    if(!StrBool(name)) return NO;
    
    value = StrFormatWhiteSpace(value);
    
    NSDictionary <NSHTTPCookiePropertyKey, NSString *> * propertyDict = @{
                                                  NSHTTPCookieVersion:@"0",
                                                  NSHTTPCookieName:name,
                                                  NSHTTPCookieValue:value,
                                                  NSHTTPCookieDomain:@"DragonYin",
                                                  NSHTTPCookiePath:@"/"
                                                                          };
    
    
    NSHTTPCookie * cookie = [NSHTTPCookie cookieWithProperties:propertyDict];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];;
    return YES;
}
@end
