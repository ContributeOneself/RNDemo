//
//  GHSPrisonBreak.h
//  GHS
//
//  Created by srx on 16/6/14.
//  Copyright © 2016年 GHS. All rights reserved.
//
////是否 越狱 设备

#import <Foundation/Foundation.h>

#define prisonBreakDefault @"prisonBreakDefaultBOOL"

@interface GHSPrisonBreak : NSObject
+ (void)prisonBreakCheck;
@end
