//
//  NSHTTPCookie+DYExtenstion.h
//  DragonYin
//
//  Created by srxboys on 2018/5/14.
//  Copyright © 2018年 https://github.com/srxboys. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHTTPCookie (DYExtenstion)
+ (BOOL)setCookieName:(NSString *)name value:(NSString *)value;
@end
