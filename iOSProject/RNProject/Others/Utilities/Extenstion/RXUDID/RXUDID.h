//
//  RXUUID.h
//  RXExtenstion
//
//  Created by srx on 16/6/27.
//  Copyright © 2016年 https://github.com/srxboys. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RXUDID : NSObject
/// 此buildID+证书在 该设备的唯一标识UDID(除非还原出厂设置失效)
+ (NSString*)getUMSUDID;
@end
