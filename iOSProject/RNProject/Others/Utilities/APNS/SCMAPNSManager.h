//
//  SCMAPNSNotificationManager.h
//  SCM
//
//  Created by srx on 2017/9/14.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//
//接受推送处理

#import <Foundation/Foundation.h>

@interface SCMAPNSManager : NSObject

/**  标识，不可以被外部调用 */
- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

@property(nonatomic, strong, readonly) NSString *deviceToken;

/// 单例
+ (SCMAPNSManager *)shareSCMAPNSManager;

/// 让设备去 注册 AFNS
- (void)registerForRemoteNotification;

/// AFNS 返回token 我们给 推送服务器
- (void)successRegisterWithDeviceToken:(NSData*)deviceToken;

//直接跳转
//- (void)AppBackgroundDetail:(NSDictionary *)allDict;

- (void)clearIconBadgeNumber;
- (void)setIconBadgeNumber:(NSInteger)num;

/// 个推绑定别名
//- (void)GeTuiBindAlias;

/// 个推解绑
//- (void)GeTuiUnBindAlias;

@end
