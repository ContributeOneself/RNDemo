//
//  SCMAPNSNotificationManager.m
//  SCM
//
//  Created by srx on 2017/9/14.
//  Copyright © 2017年 Global Home Shopping. All rights reserved.
//

#import "SCMAPNSManager.h"
#import "AppDelegate.h"
#import "Constant.h"
//#import <GTSDK/GeTuiSdk.h>
//#import "NSString+GHSEmojiAnalysis.h"
#import "SCMAlert.h"
#import "TalkingData.h"
//#import "SCMWebViewController.h"


#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    #import <UserNotifications/UserNotifications.h>
#endif

static SCMAPNSManager * AFNSManager = nil;

@interface SCMAPNSManager()<
UNUserNotificationCenterDelegate
//,GeTuiSdkDelegate
>
{
    NSDictionary * _reviceUserDict;
    BOOL           _apns_is_first;
}
@end

@implementation SCMAPNSManager

+ (SCMAPNSManager *)shareSCMAPNSManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        AFNSManager = [[SCMAPNSManager alloc] initConfig];
    });
    return AFNSManager;
}

- (instancetype)initConfig {
    if(AFNSManager) {
        return AFNSManager;
    }
    [self configAlloc];
    return [super init];
}

#pragma mark - ~~~~~~~~~~~ 所有账户设置 ~~~~~~~~~~~~~~~
- (void)configAlloc {
    /*
//    self.userId = [NSString stringWithFormat:@"%lld",[GlobalData sharedInstance].user.member_id];
    _apns_is_first = NO;
    //[1-1]:通过 AppId、 appKey 、appSecret 启动SDK
    //该方法需要在主线程中调用
    [GeTuiSdk startSdkWithAppId:GLOBAL_GETUI_APP_ID appKey:GLOBAL_GETUI_APP_KEY appSecret:GLOBAL_GETUI_APP_SECRET delegate:self];
    
    //[1-2]:设置是否后台运行开关
    [GeTuiSdk runBackgroundEnable:YES];
    //[1-3]:设置电子围栏功能，开启LBS定位服务 和 是否允许SDK 弹出用户定位请求
    [GeTuiSdk lbsLocationEnable:YES andUserVerify:YES];
    */
    NOTI_ADD_OBNAME(@selector(getNotifactionWithApplicationDidBecomeActive), UIApplicationDidBecomeActiveNotification);
}

- (void)getNotifactionWithApplicationDidBecomeActive {
    if(!_apns_is_first) {
        _apns_is_first = YES;
    }
}

#pragma mark - ~~~~~~~~~~~ 让设备去 注册 AFNS ~~~~~~~~~~~~~~~
- (void)registerForRemoteNotification {
    /*
     警告：Xcode8的需要手动开启“TARGETS -> Capabilities -> Push Notifications”
     */
    
    /*
     警告：该方法需要开发者自定义，以下代码根据APP支持的iOS系统不同，代码可以对应修改。
     以下为演示代码，注意根据实际需要修改，注意测试支持的iOS系统都能获取到DeviceToken
     */
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionCarPlay) completionHandler:^(BOOL granted, NSError *_Nullable error) {
            if (!error) {
                RXLog(@"request authorization succeeded!");
            }
        }];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
#else
        UIUserNotificationType types = (UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings ; categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif // Xcode 7编译会调用
    } else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIUserNotificationType types = (UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        //Xcode7.0
/*
 【去掉编译警告】definition of implicit copy %select{constructor|assignment operator}1 for %0 is deprecated because it has a user-declared %select{copy %select{assignment operator|constructor}1|destructor}2
 */
#pragma clang diagnostic push 
#pragma clang diagnostic ignored "-Wdeprecated"
        UIRemoteNotificationType apn_type = (UIRemoteNotificationType)(UIRemoteNotificationTypeAlert |
                                                                       UIRemoteNotificationTypeSound |
                                                                       UIRemoteNotificationTypeBadge);
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:apn_type];
#pragma clang diagnostic pop
    }
    
}

#pragma mark - ~~~~~~~~~~~ AFNS 返回token 我们给 推送服务器 ~~~~~~~~~~~~~~~
- (void)successRegisterWithDeviceToken:(NSData*)deviceToken
{
    _deviceToken = @"";
    if(deviceToken.length <= 0) return;
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    _deviceToken = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //向个推服务器注册deviceToken
//    [GeTuiSdk registerDeviceToken:self.deviceToken];
    
    //向TalkingData服务器注册deviceToken
    [TalkingData setDeviceToken:deviceToken];
    
    RXLog(@"device token: %@", self.deviceToken);
}

#pragma mark - ~~~~~~~~~~~ 清楚角标 ~~~~~~~~~~~~~~~
- (void)clearIconBadgeNumber {
    
    //    [GeTuiSdk resetBadge]; //复位 不管用
//    [GeTuiSdk clearAllNotificationForNotificationBar];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)setIconBadgeNumber:(NSInteger)num {
    if(num == 0) {
        [self clearIconBadgeNumber];
    }
    else {
//        [GeTuiSdk setBadge:num];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:num];
    }
}

/*
- (void)dealWithUserInfoDictionary:(NSDictionary *)userInfo {
 
     [TalkingData handlePushMessage:userInfo];

    SCMPageRouter * router = [SCMPageRouter sharedPageRouter];
    if(![router.rootController isKindOfClass:[UITabBarController class]]) {
        return;
    }
    
//    [self showMessage:@"来了~~~~"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NewsModel * newsModel = [NewsModel newsModelWithDict:userInfo];
        if(StrBool(newsModel.detailUrl)) {
            
            NSString * accessToken = GET_ACCESSTOKEN;
            if(!StrBool(accessToken)) {
                return ;
            }
            
            if(![newsModel.detailUrl containsString:@"accessToken"]) {
                NSString * addAccessToken = [NSString stringWithFormat:@"&accessToken=%@",accessToken];
                newsModel.detailUrl = [newsModel.detailUrl stringByAppendingString:addAccessToken];
            }
            
            SCMWebViewController * webVC = [SCMWebViewController new];
            webVC.title = @"消息详情";
            webVC.urlString = newsModel.detailUrl;
        
            router.selectedVC = 2;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [router readMsgId:newsModel.idStr];
            });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [router push:webVC];
            });
        }
    });
}











#pragma mark - ~~~~~~~~~~~ iOS 10中收到推送消息 ~~~~~~~~~~~~~~~

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
//  iOS 10: App在前台获取到通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    
    RXLog(@"willPresentNotification：%@", notification.request.content.userInfo);
    
    //去处理
    [self AppForegroundDetail:notification.request.content.userInfo];
    
    // 根据APP需要，判断是否要提示用户Badge、Sound、Alert
    completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert);
}

//  iOS 10: 点击通知进入App时触发
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    RXLog(@"didReceiveNotification：%@", response.notification.request.content.userInfo);
    
    // [ GTSdk ]：将收到的APNs信息传给个推统计
    [GeTuiSdk handleRemoteNotification:response.notification.request.content.userInfo];
    
    if(_apns_is_first) {
        [self AppBackgroundDetail:response.notification.request.content.userInfo];
    }
    
    completionHandler();
}
#endif





#pragma mark - ~~~~~~~~~~~ 个推SDK返回数据代理 ~~~~~~~~~~~~~~~
/// SDK启动成功返回cid
- (void)GeTuiSdkDidRegisterClient:(NSString *)clientId {
    //个推SDK已注册，返回clientId
    RXLog(@"\n>>[GTSdk RegisterClient]:%@\n\n", clientId);
    
    [self GeTuiBindAlias];
}

- (void)GeTuiBindAlias {
    NSString * accessToken = GET_ACCESSTOKEN;
    NSString * userId = UserGetSetting(Sandbox_USER_ID);
    if(StrBool(accessToken) && StrBool(userId)) {
        NSString * uid = UserGetSetting(Sandbox_APP_UDID);
#if DEBUG
        NSAssert(StrBool(userId), @"没有值");
#endif
        [GeTuiSdk bindAlias:userId andSequenceNum:uid];
    }
}

- (void)GeTuiUnBindAlias {
    NSString * accessToken = GET_ACCESSTOKEN;
    if(StrBool(accessToken)) {
        NSString * uid = UserGetSetting(Sandbox_APP_UDID);
        NSString * userId = UserGetSetting(Sandbox_USER_ID);
#if DEBUG
        NSAssert(StrBool(userId), @"没有值");
#endif
        [GeTuiSdk unbindAlias:userId andSequenceNum:uid andIsSelf:YES];
    }
}

- (void)GeTuiSdkDidAliasAction:(NSString *)action result:(BOOL)isSuccess sequenceNum:(NSString *)aSn error:(NSError *)aError {
    if ([kGtResponseBindType isEqualToString:action]) {
        RXLog(@"绑定结果 ：%@ !, sn : %@", isSuccess ? @"成功" : @"失败", aSn);
        if (!isSuccess) {
            RXLog(@"失败原因: %@", aError);
        }
        
    } else if ([kGtResponseUnBindType isEqualToString:action]) {
        RXLog(@"解绑 结果 ：%@ !, sn : %@", isSuccess ? @"成功" : @"失败", aSn);
        if (!isSuccess) {
            RXLog(@"失败原因: %@", aError);
        }
    }
}

/// SDK运行状态通知
- (void)GeTuiSDkDidNotifySdkState:(SdkStatus)aStatus {
    // 通知SDK运行状态
    RXLog(@"\n>>[GTSdk SdkState]:%u\n\n", aStatus);
}

/// SDK设置推送模式回调
- (void)GeTuiSdkDidSetPushMode:(BOOL)isModeOff error:(NSError *)error {
    if (error) {
        RXLog(@"\n>>[GTSdk SetModeOff Error]:%@\n\n", [error localizedDescription]);
        return;
    }
    
    RXLog(@"\n>>[GTSdk SetModeOff]:%@\n\n", isModeOff ? @"开启" : @"关闭");
}


#pragma mark --------------------【 透传推送消息 】----------------
//// APP已经接收到“远程”通知(推送) - 透传推送消息
- (void)GeTuiSdkDidReceivePayloadData:(NSData *)payloadData andTaskId:(NSString *)taskId andMsgId:(NSString *)msgId andOffLine:(BOOL)offLine fromGtAppId:(NSString *)appId {
    
    //GeTui1.5.0 新增 接受率
    [GeTuiSdk sendFeedbackMessage:90001 andTaskId:taskId andMsgId:msgId];
    
    //表示离线 和 有数据
    if(!offLine && payloadData.length > 0) {
        [self AppForegroundDetail:payloadData];
    }
}


- (void)AppForegroundDetail:(id)objc {
    
    SCMPageRouter * router = [SCMPageRouter sharedPageRouter];
    if(![router.rootController isKindOfClass:[UITabBarController class]]) return;
    
    NSDictionary *responseJSON  = objc;
    if(![objc isKindOfClass:[NSDictionary class]]) {
        responseJSON = [NSJSONSerialization JSONObjectWithData:objc options:NSJSONReadingMutableLeaves error:nil];
    }
    if(responseJSON.count > 0) {
        RXLog(@"推送消息为=%@", responseJSON);
        
        //app接收到推送，发送通知，登录窗口消失
        
        _reviceUserDict = responseJSON;
        NewsModel * newsModel = [NewsModel newsModelWithDict:_reviceUserDict];
        NSString * title = newsModel.title;
        NSString * message = newsModel.content;
        
        if(message.length > 30) {
            message = [message substringToIndex:30];
            message = [message stringByAppendingString:@"..."];
        }
        
        title = [title emojizedString];
        message = [message emojizedString];
        
        if(title.length <=0 && message.length <= 0) {
            return;
        }
        else {
            weak(weakSelf);
            [self showAlertTitle:title message:message leftTitle:@"前往" rightTitle:@"关闭" actionHandler:^(NSInteger clickIndex) {
                if(clickIndex != -1) {
                    [weakSelf dealWithUserInfoDictionary:_reviceUserDict];
                }
            }];
        }
    }
    else {
        NSString * message = [[NSString alloc] initWithData:objc encoding:NSUTF8StringEncoding];
        if(StrBool(message)) {
            _reviceUserDict = @{@"message":message};
            [self showAlertMessage:message];
        }
        else {
            RXLog(@"nono");
        }
    }
}

- (void)AppBackgroundDetail:(NSDictionary *)allDict {
    if(!DictBool(allDict)) {
        return;
    }
    id payloadDict = DictObject(allDict, @"payload");
    if(![[payloadDict class] isKindOfClass:[NSDictionary class]]) {
        NSData * JSONData = [(NSString *)payloadDict dataUsingEncoding:NSUTF8StringEncoding];
        payloadDict = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    }
    
    if(DictBool(payloadDict)) {
        NewsModel * newsModel = [NewsModel newsModelWithDict:payloadDict];
        if(StrBool(newsModel.detailUrl)) {
            [self dealWithUserInfoDictionary:payloadDict];
            return;
        }
    }
    
    [self dealWithUserInfoDictionary:allDict];
}
*/




- (void)dealloc {
    NOTI_REMOVE_SELF;
}
@end
