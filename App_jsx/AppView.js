//固定写法
import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
} from 'react-native'

import styles from './mainStyle.js'

//es6的写法
class AppView extends Component { //继承 Component
    render(){
        return(
               <View style={{flex:1}}>
                   <View style={styles.contentPad}>
                       <View style={styles.contentColor}></View>
                       <Text style={styles.containerText}>定存宝 AppView</Text>
                   </View>
               
                   <View style={styles.line} />
           
                   <View style={styles.textHeader}>
                       <View style={styles.textContent}>
                           <Text style={styles.TextLeftRateNum}>5.5309% AppView</Text>
                           <Text style={styles.TextRightRateNum}>灵活 AppView</Text>
                       </View>
                       <View style={styles.textContent}>
                           <Text style={styles.TextLeftRate}>期待年回报率 AppView</Text>
                           <Text style={styles.TextRightRate}>锁定期 AppView</Text>
                       </View>
                   </View>
               </View>
               )
    }
}


//最后导出
export default AppView;
