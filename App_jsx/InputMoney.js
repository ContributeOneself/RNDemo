//固定写法
import React, { Component } from 'react';
import {
    Text,
    View,
    TextInput,
    StyleSheet,
} from 'react-native'

import styles from './BaseStyle.js'


//es6的写法
class InputMoney extends Component { //继承 Component

	constructor(props) {
	    super(props);
	    this.state = { text: '' };
	}

    render(){
        return(
               <View style={{flex:1}}>

                    <Text style={{paddingLeft:20, height:50, fontSize:14}}>可用余额(元)0.00</Text>

                    <View style={styles.line}/>
        
                    <View style={{paddingTop:10, paddingBottom:10, paddingLeft:20, height:50, flexDirection:'row'}}>
                        <Text style={{paddingTop:5, fontSize: 16, height:30, width:12}}>￥</Text>

                        <TextInput style={{flex:1 ,marginLeft:0, paddingLeft:5, fontSize:14}}
                            placeholder="请输入100~498.990.46"
                            keyboardType="phone-pad"
                            onChangeText={(text) => this.setState({text})}
                            value={this.state.text}
                            />
               
                    </View>
    
                    <View style={styles.line}/>
                
                </View>
               )


    }


}


//最后导出
export default InputMoney;
