// 样式
// create by srxboys

import React, { Component } from 'react';

import {
    StyleSheet,
    Dimensions,
} from 'react-native';

export const deviceWidth = Dimensions.get('window').width;      //设备的宽度
export const deviceHeight = Dimensions.get('window').height;    //设备的高度
export const TextLeftRateRight = 10;
export const TextLeftRateLeft = 33;
export const halfWidth = (deviceWidth- TextLeftRateLeft -TextLeftRateRight)/2;


export default  StyleSheet.create({
  	container: {
      flex: 1,
      backgroundColor : '#f0f4f7',
  	},

  	//空
  	space:{
        padding : 0,
        height : 10,
        backgroundColor : '#E0E4E5',
    },

    //线
  	line: {
        padding : 0,
        height : 1,
        backgroundColor : '#F0E4E5',
    },

    //线
    lineIndent : {
        height : 1,
        paddingLeft : 20,
        backgroundColor : '#F0E4E5',
    },

    //线
    lineCenter : {
        height : 1,
        paddingLeft : 20,
        paddingRight : 20,
        backgroundColor : '#F0E4E5',
    },
})
