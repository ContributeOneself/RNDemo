// 样式
// create by srxboys

import React, { Component } from 'react';

import {
    StyleSheet,
    Dimensions,
} from 'react-native';

export const deviceWidth = Dimensions.get('window').width;      //设备的宽度
export const deviceHeight = Dimensions.get('window').height;    //设备的高度
export const TextLeftRateRight = 10;
export const TextLeftRateLeft = 33;
export const halfWidth = (deviceWidth- TextLeftRateLeft -TextLeftRateRight)/2;

export default  StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor : '#f0f4f7',
    },

    CSwiper : {
        margin : 0,
        height : 200,
    },

    slide: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'transparent'
    },

    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB'
    },

    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
    },

    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
    },

    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold'
    },


    space:{
        padding : 0,
        height : 10,
        backgroundColor : '#E0E4E5',
    },

    line: {
        padding : 0,
        height : 1,
        backgroundColor : '#F0E4E5',
    },

    lineIndent : {
        height : 1,
        paddingLeft : 20,
        backgroundColor : '#F0E4E5',
    },

    lineCenter : {
        height : 1,
        paddingLeft : 20,
        paddingRight : 20,
        backgroundColor : '#F0E4E5',
    },

    contentPad : {
        paddingTop : 12,
        height : 40,
        backgroundColor : 'white',
        flexDirection:'row',
    },

    contentColor : {
        marginLeft : 20,
        width : 3,
        height : 16,
        backgroundColor : 'blue',
    },

    containerText : {
        marginLeft : 10,
        height : 30,
        fontSize: 16,
        color : '#000',
    },

    textHeader : {
        paddingTop:10,
        height:70,
        backgroundColor : '#FFF',
    },

    textContent : {
        paddingTop : 8,
        height : 25,
        flexDirection:'row',
        backgroundColor : 'white',
    },

    TextLeftRateNum : {
        marginLeft : TextLeftRateLeft,
        width : halfWidth,
        fontSize: 16,
        color : '#fc8936',
    },


    TextRightRateNum : {
        marginRight: TextLeftRateRight,
        width : halfWidth,
        fontSize: 14,
        textAlign : 'right',
        color : '#fc8936',
    },

    TextLeftRate : {
        marginLeft : TextLeftRateLeft,
        width : halfWidth,
        fontSize: 14,
        color : '#a1acb4',
    },

    TextRightRate : {
        marginRight: TextLeftRateRight,
        width : halfWidth,
        fontSize: 14,
        textAlign : 'right',
        color : '#a1acb4',
    },

    ImageRow : {
        height : 80,
        width : deviceWidth,
        flexDirection:'row',
    },

    ImageRowContent : {
        flex: 1,
        justifyContent : 'center',
        alignItems : 'center',
    },

    ImageRowContentImage : {
        paddingTop : 5,
        alignItems : 'center',
        width : 10,
        height : 10,
    },

    ImageRowContentText : {
        marginTop : 5,
        height : 30,
        textAlign : 'center',
        fontSize : 12,
    },
})

