import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    NativeModules
} from 'react-native';


//默认应用的容器组件
export default class HelloWorldApp extends Component {
    
    render() {
        return (
                <View style={styles.container}>
                <Text style={styles.text} onPress={()=>this.onPress()}>点击往原生传字符串</Text>
                </View>
                );
    }
    
    // 传原生一个字符串
    onPress(){
        let model = NativeModules.Model_5;
        model.requestAPI();
    }
}


//样式定义
const styles = StyleSheet.create({
                                 container:{
                                 flex: 1,
                                 backgroundColor: 'steelblue'
                                 },
                                 text:{
                                 fontSize: 20,
                                 height: 40,
                                 color: '#333333',
                                 backgroundColor: 'powderblue'
                                 }
                                 });

AppRegistry.registerComponent('RNDemo', () => HelloWorldApp);
