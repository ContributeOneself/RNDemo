import React, { Component } from 'react';

// 这个目前不行，React 可以，React-Native 不行？
//import App from "./jsDemo/App"; //实际是 import App from "App.js" 简写而已

import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Button,
    DatePickerIOS,
    NativeModules
} from 'react-native';


//以下是 ES6 写法
export default class HelloWorldApp extends Component {
    /*
        初始化组件state的值,返回值会赋值给this.state属性,这是ES6版本的写法,ES5中使用的是getInitialState.
        在ES6的版本中有些不太一样，相对与getDefaultProps，ES6将默认属性对象作为了构造函数的一个属性，
        而getInitialState则变成了在其构造器函数中给this.state赋值
     */
    constructor(){
        //构造函数
        super();
        this.state = {startDate: new Date(), endDate: new Date()};
    }
    
    // say hello
    onPress(){
        let HelloWorld = NativeModules.RXHelloWorld;
        HelloWorld.sayHello('Hello World你好');
    }
    
    // Compar
    onPressDateValidation() {
        var myDate = NativeModules.RXMyDate;
        myDate.printDate(this.state.startDate.getTime(), this.state.endDate.getTime());
    }
    onStartDateChange(date) {
        this.setState({startDate: date});
    }
    onEndDateChange(date) {
        this.setState({endDate: date});
    }
    
    
    //渲染并返回一个虚拟dom
    render() {
        return (
                <View style={{marginTop:40}}>
                
                    <DatePickerIOS
                        date={this.state.startDate}
                        mode='date'
                        onDateChange={this.onStartDateChange.bind(this)} />
                
                    <DatePickerIOS
                        date={this.state.endDate}
                        mode='date'
                        onDateChange={this.onEndDateChange.bind(this)} />
                
                    <Button onPress={this.onPressDateValidation.bind(this)} title="Compar" />
                    <Button onPress={this.onPress.bind(this)} title="Say Hello" />
                </View>
                );
    }
}

AppRegistry.registerComponent('RNDemo', () => HelloWorldApp);
