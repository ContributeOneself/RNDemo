/**
 * <# ? #>
 *
 * author : srxboys
 * @flow
 */

'use strict'
import React, { Component } from 'react';

import {
 StyleSheet,
 Text,
 View,
 Image,
 NativeModules,
 TouchableWithoutFeedback
 } from 'react-native'

class RefText extends Component <{}> {
  constructor(props) {
    super(props);
    this.state=({
      size : 10
    })
  }

  getSize() {
      return this.state.size;
  }

  render(){
    return(
        <View style={{flex:1}} >
          <Text style={{fontSize:18, marginTop:10, height:20}} onPress={()=>{
            this.setState({
              size : this.state.size + 10
            })
          }} >
            变大大 {this.state.size}
          </Text>
          <Text style={{fontSize:18, height:20, marginTop:10}} onPress={()=>{
            this.setState({
              size : this.state.size - 10
            })
          }} >
            变小小 {this.state.size}
          </Text>
          <Text style={{fontSize:this.state.size, marginTop:10, backgroundColor:'green'}} onPress={()=>{
            this.setState({
              size : this.state.size + 10
            })
          }} >
            啊啊啊啊 {this.state.size}
          </Text>
        </View>
    )
  }
}

 export default class StudyRef extends Component<{}> {
   constructor(props) {
     super(props);
     this.state=({
       size : 10
     })
   }

   render(){
     return(
       <View style={{flex:1}}>
         <Text style={{marginTop:40, fontSize:18, height:30, backgroundColor:'yellow'}}
            onPress={()=>{
              /*
              // WARNING: 第1️⃣种
              // let gsize = this.refs.refText.getSize();
              let gsize = this.refs['refText'].getSize();
              */

              // WARNING: 第2️⃣种
              let gsize = this.refText.getSize();

              // alert(gsize)
              this.setState({
                size : gsize
              })

            }}
            >
                获取大小 {this.state.size}
         </Text>

         <RefText style={{flex:1, marginTop:10}}
           /*
           // WARNING: 第1️⃣种
            ref="refText"
            */


            // WARNING: 第2️⃣种
            ref={refText=>this.refText=refText}
         />
       </View>
     )
   }
 }
