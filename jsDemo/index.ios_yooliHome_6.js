import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    ScrollView,
    Text,
    View,
    Image,
    NativeModules,
    Navigator,
    NavigatorBar,
    TouchableWithoutFeedback,
    TouchableHighlight,
    TouchableOpacity,
} from 'react-native'

//轮播图(第三方)
import Swiper from 'react-native-swiper'

//导入样式
import styles from './App_jsx/mainStyle.js'

import AppView from "./App_jsx/AppView"; //实际是 import App from "App.js" 简写而已

var model = NativeModules.YooliHome;

export default class HelloWorldApp extends Component {
    
    render () {
        return (
               <ScrollView style={styles.container} >
                    <View style={styles.container}>
                        //第三方 轮播图
                        <View style={styles.CSwiper}>    
                            <Swiper style={styles.slide}
                             dot={<View style={{backgroundColor: 'rgba(0,0,0,.6)', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                             activeDot={<View style={{backgroundColor: '#FFF', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
                             autoplay>
                                <View style={styles.slide1}>
                                    <Text style={styles.text}>Hello Swiper</Text>
                                </View>
                                <View style={styles.slide2}>
                                    <Text style={styles.text}>Beautiful</Text>
                                </View>
                                <View style={styles.slide3}>
                                    <Text style={styles.text}>And simple</Text>
                                </View>
                            </Swiper>
                        </View>

                        <View style={styles.space} />


                        // 注意：TouchableHighlight只支持一个子节点。
                        //如果你希望包含多个子组件，用一个View来包装它们。
                        <TouchableHighlight  
                           //按下后背景透明度变化  
                          activeOpacity={0.7}  
                          //按下后背景颜色  
                          underlayColor={'red'} 
                          onPress={()=>this.goWYBInvest()}>
                          <View>
                                <View style={styles.contentPad}>
                                    <View style={styles.contentColor}></View>
                                    <Text style={styles.containerText}>无忧宝</Text>
                                </View>
                                <View style={styles.line}> </View>
                                <View style={styles.textHeader}>
                                    <View style={styles.textContent}>
                                        <Text style={styles.TextLeftRateNum}>5.5309%</Text>
                                        <Text style={styles.TextRightRateNum}>灵活</Text>
                                    </View>
                                    <View style={styles.textContent}>
                                        <Text style={styles.TextLeftRate}>期待年回报率</Text>
                                        <Text style={styles.TextRightRate}>锁定期</Text>
                                    </View> 
                                </View>
                            </View>
                         </TouchableHighlight> 
                
                
                        <View style={styles.space} />
                
                        //import 导入的js
                        <View style={{flex:1}}>
                            <AppView></AppView>
                        </View>
                
                        <View style={styles.space} />
                
                        //点击
                        <TouchableOpacity  onPress={()=>this.goDCBInvest()}>  
                                <View style={styles.contentPad}> 
                                    <View style={styles.contentColor}></View>
                                    <Text style={styles.containerText}>定存宝</Text>
                                </View>
                                <View style={styles.line} />
                                <View style={styles.textHeader}>
                                    <View style={styles.textContent}>
                                        <Text style={styles.TextLeftRateNum}>5.5309%</Text>
                                        <Text style={styles.TextRightRateNum}>灵活</Text>
                                    </View>
                                    <View style={styles.textContent}>
                                        <Text style={styles.TextLeftRate}>期待年回报率</Text>
                                        <Text style={styles.TextRightRate}>锁定期</Text>
                                    </View> 
                                </View>
                         </TouchableOpacity>       
                    
                        <View style={styles.space} />



                        <View style={styles.ImageRow}>
                            <View style={styles.ImageRowContent}>
                                <Image style={styles.ImageRowContentImage} source={{url:'https://raw.githubusercontent.com/srxboys/RXExtenstion/master/RXExtenstion/images/psb_1.jpeg'}} /> 
                                <Text style={styles.ImageRowContentText} >超2000万人的选择{"\n"}631亿投资已达成</Text>
                            </View>
                            <View style={styles.ImageRowContent}>
                                <Image style={styles.ImageRowContentImage} source={{url:'https://raw.githubusercontent.com/srxboys/RXExtenstion/master/RXExtenstion/images/psb_2.jpeg'}} /> 
                                <Text numberOfLines={2} style={styles.ImageRowContentText} >顶级VC注资{"\n"}银行级别技术保障</Text>
                            </View>
                            <View style={styles.ImageRowContent}>
                                <Image style={styles.ImageRowContentImage} source={{url:'https://raw.githubusercontent.com/srxboys/RXExtenstion/master/RXExtenstion/images/psb_3.jpeg'}} /> 
                                <Text numberOfLines={2} style={styles.ImageRowContentText} >华夏银行存管{"\n"}用户资金与平台隔离</Text>
                            </View>
                        </View>

                        <TouchableWithoutFeedback  onPress={()=>this.goLJG()}>
                            <View>
                                <Text style={{color:'blue', paddingTop:5, fontSize:12, height:22, textAlign:'center'}}>了解更多</Text>
                            </View>
                        </TouchableWithoutFeedback> 

                       <View style={{paddingTop:20, justifyContent: 'center',alignItems:'center', height:50}}>
                           <Text style={{textAlign:'center', color:'#a1acb4',fontSize: 14}}><Image style={{width:10, height:10,resizeMode:'cover'}} source={{url:'https://raw.githubusercontent.com/srxboys/RXExtenstion/master/RXExtenstion/images/psb_3.jpeg'}} /> 市场有风险，出借需谨慎</Text>
                       </View>

                    </View>
                </ScrollView>
                )
    }
    
    //下面的完全可以用一个方法
    //无忧宝点击(与原生交互，原生没写也不会崩溃)
    goWYBInvest() {
//        model.goWYBInvest('http://js.jsbundle');
    }

    //定存宝点击(与原生交互)
    goDCBInvest() {
//        model.goDCBInvest('http://js.jsbundle');
    }

    //了解更多(与原生交互)
    goLJG() {
//        model.goLJG('http://js.jsbundle');
    }
    
}






AppRegistry.registerComponent('RNDemo', () => HelloWorldApp);
