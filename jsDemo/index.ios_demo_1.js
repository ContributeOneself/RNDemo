import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';


/*
class RNHighScores extends React.Component {
  render() {
    var contents = this.props["scores"].map(
      score => <Text key={score.name}>{score.name}:{score.value}{"\n"}</Text>
    );
    return (
      <View style={styles.container}>
        <Text style={styles.highScoresTitle}>
          2048 High Scores!
        </Text>
        <Text style={styles.scores}>    
          {contents}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  highScoresTitle: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  scores: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});



// 整体js模块的名称
AppRegistry.registerComponent('RNDemo', () => RNHighScores);
*/

// 第二种
/*
import App from './ios';
AppRegistry.registerComponent('RNDemo', () => App);
*/



//第三种 hello work
/*
export default class HelloWorldApp extends Component {
  render() {
      let pic = {
          uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
      };
    return (
            // 尝试把`flexDirection`改为`column`看看
            <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{width: 50, height: 500, backgroundColor: 'powderblue'}} />
                <View style={{width: 50, height: 500, backgroundColor: 'skyblue'}} />
                <View style={{width: 50, height: 500, backgroundColor: 'steelblue'}} />
                <Image source={pic} style={{width: 193, height: 110}} />
            </View>
    );
  }
}
*/


//ES6 写法

class Greeting extends Component {
    render() {
        return (
                <View style={{alignItems: 'center'}}>
                    <Text>Hello {this.props.name}  !</Text>
                    <Text>Hel2  {this.props.pass}  !</Text>
                    <Text>Hel3  {this.props.gender}!</Text>
                </View>
                );
    }
}

export default class HelloWorldApp extends Component {
    render() {
        return (
                <View style={{alignItems: 'center'}}>
                    <Greeting name='Rexxar' />
                    <Greeting pass='Jaina' />
                    <Greeting gender='Valeera' />
                </View>
                );
    }
}
        
AppRegistry.registerComponent('RNDemo', () => HelloWorldApp);
