
/*
 
         本js文件，都是为了注释，不可作为执行文件
 
         官网 http://facebook.github.io/react-native/docs/getting-started.html
         中文翻译 https://reactnative.cn/docs
 
 
 
         RN 原理:
                https://www.jianshu.com/p/5cc61ec04b39
                https://blog.csdn.net/xiangzhihong8/article/details/54425807
 
 
         RN demo 从一个实战项目来看一下React Native开发的几个关键技术点
         http://www.cocoachina.com/apple/20170831/20427.html
 
         学了react native,然后再学express(ReactNative 通过fetch请求获得后台的json数据，而express就是处理这些请求的)
         http://www.reactnativeexpress.com
 
 
         RN开发工具 教程 http://www.hangge.com/blog/cache/detail_1490.html
 */


//----------------------------------------------------------------------------------



## package.json

{
    "name": "RNDemo",
    "version": "0.0.1", //字段没有太大意义（除非你要把你的项目发布到npm仓库）
    "private": true,
    "scripts": {
        "start": "node node_modules/react-native/local-cli/cli.js start", //启动packager服务的命令
        "test": "jest"
    },
    "dependencies": { //react和react-native的版本取决于你的具体需求,一般来说我们推荐使用最新版本
        "react": "16.3.1",
        "react-native": "0.55.4",
        /*
             你可以使用npm info react和npm info react-native来查看当前的最新版本
             这两个必须是匹配，官方也没能列出匹配的列表，只能通过命令  先尝试执行npm install，然后根据提示 ...
         */
        "eslint": "^3.17.0", //是一个JavaScript代码静态检查工具，可以检查JavaScript的语法错误，提示潜在的bug https://segmentfault.com/a/1190000009914940
                            //安装官网的Rule,配置代码的规范  http://eslint.org/docs/rules/
        "eslint-plugin-react-native": "^3.2.1",
    },
    "devDependencies": {
        "babel-jest": "22.4.3",
        "babel-preset-react-native": "4.0.0",
        "jest": "22.4.3",
        "react-test-renderer": "16.3.1"
    },
    "jest": {
        "preset": "react-native"
    }
}


/*
    原生+RN 具体了解
    https://reactnative.cn/docs/0.51/integration-with-existing-apps.html#content
 */

//----------------------------------------------------------------------------------

## main.jsbundle

/*
 
     //不知对否
     React-native 使用jsx语法
        JSX就是Javascript和XML结合的一种格式。React发明了JSX,利用HTML语法来创建虚拟DOM。
    
     //不知对否
     此文件是curl 对 index.ios.js(jsx) 编译后 -> main.jsbundle文件(ECMAScript 简称ES)

 
     Node.js 使用 ECMAScript(Node 是 JavaScript 的服务器运行环境（runtime))
          使用： node node_modules/react-native/local-cli/cli.js start
          快捷使用(这个下载node_modules 就配置好了)
             1、React-native start
             2、npm start
 
     《ECMAScript 6 入门》是一本开源的 JavaScript（简称ES6）
         语言教程  http://es6.ruanyifeng.com
 
     简介 http://es6.ruanyifeng.com/#docs/intro
 
 
*/

//----------------------------------------------------------------------------------


## index.ios.js


'use strict'; //严格模式下你不能使用未声明的变量
        //具体解释 http://www.runoob.com/js/js-strict.html

import React, { Component } from 'react'; //从 react 导出自带的组件

//import "./index.css"; //导入css样式
    //我们也可以使用在线css样式   http://www.bootcss.com

//import App from "App"; //实际是 import App from "App.js" 简写而已

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  NativeModules
} from 'react-native';
// 从 react-native 导出自带的组件






export default class HelloWorldApp extends Component {
    // https://www.jianshu.com/p/997d43e9d197
    // 渲染并返回一个虚拟dom
    render() {
        //每次都会渲染
        return (
                <View style={styles.container}>
                  //内容  包裹 text/inputText/image.....
                </View>
            );
     }
    
    
    //js 事件(func)、变量(let/var) ...
}

//样式写在外面，只会创建一次
const styles = StyleSheet.create(
                                 {
                                 //container 为样式名 供使用
                                 container: {
                                     flex: 1,
                                     marginTop:100
                                 },
                                 welcome: {
                                     fontSize: 20,
                                     textAlign: 'center',
                                     margin: 10,
                                 },
                                 instructions: {
                                     textAlign: 'center',
                                     color: '#333333',
                                     marginBottom: 5,
                                 },
                                 });
/*
 《 styles 》
 
 弹性盒(Flexbox)
    https://reactnative.cn/docs/0.21/flexbox.html
 
 flex ：弹性（Flex）宽高,动态地扩张或收缩
    https://reactnative.cn/docs/0.51/height-and-width.html#content
 
 其他样式 flexDirection、alignItems和 justifyContent三个样式属性
    https://reactnative.cn/docs/0.51/layout-with-flexbox.html#content
 
 RN props布局样式属性 https://reactnative.cn/docs/0.51/layout-props.html#content

 如果觉得解释的不全面，就看:
     1、《赢科公司》免费w3c的css样式： http://www.w3school.com.cn/css/index.asp
     2、《亿动科技》有付费的，这个广告比较多   https://www.w3cschool.cn
     3、 如果你的英文好(国外 需翻墙) ： http://www.w3schools.com
 */










// 注册应用(registerComponent)后才能正确渲染（HelloWorldApp整体js模块的名称）
// 注意：只把应用作为一个整体注册一次，而不是每个组件/模块都注册
AppRegistry.registerComponent('RNDemo', () => HelloWorldApp);




/*
    参考demo博客
    https://blog.csdn.net/jiangbo_phd/article/details/55046347
 */



/*
 一、RN 提供了四种点击事件
     1、TouchableHighlight       : 可编写点击的 背景色、透明度,只支持一个子节点
     2、TouchableNativeFeedback  : 仅限Android平台
     3、TouchableWithoutFeedback : 点击没有任何颜色变化,只支持一个链单节点
     4、TouchableOpacity         : 此组件与TouchableHighlight的区别在于并没有额外的颜色变化，
                                    继承了所有TouchableWithoutFeedback的属性,
                                    并支持多个子节点
 
 二、React认为一个组件应该具有如下特征：
     可组合（Composeable）：一个组件易于和其它组件一起使用，或者嵌套在另一个组件内部。如果一个组件内部创建了另一个组件，那么说父组件拥有它创建的子组件，通过这个特性，一个复杂的UI可以拆分成多个简单的UI组件；
     可重用（Reusable）：每个组件都是具有独立功能的，它可以被使用在多个UI场景；
     可维护（Maintainable）：每个小的组件仅仅包含自身的逻辑，更容易被理解和维护；
 
 
 三、在React Native（React.js）里，组件所持有的数据分为两种：
     属性（props）：组件的props是不可变的，它只能从其他的组件（例如父组件）传递过来。
     状态（state）：组件的state是可变的，它负责处理与用户的交互。在通过用户点击事件等操作以后，如果使得当前组件的某个state发生了改变，那么当前组件就会触发render()方法刷新自己。
 
 四、组件也有生命周期，大致分为三大阶段：
     Mounting：已插入真实 DOM
     Updating：正在被重新渲染
     Unmounting：已移出真实 DOM
 
 五、React 中组件的几种通信方式，分别是：
     父组件向子组件通信：使用 props
     子组件向父组件通信：使用 props 回调
     跨级组件间通信：使用 context 对象
     非嵌套组件间通信：使用事件订阅
 */


/*
    从一个实战项目来看一下React Native开发的几个关键技术点
    http://www.cocoachina.com/apple/20170831/20427.html
 */




/*
 许可证总结 http://www.gnu.org/licenses/
 */
