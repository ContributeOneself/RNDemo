import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TextInput,
} from 'react-native';

//输入框组件
class Search extends Component {
    //构造函数
    constructor(props) {
        super(props);
        this.state = {text: ''};
    }
    
    //组件渲染
    render() {
        return (
                <View style={styles.flex}>
                    <View style={[styles.flexDirection, styles.inputHeight]}>
                        <View style={styles.flex}>
                            <TextInput
                                style={styles.input}
                                returnKeyType="search"
                                placeholder="请输入关键字"
                                onChangeText={(text) => this.setState({text})}/>
                        </View>
                
                        <View style={styles.btn}>
                            <Text style={styles.search} onPress={this.search.bind(this)}>搜索</Text>
                        </View>
                    </View>
                
                    <Text style={styles.tip}>已输入{this.state.text.length}个文字</Text>
                </View>
                );
    }
    
    //搜索按钮点击
    search(){
        alert("您输入的内容为："+this.state.text);
    }
}

//默认应用的容器组件
export default class HelloWorldApp extends Component {
    render() {
        return (
                <View style={[styles.flex, styles.topStatus]}>
                    <Search></Search>
                </View>
                );
    }
}

//样式定义
// https://reactnative.cn/docs/0.51/height-and-width.html#content
const styles = StyleSheet.create({
     flex:{
         flex: 1,
     },
     flexDirection:{
         flexDirection:'row'
     },
     topStatus:{
         marginTop:100,
     },
     inputHeight:{
         height:45,
     },
     input:{
         height:45,
         borderWidth:1,
         marginLeft: 5,
         paddingLeft:5,
         borderColor: '#ccc',
         borderRadius: 4
     },
     btn:{
         width:55,
         marginLeft:-5,
         marginRight:5,
         backgroundColor:'#23BEFF',
         height:45,
         justifyContent:'center',
         alignItems: 'center'
     },
     search:{
         color:'#fff',
         fontSize:15,
         fontWeight:'bold'
     },
     tip:{
         marginLeft: 5,
         marginTop: 5,
         color: '#C0C0C0',
     }
});

AppRegistry.registerComponent('RNDemo', () => HelloWorldApp);
